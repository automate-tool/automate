# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Added

### Changed

### Removed

### Fixed
- Module settings did not load if large settings (e.g. scripts) were stored.

## [1.0.1] - 2021-10-22

### Added
- Implemented frontend:
  - User management
  - Module setup
    - Configuration export / import
  - Module dashboard
  - Module status
  
- Implemented modules:
  - Weather (api.openweathermap.org)
    - Current weather
    - Weather forecast (24h, 48h)
  - Raspberry pi
  - Scripting host

- Samples
