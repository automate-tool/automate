using AutoMate.Modules;
using AutoMate.Modules.Script;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;


[AmScript(000001, 1,0,0)]
public class Script : AmScript
{
    #region Events
    protected override void OnInit(bool bReInit)
    {
        // Write some infos to console:
        Console.WriteLine("Modules: {0}", iHost.Modules.Count());

        foreach (var _iModule in iHost.Modules)
            Console.WriteLine("Module '{0}'", _iModule);

        // Write something to log:
        Logger.LogInformation(24, string.Format("First module is '{0}'.", iHost.Modules.First()));
    }
    protected override void OnCycle()
    {
    }
    #endregion
}
