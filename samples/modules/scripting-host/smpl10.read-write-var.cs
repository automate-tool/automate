using AutoMate.Modules;
using AutoMate.Modules.Script;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;


[AmScript(000010, 1,0,0)]
public class Script : AmScript
{
    #region Events
    protected override void OnInit(bool bReInit)
    {
    }
    protected override void OnCycle()
    {
        var _iVar = Self.Datasets["Variables"];

        // [Sample 1] Touch own variables:
        var _iTest1 = _iVar.GetVariable<int>("test!");
        _iTest1.Value++;

        _iVar.GetVariable<int>("Display1").Value++;
        _iVar.GetVariable<int>("Input1").Value += 11;
        var _iTest2 = _iVar.GetVariable<bool>("Output1");
        _iTest2.Value = !_iTest2.Value;

        // [Sample 2] Read a weather module's variable and write value to own variable:
        var _iTimeStamp = iHost.GetVariable<DateTime>("My Weather", "Forecast", "TimeStamp");
        var _iTest3 = _iVar.GetVariable<string>("My String");
        _iTest3.Value = ("Touched at " + _iTimeStamp.Value.ToString());
    }
    #endregion
}
