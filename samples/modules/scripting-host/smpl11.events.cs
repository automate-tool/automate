using AutoMate.Modules;
using AutoMate.Modules.Script;
using Microsoft.Extensions.Logging;
using Shared.Utils.Core;
using System;
using System.Collections.Generic;
using System.Linq;


[AmScript(000011, 1,0,0)]
public class Script : AmScript
{
    #region Events
    protected override void OnInit(bool bReInit)
    {
        /*
        // [Sample 1a]
        // Subscribe to a single variable's COV event:
        iHost.GetVariable("My Weather", "Forecast", "Temperature").ValueChanged += OnVariableValueChanged;
        */
        // [Sample 1b]
        // Subscribe to multiple variable's COV event:
        iHost.GetDataset("My Weather", "Forecast").Variables.ForEach(_iVar => { _iVar.ValueChanged += OnVariableValueChanged; });
        
        // [Sample 2]
        // Subscribe to dataset updates:
        iHost.GetDataset("My Weather", "Forecast").Updated += OnDatasetUpdated;

        // [Sample 3]
        // Subscribe to a button click event:
        Self.Datasets["Variables"].GetVariable<bool>("Button").ValueChanged += OnButtonClick;
    }
    protected override void OnApplyValues()
    {
        Console.WriteLine("[{0:HH:mm:ss.fff}] Apply some changed variable values.", DateTime.Now);
    }
    protected override void OnCycle()
    {
    }
    
    private void OnDatasetUpdated(object oSender, EventArgs vArgs)
    {
        var _iVar = (IAmModuleDataset)oSender;
        Console.WriteLine("[{0:HH:mm:ss.fff}] Updated dataset '{1}'.", DateTime.Now, _iVar.Description);
    }
    private void OnVariableValueChanged(object oSender, ValueChangedEventArgs vArgs)
    {
        var _iVar = (IAmVariable)oSender;
        Console.WriteLine("[{0:HH:mm:ss.fff}] '{1}' changed value from '{2}' to '{3}'.", DateTime.Now, _iVar.DisplayName, vArgs.PreviousValue, _iVar.GetValue());
    }
    private void OnButtonClick(object oSender, ValueChangedEventArgs vArgs)
    {
        if (ValueChangedFlags.Impulse.TestFlag(vArgs.Flags))
            Console.WriteLine("[{0:HH:mm:ss.fff}] Button clicked.", DateTime.Now);
    }
    #endregion
}
