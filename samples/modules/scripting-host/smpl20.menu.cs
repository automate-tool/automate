using AutoMate.Modules;
using AutoMate.Modules.Script;
using Microsoft.Extensions.Logging;
using Shared.Asp.GenMenu;
using System;
using System.Collections.Generic;
using System.Linq;


[AmScript(000020, 1,0,0)]
public class Script : AmScript, IMenuProvider
{
    #region Properties.Management
    public IEnumerable<IMenuGroup> MenuGroups => lMenuGroups;
    private List<IMenuGroup> lMenuGroups = new List<IMenuGroup>();
    #endregion


    #region Events
    protected override void OnInit(bool bReInit)
    {
        // Initialize menu:
        var _aGen = new MenuGroup();
        {
            var _aBut = new DropDownMenuButton("Click me");
            {
                TextMenuItem _tItem;
                _aBut.Items.Add(new HeaderMenuItem("Some test menu items here"));
                _aBut.Items.Add(_tItem = new TextMenuItem("Some item"));
                _tItem.Click += OnMenu_SomeItem;
                _aBut.Items.Add(new MenuSeparatorItem());
                _aBut.Items.Add(_tItem = new TextMenuItem("Another item"));
                _tItem.Click += OnMenu_AnotherItem;
            }
            _aGen.MenuButtons.Add(_aBut);
        }
        lMenuGroups.Add(_aGen);
    }
    protected override void OnCycle()
    {
    }
    #endregion
    #region Events.Menu
    public void OnMenu_SomeItem(object oSender, MenuEventArgs mArgs)
    {
    }
    public void OnMenu_AnotherItem(object oSender, MenuEventArgs mArgs)
    {
    }
    #endregion
}
