using AutoMate.Modules;
using AutoMate.Modules.Script;
using Microsoft.Extensions.Logging;
using Shared.Asp.Alert;
using System;
using System.Collections.Generic;
using System.Linq;


[AmScript(000021, 1,0,0)]
public class Script : AmScript, IAlertProvider
{
    #region Enumerations
    private enum AlertContext
    {
        eStatus
    }
    #endregion


    #region Properties.Management
    public IEnumerable<Alert> Alerts => lAlerts.Values;
    private Dictionary<AlertContext, Alert> lAlerts = new Dictionary<AlertContext, Alert>();
    #endregion


    #region Events
    protected override void OnInit(bool bReInit)
    {
    }
    protected override void OnCycle()
    {
        bool bUpdate = (++iCounter == 10);
        if (bUpdate)
        {
            iCounter = 0;

            var aStatus = lAlerts[AlertContext.eStatus] = new Alert(AlertLayout.eSuccess);
            {
                aStatus.Items.Add(new AlertTitleItem("Current status"));
                aStatus.Items.Add(new AlertTextItem(string.Format("Counter value is <u>{0}</u>.", iCounter)));
                aStatus.Items.Add(new AlertSeparatorItem());
                aStatus.Items.Add(new AlertTextItem(string.Format("Updated on:\n<b>{0:dddd, d.MMMM.yyyy}</b> at {0:HH:mm}", DateTime.Now), AlertIcon.eCheck));
            }
        }
    }
    #endregion
    

    private int iCounter;
}
