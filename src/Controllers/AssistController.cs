using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using AutoMate.Data;
using AutoMate.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Shared.Asp.UserNotice;
using Shared.Utils.Asp;
using Shared.Utils.Core;

namespace AutoMate.Controllers
{
    public class AssistController : BaseController
    {
        public AssistController(ApplicationDbContext aContext, AmCoreService aCoreService) : base(aContext)
        {
            this.aCoreService = aCoreService;
        }


        #region Views
        public IActionResult About() => View();
        #endregion

        
        #if DEBUG
        [HttpPost]
        public IActionResult ClearSession()
        {
            HttpContext.Session.Clear();

            // Notify user:
            aCoreService.UserNotices.Enqueue(new UserNotice("Cleared session.", "Session", "", NoticeType.eSuccess));

            return (Redirect("/"));
        }


        #region Views
        public IActionResult Debug() => View();
        #endregion
        #endif


        private AmCoreService aCoreService;
    }
}
