﻿using System;
using AutoMate.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace AutoMate.Controllers
{
    public class HomeController : BaseController
    {
        public HomeController(ApplicationDbContext aContext) : base(aContext)
        {
        }


        #region Views
        public IActionResult Index() => View();
        public IActionResult Privacy() => View();
        #endregion
    }
}
