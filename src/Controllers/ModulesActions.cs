using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using AutoMate.Modules;
using AutoMate.Services;
using AutoMate.Event;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Shared.Asp.GenMenu;
using Shared.Asp.GenNav;
using Shared.Utils.Core;


namespace AutoMate.Actions
{
    #region Types.Actions
    public class AmAddModuleAction : NavAction
    {
        #region Constants
        public const string KEY = "type";
        #endregion


        public AmAddModuleAction(NavActionBinder nBinder, string sValue) : base(nBinder, KEY)
        {
            this.ModuleType = Assembly.GetExecutingAssembly().GetType(sValue);
        }

        
        #region Properties.Management
        public Type ModuleType { init; get; }
        #endregion


        #region Format
        public static string FormatType(Type tType) => Format(KEY, tType.FullName);
        #endregion
    }
    public class AmSelectModuleAction : NavAction
    {
        #region Constants
        public const string KEY = "sel";
        #endregion


        public AmSelectModuleAction(NavActionBinder nBinder, string sValue) : base(nBinder, KEY)
        {
            this.SelectedModule = int.Parse(sValue);
        }

        
        #region Properties.Management
        public int SelectedModule { init; get; }
        #endregion


        #region Format
        public static string FormatName(int iIndex) => Format(KEY, iIndex);
        #endregion
    }
    public class AmRemoveModuleAction : NavAction
    {
        #region Constants
        public const string KEY = "remove";
        #endregion


        public AmRemoveModuleAction(NavActionBinder nBinder, string sValue) : base(nBinder, KEY)
        {
        }


        #region Format
        public static string FormatName() => Format(KEY);
        #endregion
    }
    public class AmSaveModuleAction : NavAction
    {
        #region Constants
        public const string KEY = "save";
        #endregion


        public AmSaveModuleAction(NavActionBinder nBinder, string sValue) : base(nBinder, KEY)
        {
        }


        #region Format
        public static string FormatName() => Format(KEY);
        #endregion
    }
    #endregion
    #region Types.Binder
    public abstract class AmModulesActionBinder : NavActionBinder<AmModule>
    {
        #region Management
        /// Determines the affected module.
        public AmModule GetModule(AmCoreService aCoreService)
        {
            // Determine instance:
            string _sKey = nameof(AmModule.Identifier);
            if (dData.TryPop(_sKey, out string _sIdfr))
            {
                var _lIdfr = long.Parse(_sIdfr);
                return (aCoreService.Modules.First(_mModule => (_mModule.Identifier == _lIdfr)));
            }
            else
                throw new KeyNotFoundException("Failed to determine module due to missing identifier!");
        }
        #endregion
    }
    public class AmModulesMenuActionBinder : AmModulesActionBinder
    {
        public AmModulesMenuActionBinder()
        {
            SupportAction(typeof(ButtonClickMenuAction));
        }
    }
    public class AmModulesNavActionBinder : AmModulesActionBinder
    {
        public AmModulesNavActionBinder()
        {
            SupportAction(typeof(AmAcknowledgeEventAction));
        }
    }
    public class AmModuleSetupNavActionBinder : NavActionBinder<AmModuleSettings>
    {
        public AmModuleSetupNavActionBinder()
        {
            Settings.OptimizeBoolValues = true;
            
            SupportAction(typeof(AmAddModuleAction));
            SupportAction(typeof(AmSelectModuleAction));
            SupportAction(typeof(AmRemoveModuleAction));
            SupportAction(typeof(AmSaveModuleAction));
        }
    }
    #endregion
}