using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Collections;
using System.Reflection;
using System.Text.Json;
using System.Threading.Tasks;
using AutoMate.Data;
using AutoMate.Models;
using AutoMate.Modules;
using AutoMate.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.Extensions.Logging;
using Shared.Utils.Asp;
using static Shared.Utils.Core.UtlJson;
using Shared.Asp.GenNav;
using Shared.Utils.Core;
using Microsoft.AspNetCore.Http;
using System.IO;
using Shared.Asp.UserNotice;
using Shared.Asp.GenMenu;
using System.IO.Compression;
using AutoMate.Actions;
using AutoMate.Event;

namespace AutoMate.Controllers
{
    [Authorize]
    public class ModulesController : BaseController
    {
        #region Constants
        private const string TITLE_MODULE_EXPORT = "Module export";
        private const string TITLE_MODULE_IMPORT = "Module import";
        private const string TITLE_UPDATE_MODEL = "Update";
        private const string TITLE_LOGS = "Logs";
        
        public const string KEY_LAST_WRITE = "LastWrite";
        public const string KEY_TEMP_SETTINGS = "TempSettings";
        public const string KEY_SEL_MODULE = "SelModule";
        #endregion


        public ModulesController(ApplicationDbContext aContext, AmCoreService aCoreService) : base(aContext)
        {
            this.aCoreService = aCoreService;
        }


        #region Properties.Management
        public ISession Session => HttpContext.Session;
        #endregion


        #region Management
        private void InitSession()
        {
            ReadSession(Session, out lTempSettings, out iSelModule);

            if (!Session.Keys.Contains(KEY_TEMP_SETTINGS))
            {
                // Begin configuration with current settings:
                lTempSettings.Clear();
                lTempSettings.AddRange(ModuleSettings);
                iSelModule = -1;
            }

            WriteSession();
        }
        public static void ReadSession(ISession iSession, out List<AmModuleSettings> lTempSettings, out int iSelModule)
        {
            lTempSettings = iSession.Get<List<AmModuleSettings>>(KEY_TEMP_SETTINGS, new ListJsonConverter<AmModuleSettings>(), new System.Text.Json.Workaround.TimeSpanConverter());

            var _iSelModule = iSession.GetInt32(KEY_SEL_MODULE);
            if ((_iSelModule != null) && (_iSelModule <= (lTempSettings.Count-1)))
                iSelModule = (int)_iSelModule;
            else
                iSelModule = -1;
        }
        private void WriteSession()
        {
            if (lTempSettings == null)
                Session.Clear();
            else
            {
                Session.Set(KEY_TEMP_SETTINGS, lTempSettings, new ListJsonConverter<AmModuleSettings>(), new System.Text.Json.Workaround.TimeSpanConverter());
                Session.SetInt32(KEY_SEL_MODULE, iSelModule);
            }
        }
        private void ResetSession()
        {
            lTempSettings = null;
            WriteSession();
        }
        #endregion
        #region Views
        public IActionResult Index() => View();
        public IActionResult Module(int iIndex)
        {
            if ((0 <= iIndex) && (iIndex < aCoreService.Modules.Count()))
                return (View(nameof(Module), aCoreService.Modules[iIndex]));
            else
                return (Error());
        }
        [Authorize(Roles = Env.ROLE_ADMIN)]
        public IActionResult Setup()
        {
            InitSession();

            if (iSelModule == -1)
                return (View());
            else
                return (View());
        }
        [Authorize(Roles = Env.ROLE_ADMIN)]
        public IActionResult Logs()
        {
            var _sFiles = Directory.EnumerateFiles(Env.LOG_PATH, string.Format("{0}*.{1}", Env.LOG_FILENAME, Env.LOG_FILEEXT));
            if (_sFiles.IsEmpty())
                ;
            else
            {
                using (var _mStream = new MemoryStream())
                {
                    // Create zip archive from log files:
                    using (var _zArchive = new ZipArchive(_mStream, ZipArchiveMode.Create, true))
                    {
                        _sFiles.ForEach(_sFile => _zArchive.CreateEntryFromFile(_sFile, Path.GetFileName(_sFile)));
                    }
                    _mStream.Flush();

                    // Write collected logs:
                    return (File(_mStream.GetBuffer(), "application/zip", string.Format("{0}.zip", Env.LOG_FILENAME)));
                }
            }
            return (NotFound("No logs found to download."));
        }
        #endregion
        #region Views.Redirect
        private IActionResult RedirectToModule(AmModule aModule)
        {
            return Redirect(string.Format("/Modules/{0}?iIndex={1}", nameof(Module), aCoreService.Modules.IndexOf(aModule)));
        }
        #endregion
        #region Actions
        [HttpPost]
        public IActionResult Module_MenuPostAction([ModelBinder(typeof(AmModulesMenuActionBinder))] NavAction aAction)
        {
            // Determine module instance:
            var aModule = ((AmModulesMenuActionBinder)aAction.Binder).GetModule(aCoreService);
            if (aModule is IMenuProvider _iMenu)
            {
                // Evaluate action:
                switch (aAction)
                {
                    case ButtonClickMenuAction _bBut:
                        // Generate "Click" event:
                        if (_iMenu.FindMenuItem(_bBut.ItemId) is TextMenuItem _mTextItem)
                        {
                            if (!_mTextItem.Enabled)
                                ; // Skip.
                            else if (!_mTextItem.Visible)
                                ; // Skip.
                            else
                            {
                                aModule.WorkSemaphore.Wait();
                                try
                                {
                                    _mTextItem._Click(this);
                                }
                                catch (Exception _eExcpt)
                                {
                                    Logger.LogError(149, _eExcpt, string.Format("Detected failed menu command in module '{0}'!", aModule));
                                }
                                finally
                                {
                                    aModule.WorkSemaphore.Release();
                                }
                            }
                        }
                        break;

                    default:
                        throw new NotImplementedException(string.Format("Not implemented type '{0}'!", aAction.GetType().Name));
                }

                return (RedirectToModule(aModule));
            }
            else
                return (Error());
        }
        [HttpPost]
        public IActionResult Module_NavPostAction([ModelBinder(typeof(AmModulesNavActionBinder))] NavAction aAction)
        {
            // Determine module instance:
            var aModule = ((AmModulesNavActionBinder)aAction.Binder).GetModule(aCoreService);

            // Update module data:
            Action<Exception> ShowException = (_eExcpt) => {
                Logger.LogError(201, _eExcpt, string.Format("Failed to post '{0}' action!", aAction.Key));
                aCoreService.UserNotices.Enqueue(new UserNotice(_eExcpt.Message, TITLE_UPDATE_MODEL, "Failed to parse input!", NoticeType.eError));
            };

            try
            {
                aAction.UpdateModel(aModule);
            }
            catch (AggregateException aExcpt)
            {
                aExcpt.InnerExceptions.ForEach(_eExcpt => ShowException(_eExcpt));
            }
            catch (Exception eExcpt)
            {
                ShowException(eExcpt);
            }

            // Evaluate action:
            switch (aAction)
            {
                case ApplyValuesAction:
                    aModule.OnApplyValues();
                    break;
                case ButtonLayoutNavAction _bBut:
                    // Generate "Click" event:
                    UtlComponentModel.SetValue(NavActionBinder.PropertyResolver, aModule, _bBut.PropertyName, true);
                    break;
                case AmAcknowledgeEventAction _aAckEvent:
                    _aAckEvent.Event.Acknowledge();
                    break;

                default:
                    throw new NotImplementedException(string.Format("Not implemented type '{0}'!", aAction.GetType().Name));
            }

            // Update persistent data:
            Context.WriteModulePersistence();

            return (RedirectToModule(aModule));
        }
        public IActionResult Setup_GetAction([FromQuery] string property)
        {
            InitSession();
            if (iSelModule != -1)
            {
                // Read requested value:
                if (UtlComponentModel.TryGetValue(NavActionBinder.PropertyResolver, lTempSettings[iSelModule], property, out var oValue))
                {
                    switch (oValue)
                    {
                        case string _sVal: return (Content(_sVal));

                        default:
                            return (BadRequest(string.Format("Type '{0}' of rquested value is not supported!", oValue.GetType().FullName)));
                    }
                }
            }
            return (BadRequest(string.Format("Requested property '{0}' not found!", property)));
        }
        [HttpPost]
        public IActionResult Setup_PostAction([ModelBinder(typeof(AmModuleSetupNavActionBinder))] NavAction aAction)
        {
            IActionResult iResult = RedirectToAction(nameof(Setup));
            Type _tType;

            Debug.Assert(aAction != null);

            InitSession();

            AmModuleSettings _aSelModuleSettings = null;
            if (iSelModule != -1)
            {
                _aSelModuleSettings = lTempSettings[iSelModule];

                // Update settings:
                aAction.UpdateModel(_aSelModuleSettings);
            }

            // Evaluate action:
            switch (aAction)
            {
                case AmAddModuleAction _aAdd:
                    _tType = AmModuleSettings.GetSettingsType(_aAdd.ModuleType);
                    _aSelModuleSettings = AmModuleSettings.GetSettingsDefaults(_tType);
                    lTempSettings.Add(_aSelModuleSettings);
                    iSelModule = lTempSettings.IndexOf(_aSelModuleSettings);
                    break;

                case AmSelectModuleAction _aSel:
                    iSelModule = _aSel.SelectedModule;
                    break;

                case AmRemoveModuleAction _aRemove:
                    if (iSelModule == -1)
                        return (Error());
                    else
                        lTempSettings.RemoveAt(iSelModule);
                    break;

                case AmSaveModuleAction _aSave:
                    // Save modified module settings:
                    ModuleSettings = lTempSettings;

                    // Clear temporary data:
                    lTempSettings = null;
                    iSelModule = -1;

                    // Close view:
                    iResult = Redirect("/");
                    break;

                default:
                    aAction.DoDefaultPost(_aSelModuleSettings);
                    break;
            }
            
            WriteSession();
            return (iResult);
        }
        [HttpPost]
        public IActionResult Import_PostAction(IFormFile File)
        {
            ResetSession();

            try
            {
                using (var _sReader = new StreamReader(File.OpenReadStream()))
                {
                    var _sImported = Context
                        .ImportModules(_sReader.ReadToEnd())
                        .Select(_aSetting => ("- " + _aSetting.Title));
                    
                    var _sText = "Successfully imported modules:\n" + string.Join('\n', _sImported);
                    aCoreService.UserNotices.Enqueue(new UserNotice(_sText, TITLE_MODULE_IMPORT, "", NoticeType.eSuccess));
                }
            }
            catch (Exception eExcpt)
            {
                Logger.LogError(307, eExcpt, "Failed to import modules!");
                aCoreService.UserNotices.Enqueue(new UserNotice("Operation failed (See logs for details).", TITLE_MODULE_IMPORT, "", NoticeType.eError));
            }

            return (RedirectToAction(nameof(Setup)));
        }
        [HttpPost]
        public FileResult Export_PostAction(int[] Modules)
        {
            // Export modules:
            var _sData = Context.ExportModuleSettings(Modules);

            // Write exported module settings to file:
            return (File(System.Text.Encoding.UTF8.GetBytes(_sData), "application/json", "modules.settings.json"));
        }
        #endregion


        private List<AmModuleSettings> lTempSettings;
        private int iSelModule;

        private AmCoreService aCoreService;
    }
}
