


using AutoMate.Models;
using Shared.Asp.GenNav;

namespace AutoMate.Actions
{
    #region Types.Actions
    public class AmSaveSettingsAction : NavAction
    {
        #region Constants
        public const string KEY = "save";
        #endregion


        public AmSaveSettingsAction(NavActionBinder nBinder, string sValue) : base(nBinder, KEY)
        {
        }


        #region Format
        public static string FormatName() => Format(KEY);
        #endregion
    }
    #endregion
    #region Types.Binder
    public class AmSettingsNavActionBinder : NavActionBinder<AmSettings>
    {
        public AmSettingsNavActionBinder()
        {
            SupportAction(typeof(AmSaveSettingsAction));
        }
    }
    #endregion
}