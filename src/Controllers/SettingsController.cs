using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using AutoMate.Actions;
using AutoMate.Data;
using AutoMate.Models;
using AutoMate.Modules.Script;
using AutoMate.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Shared.Asp.GenNav;
using Shared.Asp.UserNotice;
using Shared.Utils.Asp;
using Shared.Utils.Core;

namespace AutoMate.Controllers
{
    [Authorize]
    public class SettingsController : BaseController
    {
        #region Constants
        private const string TITLE_SAVE_SETTINGS = "Update settings";
        private const string TITLE_UPDATE_LOCAL = "Update local";
        #endregion


        public SettingsController(ApplicationDbContext aContext, AmCoreService aCoreService) : base(aContext)
        {
            this.aCoreService = aCoreService;
        }


        #region Views
        public IActionResult Index() => View(Env.Settings);
        #endregion


        #region Actions
        [HttpPost]
        public IActionResult SaveSettings_PostAction([ModelBinder(typeof(AmSettingsNavActionBinder))] NavAction aAction)
        {
            // Update module data:
            Action<Exception> ShowException = (_eExcpt) => {
                Logger.LogError(201, _eExcpt, string.Format("Failed to post '{0}' action!", aAction.Key));
                aCoreService.UserNotices.Enqueue(new UserNotice(_eExcpt.Message, TITLE_SAVE_SETTINGS, "Failed to parse input!", NoticeType.eError));
            };

            try
            {
                aAction.UpdateModel(Env.Settings);
                Context.WriteSettings();
            }
            catch (AggregateException aExcpt)
            {
                aExcpt.InnerExceptions.ForEach(_eExcpt => ShowException(_eExcpt));
            }
            catch (Exception eExcpt)
            {
                ShowException(eExcpt);
            }

            return (Redirect("/"));
        }
        [HttpPost]
        public IActionResult LocalUpdate_PostAction(bool restartModules)
        {
            // Update local folder:
            try
            {
                Env.Local.Update();
            }
            catch (Exception eExcpt)
            {
                Logger.LogError(73, eExcpt, "Failed to update local folder!");
                aCoreService.UserNotices.Enqueue(new UserNotice(eExcpt.Message, TITLE_UPDATE_LOCAL, "Operation failed (See logs for details).", NoticeType.eError));

                restartModules = false;
            }

            if (restartModules)
                aCoreService.RestartAsync(CancellationToken.None);

            return (Redirect("/"));
        }
        #endregion


        private AmCoreService aCoreService;
    }
}
