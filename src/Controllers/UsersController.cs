using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using AutoMate.Data;
using AutoMate.Models;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Authorization;

namespace AutoMate.Controllers
{
    [Authorize(Roles = Env.ROLE_ADMIN)]
    public class UsersController : BaseController
    {
        public UsersController(ApplicationDbContext aContext, IServiceProvider iServices) : base(aContext)
        {
            uUserManager = iServices.GetService<UserManager<IdentityUser>>();
        }


        #region Views
        public IActionResult Index() => View();
        #endregion
        #region Actions
        [HttpPost]
        public IActionResult SetRole(string sEmail, string sRole)
        {
            var _iUser = uUserManager.FindByEmailAsync(sEmail).Result;
            if (_iUser == null)
                return (NotFound());
            else if (!uUserManager.AddToRoleAsync(_iUser, sRole).Result.Succeeded)
                return (NotFound());
            else
                return (RedirectToAction(nameof(Index)));
        }
        [HttpPost]
        public IActionResult Delete(string sEmail)
        {
            var _iUser = uUserManager.FindByEmailAsync(sEmail).Result;
            if (_iUser == null)
                return (NotFound());
            else
            {
                var _tIsAdmin = uUserManager.IsInRoleAsync(_iUser, Env.ROLE_ADMIN);
                var _tAdminUsers = uUserManager.GetUsersInRoleAsync(Env.ROLE_ADMIN);
                Task.WaitAll(_tIsAdmin, _tAdminUsers);

                if ((_tIsAdmin.Result) && (_tAdminUsers.Result.Count <= 1))
                    return (NotFound("Unable to delete only administrator!"));
                else if (!uUserManager.DeleteAsync(_iUser).Result.Succeeded)
                    return (NotFound());
                else
                    return (RedirectToAction(nameof(Index)));
            }
        }
        #endregion


        private UserManager<IdentityUser> uUserManager;
    }
}
