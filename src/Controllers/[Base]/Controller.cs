using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using AutoMate.Data;
using AutoMate.Models;
using AutoMate.Modules;
using AutoMate.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace AutoMate.Controllers
{
    public abstract class BaseController : Controller
    {
        public BaseController(ApplicationDbContext aContext)
        {
            this.Context = aContext;
            this.Logger = Env.CreateLogger(GetType());
        }


        #region Properties.Settings
        public List<AmModuleSettings> ModuleSettings
        {
            set { Context.ModuleSettings = value; }
            get => Context.ModuleSettings;
        }
        #endregion
        #region Properties.Management
        protected ILogger Logger { init; get; }
        protected ApplicationDbContext Context { init; get; }
        #endregion


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return (View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier }));
        }
    }
}
