﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.Json;
using AutoMate.Models;
using AutoMate.Modules;
using AutoMate.Services;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Shared.Utils.Core;

namespace AutoMate.Data
{
    #region Types.Database
    [Table("Settings")]
    public class AmSettingsDataset
    {
        public AmSettingsDataset()
        {
            this.Data = Env.Settings.Write();
        }


        #region Properties.Management
        public int Id { get; set; }
        #endregion
        #region Properties
        public string Data { get; set; }
        #endregion
    }
    [Table("ModuleSettings")]
    public class AmModuleSettingsDataset
    {
        public AmModuleSettingsDataset()
        {
        }
        public AmModuleSettingsDataset(AmModule aModule) : this(aModule.GetSettings())
        {
        }
        public AmModuleSettingsDataset(AmModuleSettings aSettings)
        {
            this.SettingsType = aSettings.GetType().FullName;
            this.Data = aSettings.Write();
        }


        #region Properties.Management
        public int Id { get; set; }
        #endregion
        #region Properties
        public string SettingsType { get; set; }
        public string Data { get; set; }
        #endregion


        #region Management
        internal AmModuleSettings CreateSettings() => AmModuleSettings.Read(SettingsType, Data);
        #endregion
    }
    [Table("ModulePersistence")]
    public class AmModulePersistenceDataset
    {
        public AmModulePersistenceDataset()
        {
        }
        public AmModulePersistenceDataset(AmModule aModule)
        {
            this.ModuleId = aModule.Identifier;
            this.PersistentData = aModule.WritePersistence();
        }


        #region Properties.Management
        [Browsable(false)]
        public bool HasData => !string.IsNullOrEmpty(PersistentData);
        #endregion
        #region Properties.Management
        public int Id { get; set; }
        #endregion
        #region Properties
        public long ModuleId { get; set; }
        public string PersistentData { get; set; }
        #endregion
    }
    #endregion
    #region Types
    public class ApplicationDbContext : IdentityDbContext
    {
        #region Constants
        private const string JSON_PROP_MODULES = "Modules";
        private const string JSON_PROP_SETTINGS = "Settings";
        private const string JSON_PROP_PERSISTENT = "Persistent";
        private const string JSON_PROP_TYPE = "Type";
        private const string JSON_PROP_DATA = "Data";
        #endregion


        public ApplicationDbContext(ILogger<ApplicationDbContext> logger, DbContextOptions<ApplicationDbContext> options, IConfiguration configuration, AmCoreService aCoreService) : base(options)
        {
            this.iLogger = logger;
            this.iConfig = configuration;
            this.aCoreService = aCoreService;
            
            // Execute pending migrations:
            Database.Migrate();

            // [Hint]
            // Settings are stored in the database wich is read at this point.
            // > Hence that, components cannot acces consistent settings (e.g. log configuration) in early initialization!
            ReadSettings();

            ReadModuleSettings();
        }


        #region Properties.Dataset
        private DbSet<AmSettingsDataset> SettingsDatasets { set; get; }
        private DbSet<AmModuleSettingsDataset> ModuleSettingsDatasets { set; get; }
        private DbSet<AmModulePersistenceDataset> ModulePersistenceDatasets { set; get; }
        #endregion
        #region Properties.Settings
        public List<AmModuleSettings> ModuleSettings
        {
            set
            {
                lModuleSettings = value;
                WriteModuleSettings();

                aCoreService.ExecuteScopedServed(this, () => aCoreService.OnModuleSettingsChanged(value));
            }
            get => lModuleSettings;
        }
        private List<AmModuleSettings> lModuleSettings = new List<AmModuleSettings>();
        #endregion


        #region Events
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
             string _sConnection;
            
            _sConnection = iConfig.GetConnectionString("SqLiteConnection");
            if (!string.IsNullOrEmpty(_sConnection))
            {
                optionsBuilder.UseSqlite(_sConnection);
                SQLitePCL.Batteries.Init();

                return;
            }
            
            _sConnection = iConfig.GetConnectionString("DefaultConnection");
            if (!string.IsNullOrEmpty(_sConnection))
            {
                optionsBuilder.UseSqlServer(_sConnection);
                return;
            }
        }
        #endregion
        #region Management.Settings
        private void ReadSettings()
        {
            iLogger.LogTrace(167, "Reading settings.");

            try
            {
                var _aDataset = SettingsDatasets.FirstOrDefault();
                if (_aDataset != null)
                    Env.Settings = AmSettings.Read(_aDataset.Data);
            }
            catch (Exception eExcpt)
            {
                iLogger.LogError(173, eExcpt, "Failed to read settings.");
            }

        }
        public void WriteSettings() => WriteSettings(true);
        private void WriteSettings(bool bSaveChanges)
        {
            var _aDataset = SettingsDatasets.FirstOrDefault();
            if (_aDataset != null)
                SettingsDatasets.Remove(_aDataset);

            SettingsDatasets.Add(new AmSettingsDataset());

            if (bSaveChanges)
                SaveChanges();
        }
        #endregion
        #region Management.Modules.Settings
        private void ReadModuleSettings()
        {
            try
            {
                iLogger.LogTrace(148, "Reading module settings.");
                
                // Reset and create new settings:
                lModuleSettings.Clear();

                foreach (var _dDataset in ModuleSettingsDatasets)
                    lModuleSettings.Add(AmModuleSettings.Read(_dDataset.SettingsType, _dDataset.Data));
            }
            catch (Exception eExcpt)
            {
                iLogger.LogError(156, eExcpt, "Failed to read module settings.");
            }
        }
        private void WriteModuleSettings(AmModuleSettings aSettings, bool bSaveChanges = true)
        {
            // Remove to replace dataset:
            var _aOldDataset = ModuleSettingsDatasets
                .Where(_aDataset => (_aDataset.SettingsType == aSettings.GetType().FullName))
                .ToArray()
                .FirstOrDefault(_aDataset => (_aDataset.CreateSettings().Identifier == aSettings.Identifier));
            if (_aOldDataset != null)
                ModuleSettingsDatasets.Remove(_aOldDataset);

            // Create new dataset:
            ModuleSettingsDatasets.Add(new AmModuleSettingsDataset(aSettings));

            if (bSaveChanges)
                SaveChanges();
        }
        private void WriteModuleSettings(IEnumerable<AmModuleSettings> aSettings, bool bSaveChanges = true)
        {
            aSettings.ForEach(_aSettings => WriteModuleSettings(_aSettings, false));
            if (bSaveChanges)
                SaveChanges();
        }
        private void WriteModuleSettings()
        {
            // Reset and create new datasets:
            ModuleSettingsDatasets.RemoveRange(ModuleSettingsDatasets);

            lModuleSettings.ForEach(_aSetting => ModuleSettingsDatasets.Add(new AmModuleSettingsDataset(_aSetting)));
            SaveChanges();
        }
        #endregion
        #region Management.Modules.Persistence
        public bool ReadModulePersistence(AmModule aModule)
        {
            var _aDataset = ModulePersistenceDatasets.FirstOrDefault(_aDataset => (_aDataset.ModuleId == aModule.Identifier));
            if (_aDataset == null)
                return (false);
            else
            {
                aModule.ReadPersistence(_aDataset.PersistentData);
                return (true);
            }
        }
        public AmModulePersistenceDataset WriteModulePersistence(AmModule aModule, bool bSaveChanges = true)
        {
            var aDataset = ModulePersistenceDatasets.GetDataset(aModule.Identifier);

            var _sData = aModule.WritePersistence();
            if ((string.IsNullOrEmpty(_sData)) != (aDataset == null))
            {
                if (aDataset == null)
                {
                    // Create new dataset:
                    aDataset = new AmModulePersistenceDataset { ModuleId = aModule.Identifier };
                    ModulePersistenceDatasets.Add(aDataset);
                }
                else
                {
                    // Remove previously existing dataset:
                    ModulePersistenceDatasets.Remove(aDataset);
                    aDataset = null;
                }
            }
            if (aDataset != null)
                aDataset.PersistentData = _sData;
            if (bSaveChanges)
                SaveChanges();

            return (aDataset);
        }
        public void WriteModulePersistence(AmModulePersistenceDataset aDataset, bool bSaveChanges = true)
        {
            ModulePersistenceDatasets.RemoveDataset(aDataset.ModuleId);
            ModulePersistenceDatasets.Add(aDataset);

            if (bSaveChanges)
                SaveChanges();
        }
        public void WriteModulePersistence()
        {
            // Reset and create new datasets:
            ModulePersistenceDatasets.RemoveRange(ModulePersistenceDatasets);
            aCoreService.Modules.ForEach(_aModule =>
            {
                var _aDataset = new AmModulePersistenceDataset(_aModule);
                if (_aDataset.HasData)
                    ModulePersistenceDatasets.Add(_aDataset);
            });
            SaveChanges();
        }
        public void RemoveModulePersistence(AmModule aModule, bool bSaveChanges = true) => RemoveModulePersistence(aModule.Identifier, bSaveChanges);
        public void RemoveModulePersistence(long lModuleId, bool bSaveChanges = true)
        {
            if ((ModulePersistenceDatasets.RemoveDataset(lModuleId)) && (bSaveChanges))
                SaveChanges();
        }
        #endregion
        #region Management.Modules.ExportImport
        public ICollection<AmModuleSettings> ImportModules(string sData)
        {
            var _dUpdate = new Dictionary<AmModuleSettings, AmModulePersistenceDataset>();

            // Read data:
            using (var _jDoc = JsonDocument.Parse(sData))
            {
                JsonElement _jProp;

                _jProp = _jDoc.RootElement.GetProperty(JSON_PROP_MODULES);
                foreach (var _jModule in _jProp.EnumerateArray())
                {
                    // Read settings:
                    AmModuleSettings _aSettings = null;
                    _jProp = _jModule.GetProperty(JSON_PROP_SETTINGS);
                    {
                        _aSettings = AmModuleSettings.Read(
                            _jProp.GetPropertyValue<string>(JSON_PROP_TYPE),
                            _jProp.GetProperty(JSON_PROP_DATA).GetRawText()
                        );
                    }
                    
                    // Read persistent data:
                    AmModulePersistenceDataset _aPersist = null;
                    if (_jModule.TryGetProperty(JSON_PROP_PERSISTENT, out _jProp))
                    {
                        _aPersist = new AmModulePersistenceDataset()
                        {
                            ModuleId = _aSettings.Identifier,
                            PersistentData = _jProp.GetRawText()
                        };
                    }

                    _dUpdate.Add(_aSettings, _aPersist);
                }
            }

            // Import module settings:
            WriteModuleSettings(_dUpdate.Keys);

            // Import modules:
            aCoreService.ExecuteScopedServed(this, () => aCoreService.OnImportModules(_dUpdate));

            return (_dUpdate.Keys);
        }
        /// Export all modules.
        public string ExportModules() => ExportModules(aCoreService.Modules);
        /// Export selected modules.
        public string ExportModules(IEnumerable<AmModule> aSelected)
        {
            return (ExportModuleSettings(aSelected.ToTuple(
                _aItem1 => new AmModuleSettingsDataset(_aItem1),
                _aItem2 => WriteModulePersistence(_aItem2, false))));
        }
        /// Export modules of selected settings.
        public string ExportModuleSettings(IEnumerable<int> iIndex)
        {
            // Export selected settings:
            return (ExportModuleSettings(iIndex.ToTuple(
                _iIdx1 => {
                    // Get settings data:
                    return (new AmModuleSettingsDataset(ModuleSettings.ElementAt(_iIdx1)));
                },
                _iIdx2 => {
                    // Get persistent data:
                    var _aSettings = lModuleSettings.ElementAt(_iIdx2);
                    var _aModule = aCoreService.Modules.FirstOrDefault(_mModule => (_mModule.Identifier == _aSettings.Identifier));
                    if (_aModule == null)
                        // Read persitence from database:
                        return (ModulePersistenceDatasets.FirstOrDefault(_aDataset => (_aDataset.ModuleId == _aSettings.Identifier)));
                    else
                        // Update current persistence:
                        return (WriteModulePersistence(_aModule, false));
                })));
        }
        /// Export selected modules.
        public string ExportModuleSettings(IEnumerable<Tuple<AmModuleSettingsDataset, AmModulePersistenceDataset>> aSelected)
        {
            using (var _mStream = new MemoryStream())
            using (var _uWriter = new Utf8JsonWriter(_mStream, new JsonWriterOptions() { Indented = true }))
            {
                _uWriter.WriteStartObject();
                {
                    _uWriter.WriteStartArray(JSON_PROP_MODULES);
                    aSelected.ForEach(_tData =>
                    {
                        _uWriter.WriteStartObject();
                        {
                            // Write settings:
                            _uWriter.WriteStartObject(JSON_PROP_SETTINGS);
                            {
                                Debug.Assert((typeof(AmModuleSettingsDataset).GetProperties().Count() == 3), "[Debug] Check for unserialized properties!");

                                _uWriter.WriteString(JSON_PROP_TYPE, _tData.Item1.SettingsType);
                                _uWriter.WriteJson(JSON_PROP_DATA, _tData.Item1.Data);
                            }
                            _uWriter.WriteEndObject();
                            
                            // Update persistent data:
                            if ((_tData.Item2 != null) && (_tData.Item2.HasData))
                                // Write persistent data:
                                _uWriter.WriteJson(JSON_PROP_PERSISTENT, _tData.Item2.PersistentData);
                        }
                        _uWriter.WriteEndObject();

                    });
                    _uWriter.WriteEndArray();
                }
                _uWriter.WriteEndObject();
                _uWriter.Flush();
                
                // Write updates persistent data:
                SaveChanges();

                return (Encoding.UTF8.GetString(_mStream.ToArray()));
            }
        }
        #endregion


        protected readonly ILogger<ApplicationDbContext> iLogger;
        private IConfiguration iConfig;
        private AmCoreService aCoreService;
    }
    #endregion


    #region Extensions
    internal static class ExtDbSet
    {
        public static AmModulePersistenceDataset GetDataset(this DbSet<AmModulePersistenceDataset> sSource, long lModuleId)
        {
            return (sSource.FirstOrDefault(_aDataset => (_aDataset.ModuleId == lModuleId)));
        }
        public static bool RemoveDataset(this DbSet<AmModulePersistenceDataset> sSource, long lModuleId)
        {
            var _aDataset = sSource.GetDataset(lModuleId);
            if (_aDataset == null)
                return (false);
            else
            {
                sSource.Remove(_aDataset);
                return (true);
            }
        }
    }
    #endregion
}
