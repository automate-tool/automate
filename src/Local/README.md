# Description

This is the _local_ folder.
It is ignored to git.

Use it this way:
- Debugging environment
  Contents are **compiled** into application code and ready to be used in debug sessions.

- Productive environment
  Contents are **not compiled** (see the build configuration dependent item in *.csproj).
  Hence that, repositories can be cloned and contents (scripts) easily be updated and compiled at runtime.
