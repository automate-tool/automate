using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using AutoMate.Modules;
using Microsoft.Extensions.Logging;
using Shared.Utils.Core;
using Shared.Collections.Exposed;
using System.ComponentModel;
using Shared.Asp.Alert;
using Shared.Asp.GenNav;

namespace AutoMate.Event
{
    #region Attributes
    internal class EventAttribute : Attribute
    {
        public EventAttribute(Type tType)
        {
            if (tType.IsAssignableTo(typeof(AmEvent)))
                this.Type = tType;
            else
                throw new TypeLoadException(string.Format("'{0}' is not a valid event attribute!", tType.Name));
        }


        #region Properties
        public Type Type { get; }
        #endregion
    }
    internal class LogLevelAttribute : Attribute
    {
        public LogLevelAttribute(LogLevel lLevel)
        {
            this.Level = lLevel;
        }


        #region Properties
        public LogLevel Level { get; }
        #endregion
    }
    #endregion
    #region Enumerations
    public enum AmEventType
    {
        [Description("INF")]
        [AlertLayout(AlertLayout.eInfo, AlertIcon.eInfo)]
        [LogLevel(LogLevel.Information)]
        [Event(typeof(AmInfoEvent))]
        eInfo,
        [Description("WRN")]
        [AlertLayout(AlertLayout.eWarning, AlertIcon.eExclamation)]
        [LogLevel(LogLevel.Warning)]
        [Event(typeof(AmWarningEvent))]
        eWarning,
        [Description("ERR")]
        [AlertLayout(AlertLayout.eDanger, AlertIcon.eExclamation)]
        [LogLevel(LogLevel.Error)]
        [Event(typeof(AmErrorEvent))]
        eError,
        [Description("CRT")]
        [AlertLayout(AlertLayout.eDanger, AlertIcon.eExclamation)]
        [LogLevel(LogLevel.Critical)]
        [Event(typeof(AmCriticalEvent))]
        eCritical
    }
    #endregion


    #region Types.Actions
    public class AmAcknowledgeEventAction : NavAction
    {
        #region Constants
        public const string KEY = "ackEvent";
        #endregion


        public AmAcknowledgeEventAction(NavActionBinder nBinder, string sValue) : base(nBinder, KEY)
        {
            AmEventFactory.TryGetEvent(ulong.Parse(sValue), out var _iEvent);
            this.Event = _iEvent;
        }

        
        #region Properties.Management
        public bool Valid => (Event != null);
        #endregion
        #region Properties
        public IAmEvent Event { get; }
        #endregion


        #region Format
        public static string FormatName(ulong ulEventId) => Format(KEY, ulEventId);
        #endregion
    }
    #endregion
    #region Types.Event
    public class EventChangedEventArgs : EventArgs
    {
        public EventChangedEventArgs(IAmEvent iEvent)
        {
            this.Event = iEvent;
        }


        #region Properties
        public IAmEvent Event { get; }
        #endregion
    }
    #endregion
    #region Types
    public interface IAmEventProvider : IAmContext
    {
        #region Properties.Management
        IEnumerable<IAmEvent> Events => GetEvents();
        AmEventType MostCriticalType => Events.Select(_iEvent => _iEvent.Type).Max();
        #endregion


        #region Management
        IEnumerable<IAmEvent> GetEvents() => AmEventFactory.GetEvents(this).Values;
        #endregion
    }

    
    public interface IAmEvent
    {
        #region Properties.Management
        IAmContext Owner { get; }
        AmEventType Type { get; }
        AlertLayout Layout { get; }
        AlertIcon Icon { get; }

        bool Active { get; }
        #endregion
        #region Properties
        ulong Id { get; }
        EventId Identifier { get; }
        DateTime Timestamp { get; }
        string Message { get; }
        #endregion


        #region Management
        void Acknowledge();
        #endregion
    }
    internal abstract class AmEvent : IAmEvent
    {
        protected AmEvent(IAmContext iOwner, EventId eIdentifier, string sMessage)
        {
            this.Id = ++ulId;
            this.Owner = iOwner;
            this.Timestamp = DateTime.Now;
            this.Identifier = eIdentifier;
            this.Message = sMessage;
            this.lLayout = UtlEnum.GetCustomAttribute<AlertLayoutAttribute>(Type);

            this.Logger = (ILogger)Owner.GetType().BaseType
                .GetProperty("Logger", (BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance))
                .GetValue(Owner);

            WriteLog(Message);
        }


        #region Properties.Management
        private ILogger Logger { get; }

        public IAmContext Owner { get; }
        public abstract AmEventType Type { get; }
        public AlertLayout Layout => lLayout.Layout;
        public AlertIcon Icon => lLayout.Icon;
        private AlertLayoutAttribute lLayout;
        public LogLevel LogLevel => UtlEnum.GetCustomAttribute<LogLevelAttribute>(Type).Level;

        public bool Active => AmEventFactory.GetEvents(Owner).Values.Contains(this);
        #endregion
        #region Properties
        public ulong Id { get; }
        public EventId Identifier { get; }
        public DateTime Timestamp { get; }
        public string Message { get; }
        #endregion


        #region Management
        public void Acknowledge() => AmEventFactory.Acknowledge(this);
        #endregion


        #region Helper
        internal void WriteLog(string Message) => Logger.Log(LogLevel, Identifier, Message);
        #endregion


        public override string ToString() => string.Format("{0} [{1:HH:mm:ss}] {2}. {3}", UtlEnum.GetEnumDescription(Type), Timestamp, Identifier.Id, Message);


        private static ulong ulId = 0;
    }
    public static class AmEventFactory
    {
        #region Types
        private class AmEventLock : AmLock
        {
            #region Properties
            public override string Name { get; } = "Event context";
            #endregion
        }
        #endregion


        static AmEventFactory()
        {
            dEventTypeMap = Assembly.GetExecutingAssembly()
                .ExportedTypes
                .Where(_tType => _tType.IsAssignableTo(typeof(AmEvent)))
                .ToDictionary(
                    _tItem => _tItem.GetInterfaces().First(_tItf => _tItf.IsAssignableTo(typeof(IAmEvent))),
                    _tItem => _tItem.DeclaringType
                );
        }


        #region Properties.Management
        private static AmEventLock Lock { get; } = new AmEventLock();

        internal static IEnumerable<IAmEvent> Events
        {
            get
            {
                foreach (var _dEvents in dEvents.Values)
                {
                    foreach (var _iEvent in _dEvents.Values)
                        yield return (_iEvent);
                }
                yield break;
            }
        }
        private static IEnumerable<IAmVariable> Io
        {
            get
            {
                if (iIoAcknowledge != null)
                    yield return (iIoAcknowledge);
                if (iIoLed != null)
                    yield return (iIoLed);
                yield break;
            }
        }
        #endregion


        #region Events
        public static event EventHandler<EventChangedEventArgs> EventCreated;
        public static event EventHandler<EventChangedEventArgs> EventAcknowledged;
        #endregion
        #region Events
        private static void OnAcknowledge(object oSender, ValueChangedEventArgs vArgs)
        {
            if (ValueChangedFlags.Impulse.TestFlag(vArgs.Flags))
                Acknowledge(oSender);
        }
        #endregion


        #region Initialization
        /// <summary>
        /// Configures I/O variables for indication and acknowledgement of events.
        /// </summary>
        public static void ConfigureIo(IAmInputVariable<bool> iAcknowledge = null, IAmOutputVariable<bool> iLed = null)
        {
            // De-initialize previously assigned I/O's:
            if (iIoAcknowledge != null)
                iIoAcknowledge.ValueChanged -= OnAcknowledge;
            if (iIoLed != null)
                iIoLed.SetValue(false, Lock.LockContext);

            Io.ForEach(_iVar => _iVar.Unlock(Lock.LockContext));

            // Initialize new I/O's:
            iIoAcknowledge = iAcknowledge;
            iIoLed = iLed;

            if (iIoAcknowledge != null)
                iIoAcknowledge.ValueChanged += OnAcknowledge;

            Io.ForEach(_iVar => _iVar.Lock(Lock.LockContext));

            UpdateLed();
        }
        #endregion
        #region Management
        internal static IReadOnlyDictionary<int, IAmEvent> GetEvents(IAmContext iContext) => dEvents.GetOrCreateValue(iContext);
        internal static bool TryGetEvent(ulong ulId, out IAmEvent iEvent)
        {
            foreach (var _dEvents in dEvents.Values)
            {
                iEvent = _dEvents.Values.FirstOrDefault(_iEvent => (_iEvent.Id == ulId));
                if (iEvent != null)
                    return (true);
            }
            iEvent = null;
            return (false);
        }

        internal static IAmEvent Create(AmEventType aType, IAmContext iOwner, EventId eId, string sMessage)
        {
            var _eAttrib = UtlEnum.GetCustomAttribute<EventAttribute>(aType);
            return (Create(_eAttrib.Type, iOwner, eId, sMessage));
        }
        /*
        internal static IAmEvent Create<TEvent>(IAmContext iOwner, EventId eId, string sMessage)
            where TEvent : IAmEvent
        {
            var _tType = typeof(TEvent);
            if (dEventTypeMap.TryGetValue(_tType, out var _tEvent))
                return (Create(_tType, iOwner, eId, sMessage));
            else
                throw new TypeLoadException(string.Format("Failed to create event of unknown type '{0}'!", _tType.Name));
        }
        */
        private static IAmEvent Create(Type tType, IAmContext iOwner, EventId eId, string sMessage)
        {
            if (iOwner == null)
                throw new NullReferenceException("Failed to create event of undefined owner!");
            else
            {
                // Create event:
                var _iEvent = (IAmEvent)Activator.CreateInstance(tType, new object[] { iOwner, eId, sMessage });
                dEvents
                    .GetOrCreateValue(iOwner)
                    .AddOrUpdate(eId.Id, _iEvent);

                UpdateLed();

                EventCreated?.Invoke(iOwner, new(_iEvent));

                return (_iEvent);
            }
        }

        private static void _Acknowledge(IAmEvent iEvent, object oSender = null)
        {
            if (iEvent.Active)
            {
                dEvents[iEvent.Owner].Remove(iEvent.Identifier.Id);

                (iEvent as AmEvent).WriteLog("Acknowledged.");

                EventAcknowledged?.Invoke(oSender, new(iEvent));
            }
        }
        /// <summary>
        /// Acknowledges an event.
        /// </summary>
        public static void Acknowledge(IAmEvent iEvent, object oSender = null)
        {
            _Acknowledge(iEvent, oSender);
            UpdateLed();
        }
        /// <summary>
        /// Acknowledges all active events.
        /// </summary>
        public static void Acknowledge(object oSender = null)
        {
            Events.ForEach(_iEvent => _Acknowledge(_iEvent, oSender));
            UpdateLed();
        }
        #endregion


        #region Helper
        private static void UpdateLed()
        {
            if (iIoLed != null)
                iIoLed.SetValue(Events.Any(_iEvent => (_iEvent.Type >= AmEventType.eError)), Lock.LockContext);
        }
        #endregion


        private static IReadOnlyDictionary<Type, Type> dEventTypeMap = null;
        private static Dictionary<IAmContext, Dictionary<int, IAmEvent>> dEvents = new ();

        private static IAmInputVariable<bool> iIoAcknowledge;
        private static IAmOutputVariable<bool> iIoLed;
    }
    public interface IAmInfoEvent : IAmEvent
    {
    }
    internal class AmInfoEvent : AmEvent, IAmInfoEvent
    {
        public AmInfoEvent(IAmContext iOwner, EventId eId, string sMessage) : base(iOwner, eId, sMessage)
        {
        }


        #region Properties.Management
        public override AmEventType Type => AmEventType.eInfo;
        #endregion
    }
    public interface IAmWarningEvent : IAmEvent
    {
    }
    internal class AmWarningEvent : AmEvent, IAmWarningEvent
    {
        public AmWarningEvent(IAmContext iOwner, EventId eId, string sMessage) : base(iOwner, eId, sMessage)
        {
        }


        #region Properties.Management
        public override AmEventType Type => AmEventType.eWarning;
        #endregion
    }
    public interface IAmErrorEvent : IAmEvent
    {
    }
    internal class AmErrorEvent : AmEvent, IAmErrorEvent
    {
        public AmErrorEvent(IAmContext iOwner, EventId eId, string sMessage) : base(iOwner, eId, sMessage)
        {
        }


        #region Properties.Management
        public override AmEventType Type => AmEventType.eError;
        #endregion
    }
    public interface IAmCriticalEvent : IAmEvent
    {
    }
    internal class AmCriticalEvent : AmEvent, IAmCriticalEvent
    {
        public AmCriticalEvent(IAmContext iOwner, EventId eId, string sMessage) : base(iOwner, eId, sMessage)
        {
        }


        #region Properties.Management
        public override AmEventType Type => AmEventType.eCritical;
        #endregion
    }
    #endregion


    #region Extensions
    public static class ExtEvent
    {
        public static IAmEvent CreateEvent(this IAmEventProvider iOwner, AmEventType aType, EventId eId, string sMessage) => AmEventFactory.Create(aType, iOwner, eId, sMessage);
    }
    #endregion
}