using System;
using System.ComponentModel;
using System.IO;
using AutoMate.Logger;
using AutoMate.Local;
using Microsoft.Extensions.Logging;
using Serilog.Core;

namespace AutoMate
{
    public class Env
    {
        #region Constants
        public const string ROLE_ADMIN = "Administrator";
        public const string ROLE_RESIDENT = "Resident";

        public static readonly string LOG_PATH = Path.Combine(Directory.GetCurrentDirectory(), "Logs");
        public static readonly string LOG_FILENAME = "log.debug";
        public static readonly string LOG_FILEEXT = "txt";
        #endregion


        #region Properties.Log
        /// Minimum level of general logged events.
        public static LoggingLevelSwitch GeneralLogLevel { get; } = new();
        /// Minimum level of logged automate-events.
        public static LoggingLevelSwitch AutomateLogLevel { get; } = new();
        /// Level of displaed events in a log view.
        public static LoggingLevelSwitch FilteredLogLevel { get; } = new();
        #endregion
        #region Properties.Management
        public static Models.AmSettings Settings { internal set; get; } = new();
        public static LocalFolder Local { get; } = new();
        #endregion


        #region Initialization
        public static void Init(ILoggerFactory iLoggerFactory)
        {
            Env.iLoggerFactory = iLoggerFactory;

            Local.Init();
        }
        #endregion
        #region Log
        public static ILogger CreateLogger(string sCategoryName, string sId = "") => iLoggerFactory.CreateLogger(ModuleLogSink.FormatContext(sCategoryName, sId));
        public static ILogger CreateLogger(Type tType, string sId = "") => iLoggerFactory.CreateLogger(ModuleLogSink.FormatContext(tType, sId));
        #endregion


        private static ILoggerFactory iLoggerFactory;
    }
}