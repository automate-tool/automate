using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using CSScriptLib;
using Microsoft.Extensions.Logging;
using Shared.Source.Git;
using Shared.Utils.Core;


namespace AutoMate.Local
{
    public class LocalFolder
    {
        #region Constants
        private static readonly string BINARIES_FOLDER = Path.Combine(Env.Local.Location, "bin");
        #endregion


        #region Properties.Management
        protected ILogger Logger { private set; get; }
        public IAmRepository[] Repositories => aRepositories;
        private AmRepository[] aRepositories { set; get; }

        public Assembly CompiledAssembly { private set; get; }
        #endregion
        #region Properties
        public string Location { private set; get; }
        #endregion


        #region Management.Git
        internal void Init()
        {
            if (Logger != null)
                throw new Exception(string.Format("Detected use of multiple '{0}' instances wich is illegal!", typeof(LocalFolder)));
            else
            {
                this.Logger = Env.CreateLogger(typeof(LocalFolder));
                this.Location = Path.GetFullPath("Local");

                // Determine existing repositories:
                Logger.LogInformation(34, "Enumerating repositories...");
                aRepositories = Git.EnumerateReporitories<AmRepository>(Location).ToArray();

                // Filter repositories to protect other repositories from damage while debugging:
                #if DEBUG
                var WHITELIST = new string[] { "debug-test" };
                aRepositories = aRepositories.Where(_rRepo => WHITELIST.Contains(Path.GetFileName(_rRepo.Path)?.ToLower())).ToArray();
                #endif

                foreach (var _aRepo in aRepositories)
                {
                    Logger.LogTrace(43, string.Format("Located repository at '{0}'", _aRepo.Path));

                    _aRepo.EnumerateFiles();
                }
            }
        }
        public void Update()
        {
            Logger.LogInformation(49, "Updating repositories...");
            #if DEBUG
                Logger.LogWarning(51, "Remember: Updated scripts won't be loaded at runtime in debug-mode!");
            #endif

            foreach (var _aRepo in aRepositories)
            {
                _aRepo.DiscardChanges();
                _aRepo.Pull();

                Logger.LogTrace(55, string.Format("Updated repository '{0}'.", _aRepo));

                _aRepo.EnumerateFiles();
            }
        }
        private void EnumerateFiles()
        {
            aRepositories.ForEach(_aRepo => _aRepo.EnumerateFiles());
        }
        #endregion
        #region Management.Script
        public bool CompileScripts()
        {
            var _iFiles = Repositories.SelectMany(_iRepo => _iRepo.ScriptFiles);
            if (_iFiles.IsEmpty())
            {
                Logger.LogWarning(73, string.Format("No script files found in local repositories."));
                return (false);
            }
            else
            {
                Logger.LogInformation(97, string.Format("Loading {0} script files from local repository.", _iFiles.Count()));
                _iFiles.ForEach(_sFile => Logger.LogTrace(98, string.Format("Enumerated local script file '{0}'.", _sFile)));

                // Step 1) Remove depricated assembly:
                if (CompiledAssembly != null)
                {
                    CompiledAssembly = null;
                    GC.Collect();
                }

                // Step 2) Combine scripts to a single file that can be compiled.
                // > Separate 'using' area from code.
                var _hUse = new HashSet<string>();
                var _sSrc = new StringBuilder();
                var _rPattern = new Regex(@"using[ 	]+(.*);");
                foreach (var _sFile in _iFiles)
                {
                    var _bFindUse = true;
                    foreach (var _sLine in File.ReadAllLines(_sFile))
                    {
                        // Filter used namespaces (Skip duplicates):
                        if (_bFindUse)
                        {
                            var _mRes = _rPattern.Match(_sLine);
                            if (_bFindUse = _mRes.Success)
                                _hUse.Add(_mRes.Groups.ElementAt(1).ToString());
                        }
                        
                        if (!_bFindUse)
                            _sSrc.AppendLine(_sLine);
                    }
                }
                
                // Step 3) Determine new namespace:
                var _sRootClass = string.Format("AmLocal{0}", uiAsmNamespace++);

                // Step 4) Compile assembly from scripts:
                Directory.CreateDirectory(BINARIES_FOLDER);
                
                var _sUse = string.Join(Environment.NewLine, _hUse.Select(_sKey => string.Format("using {0};", _sKey)));
                var _sScript = (_sUse + Environment.NewLine + _sSrc.ToString());
                var _sWatch = Stopwatch.StartNew();
                {
                    CompiledAssembly = CSScript.Evaluator.CompileCode(_sScript, new CompileInfo() {
                        RootClass = _sRootClass,
                        AssemblyFile = Path.Combine(BINARIES_FOLDER, (_sRootClass + ".dll"))
                    });
                }
                _sWatch.Stop();
                Logger.LogInformation(129, string.Format("Compiled local script assembly in '{0}'.", _sWatch.Elapsed));

                return (true);
            }
        }
        #endregion


        private static uint uiAsmNamespace = 1;
    }
    public interface IAmRepository
    {
        #region Properties
        public string Name { init; get; }
        public string Path { init; get; }
        public IReadOnlyList<string> ScriptFiles { get; }
        #endregion
    }
    internal class AmRepository : Git.Repository, IAmRepository
    {
        #region Constants
        private const string PATTERN = "*.cs";
        #endregion


        public AmRepository(string sPath) : base(sPath)
        {
            this.Logger = Env.CreateLogger(typeof(LocalFolder), Name);
        }


        #region Properties.Management
        protected ILogger Logger { private set; get; }
        #endregion
        #region Properties
        public IReadOnlyList<string> ScriptFiles => sScriptFiles;
        private List<string> sScriptFiles { set; get; }
        #endregion


        #region Management
        public void EnumerateFiles()
        {
            sScriptFiles = Directory.EnumerateFiles(Path, PATTERN, new EnumerationOptions() { RecurseSubdirectories = true }).ToList();
            sScriptFiles.ForEach(_sFile => Logger.LogTrace(221, string.Format("Enumerated script file '{0}'.", _sFile)));
        }
        #endregion
    }
}