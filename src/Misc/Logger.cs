
// Queues logs at runtime.
// Tutorials:
// - https://github.com/serilog/serilog/wiki/Configuration-Basics
// - https://github.com/serilog/serilog-sinks-trace

using System;
using Serilog;
using Serilog.Configuration;
using Serilog.Core;
using Serilog.Events;
using Serilog.Formatting.Display;
using Serilog.Formatting;
using Serilog.Formatting.Json;
using Shared.Utils.Core;
using System.Diagnostics;
using System.Collections.Generic;
using Microsoft.Extensions.Logging;
using System.ComponentModel;
using AutoMate.Modules;
using System.IO;
using System.Linq;

namespace AutoMate.Logger
{
    #region Types.Sinks
    /// Captures log events at runtime to provide logs, separated per module.
    public class ModuleLogSink : ILogEventSink
    {
        #region Constants
        private const char PROPERTY_SEPARATOR = '\'';
        #endregion


        public ModuleLogSink(ITextFormatter iTextFormatter, int retainedDayLimit = 0)
        {
            if (iTextFormatter == null)
                throw new ArgumentNullException(nameof(iTextFormatter));

            this.Formatter = iTextFormatter;
            RetainedDayLimit = retainedDayLimit;
        }


        #region Properties.Management
        private ITextFormatter Formatter { init; get; }
        #endregion
        #region Properties
        public static int RetainedDayLimit { private set; get; }
        private static DateTime dRtndDayLmtUpdate = default;
        public static IReadOnlyDictionary<long, Queue<Tuple<LogEventLevel, DateTimeOffset, string>>> Events
        {
            get
            {
                if ((RetainedDayLimit > 0) && ((DateTime.Now - dRtndDayLmtUpdate).TotalMinutes >= 1))
                {
                    // Remove expired events:
                    dRtndDayLmtUpdate = DateTime.Now;
                    dEvents.Values
                        .Where(_qQueue => !_qQueue.IsEmpty())
                        .ForEach(_qQueue => {
                            var _iToRemove = _qQueue.TakeWhile(_tEvent => ((DateTime.Now - _tEvent.Item2.DateTime).TotalDays > RetainedDayLimit)).Count();
                            for (int i = 0; i < _iToRemove; i++)
                                _qQueue.Dequeue();
                        });
                }
                return (dEvents);
            }
        }
        private static Dictionary<long, Queue<Tuple<LogEventLevel, DateTimeOffset, string>>> dEvents = new();
        #endregion
        

        #region Format
        public static string FormatContext(string sCategoryName, string sId = "")
        {
            if (!string.IsNullOrEmpty(sId))
                sCategoryName += (PROPERTY_SEPARATOR + sId);
            return (sCategoryName);
        }
        public static string FormatContext(Type tType, string sId = "") => FormatContext(tType.FullName, sId);
        #endregion
        #region Log
        public void Emit(LogEvent lLogEvent)
        {
            if (lLogEvent == null)
                throw new ArgumentNullException(nameof(lLogEvent));

            // Log all modules events:
            if (lLogEvent.Properties.TryGetValue("SourceContext", out var _lVal))
            {
                var _sContext = _lVal.ToString().Trim('"');
                if (_sContext.StartsWith(typeof(AmModule).Namespace))
                {
                    var _iIdx = _sContext.LastIndexOf(PROPERTY_SEPARATOR);
                    if (_iIdx != -1)
                    {
                        using (var _sWriter = new StringWriter())
                        {
                            var _lId = long.Parse(_sContext.Remove(0, (_iIdx+1)));
                            Formatter.Format(lLogEvent, _sWriter);

                            dEvents.GetOrCreateValue(_lId).Enqueue(new (lLogEvent.Level, lLogEvent.Timestamp, _sWriter.ToString().Trim()));
                        }
                    }
                }
            }
        }
        #endregion
    }
    #endregion
    #region Types.Enrichers
    /// Enricher for 'EventId' to be well formated when used in a output template.
    public class EventIdEnricher : ILogEventEnricher
    {
        #region Constants
        private const string PROP_EVENTID = "EventId";
        private const string PROP_ID = "Id";
        #endregion


        #region Management
        public void Enrich(LogEvent logEvent, ILogEventPropertyFactory propertyFactory)
        {
            if (logEvent.Properties.TryGetValue(PROP_EVENTID, out LogEventPropertyValue _lVal))
            {
                var iId = -1;

                // Extract event Id:
                try
                {
                    var _sVal = (StructureValue)_lVal;
                    var _lIdProp = _sVal.Properties[0];
                    if (_lIdProp.Name != PROP_ID)
                        throw new InvalidDataException(string.Format("Found unexpected property '{0}'!", _lIdProp.Name));
                    var _sScalarVal = (ScalarValue)_lIdProp.Value;
                    iId = (int)_sScalarVal.Value;
                }
                catch (Exception)
                {
                }

                // Simplify and update property:
                logEvent.AddOrUpdateProperty(propertyFactory.CreateProperty(PROP_EVENTID, iId));
            }
        }
        #endregion
    }
    #endregion


    #region Extensions
    public static class ExtLoggerSinkConfiguration
    {
        #region Constants
        private const string DEFAULT_TEMPLATE = "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [{Level}] {Message}{NewLine}{Exception}";
        #endregion


        public static LoggerConfiguration ModuleLog(this LoggerSinkConfiguration sinkConfiguration, int retainedDayLimit = 0, LogEventLevel restrictedToMinimumLevel = LevelAlias.Minimum, string outputTemplate = DEFAULT_TEMPLATE, IFormatProvider formatProvider = null, LoggingLevelSwitch levelSwitch = null)
        {
            if (sinkConfiguration == null)
                throw new ArgumentNullException(nameof(sinkConfiguration));
            else if (outputTemplate == null)
                throw new ArgumentNullException(nameof(outputTemplate));
            else
            {
                var formatter = new MessageTemplateTextFormatter(outputTemplate, formatProvider);
                return (ModuleLog(sinkConfiguration, formatter, retainedDayLimit, restrictedToMinimumLevel, levelSwitch));
            }
        }
        public static LoggerConfiguration ModuleLog(this LoggerSinkConfiguration sinkConfiguration, ITextFormatter formatter, int retainedDayLimit = 0, LogEventLevel restrictedToMinimumLevel = LevelAlias.Minimum, LoggingLevelSwitch levelSwitch = null)
        {
            if (sinkConfiguration == null)
                throw new ArgumentNullException(nameof(sinkConfiguration));
            else if (formatter == null)
                throw new ArgumentNullException(nameof(formatter));
            else
                return (sinkConfiguration.Sink(new ModuleLogSink(formatter, retainedDayLimit), restrictedToMinimumLevel, levelSwitch));
        }
    }
    public static class ExtEnrichers
    {
        public static LoggerConfiguration BeautifyEventId(this LoggerEnrichmentConfiguration enrich)
        {
            if (enrich == null)
                throw new ArgumentNullException(nameof(enrich));
            else
                return (enrich.With<EventIdEnricher>());
        }
    }
    #endregion
}