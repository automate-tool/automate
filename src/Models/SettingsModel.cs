using System;
using System.ComponentModel;
using System.Text.Json;
using AutoMate.Modules;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Serilog.Events;


namespace AutoMate.Models
{
    public class AmSettings
    {
        #region Constants
        private const string CAT_LOGLEVEL = "Log level";
        #endregion


        #region Properties.Log
        [Category(CAT_LOGLEVEL)]
        [Description("Minimum log level of general events.")]
        [DisplayName("General")]
        public LogEventLevel GeneralLogLevel { set { Env.GeneralLogLevel.MinimumLevel = value; } get => Env.GeneralLogLevel.MinimumLevel; }
        [Category(CAT_LOGLEVEL)]
        [Description("Minimum log level of <i>Automate</i> related events.")]
        [DisplayName("Automate")]
        public LogEventLevel AutomateLogLevel { set { Env.AutomateLogLevel.MinimumLevel = value; } get => Env.AutomateLogLevel.MinimumLevel; }
        [Category(CAT_LOGLEVEL)]
        [Description("Minimum log level of filtered events to be displayed in log views.")]
        [DisplayName("Filtered")]
        public LogEventLevel FilteredLogLevel { set { Env.FilteredLogLevel.MinimumLevel = value; } get => Env.FilteredLogLevel.MinimumLevel; }
        #endregion


        #region Management
        /// Reads settings.
        public static AmSettings Read(string sData)
        {
            return (JsonSerializer.Deserialize<AmSettings>(sData));
        }
        /// Writes settings.
        public string Write()
        {
            return (JsonSerializer.Serialize(this, typeof(AmSettings)));
        }
        #endregion
    }
}
