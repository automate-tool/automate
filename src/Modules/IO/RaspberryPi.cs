
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Threading;
using AutoMate.Models;
using AutoMate.Services;
using Microsoft.AspNetCore.Mvc;
using Shared.Utils.Core;
using System.Device.Gpio;
using Shared.Asp.GenNav;
using System.Text.Json.Serialization;
using static Shared.Utils.Core.UtlJson;
using System.Linq;
using System.Diagnostics;
using Microsoft.Extensions.Logging;

namespace AutoMate.Modules.IO
{
    public class AmRaspberryPiSettings : AmModuleSettings
    {
        #region Constants
        public static readonly AmRaspberryPiSettings Default = new AmRaspberryPiSettings()
        {
            Title = "Raspberry Pi Module"
        };
        #endregion


        #region Types
        [AllowedInputAttribute(nameof(VariableType), AmVariableType.eInput, AmVariableType.eOutput)]
        [AllowedInputAttribute(nameof(DataType), AmDataType.eBoolean)]
        public class AmPinSettings : AmVariableSettings
        {
            #region Properties
            [DisplayName("Pin")]
            [Description("")]
            public int Pin { set; get; }
            #endregion
        }
        #endregion


        public AmRaspberryPiSettings()
        {
        }


        [Category("GPIO")]
        [Description("Configure in- and output pins.")]
        [DisplayName("Pins")]
        [CollectionItemDisplayName("Pin")]
        public List<AmPinSettings> IO { set; get; } = new List<AmPinSettings>();
    }
    
    [ModuleDescription(AmModuleCategory.eIO, "Raspberry Pi", "raspberry.pi", 1,0,0)]
    public partial class AmRaspberryPiModule : AmModule<AmRaspberryPiSettings>
    {
        #region Constants
        private const string DatasetGpio = "GPIO's";

        private static readonly Dictionary<AmVariableType, PinMode> PINMODE_MAP = new Dictionary<AmVariableType, PinMode>()
        {
            { AmVariableType.eInput, PinMode.Input },
            { AmVariableType.eOutput, PinMode.Output }
        };
        private static readonly Dictionary<PinEventTypes, bool> PINEVENT_MAP = new Dictionary<PinEventTypes, bool>()
        {
            { PinEventTypes.Falling, false },
            { PinEventTypes.Rising, true }
        };
        private static readonly Dictionary<bool, PinValue> PINVALUE_MAP = new Dictionary<bool, PinValue>()
        {
            { false, PinValue.Low },
            { true, PinValue.High }
        };
        #endregion


        public AmRaspberryPiModule(AmCoreService aOwner, AmRaspberryPiSettings aSettings) : base(aOwner, aSettings)
        {
        }
        public AmRaspberryPiModule(AmCoreService aOwner) : this(aOwner, AmRaspberryPiSettings.Default)
        {
        }


        #region Events.Initialization
        public override bool OnInitModule(bool bReInit = false)
        {
            if (base.OnInitModule(bReInit))
            {
                // Add configured GPIO's:
                var _dGpioDataset = AddDynamicVariables(Settings.IO, DatasetGpio);

                // Subscribe to COV events of configured GPIO variables:
                _dGpioDataset.Variables.ForEach(_iVar => { _iVar.ValueChanged += OnVariableValueChanged; });

                // Initialize GPIO communication:
                #if DEBUG
                    // Skip to prevent from crashing while debugging.
                #else
                    Debug.Assert(gGPioCtrl == null);

                    gGPioCtrl = new GpioController();

                    // Open pins:
                    Settings.IO.ForEach(_aSetting => gGPioCtrl.OpenPin(_aSetting.Pin, PINMODE_MAP[_aSetting.VariableType]));

                    // Subscribe to COV events of opened pins:
                    Settings.IO
                        .Where(_aSetting => (_aSetting.VariableType == AmVariableType.eInput))
                        .ForEach(_aSetting => gGPioCtrl.RegisterCallbackForPinValueChangedEvent(_aSetting.Pin, (PinEventTypes.Falling | PinEventTypes.Rising), OnPinChanged));
                #endif

                return (true);
            }
            else
                return (false);
        }
        #endregion
        #region Events.Variable
        private void OnVariableValueChanged(object oSender, ValueChangedEventArgs vArgs)
        {
            if (oSender is IAmOutputVariable _iOutputVar)
                // Write variable value to corresponding pin:
                WritePin(_iOutputVar);
        }
        #endregion
        #region Events.GPIO
        private void OnPinChanged(object oSender, PinValueChangedEventArgs pArgs)
        {
            var _iVar = GetVariable<bool>(pArgs.PinNumber);

            // Write pin value to corresponding variable:
            _iVar.Value = PINEVENT_MAP[pArgs.ChangeType];

            Logger.LogTrace(143, string.Format("Pin {0} ({1}) changed value to '{2}'.", pArgs.PinNumber, _iVar.VariableName, _iVar.Value));
        }
        #endregion


        #region Management
        public override void Dispose()
        {
            // Close GPIO communication:
            #if DEBUG
                // Skip to prevent from crashing while debugging.
            #else
                Debug.Assert(gGPioCtrl != null);

                // Unsubscribe from COV events of opened pins:
                Settings.IO
                    .Where(_aSetting => (_aSetting.VariableType == AmVariableType.eInput))
                    .ForEach(_aSetting => gGPioCtrl.UnregisterCallbackForPinValueChangedEvent(_aSetting.Pin, OnPinChanged));

                // Close pins:
                Settings.IO.ForEach(_aSetting => gGPioCtrl.ClosePin(_aSetting.Pin));

                gGPioCtrl.Dispose();
                gGPioCtrl = null;
            #endif

            base.Dispose();
        }
        #endregion
        #region GPIO
        public void WritePin(IAmVariable iVariable)
        {
            var _iPin = GetPin(iVariable);
            var _iVar = (IAmVariable<bool>)iVariable;
            
            Logger.LogTrace(143, string.Format("Writing value '{0}' to pin {1} ({2}).", _iVar.Value, _iPin, _iVar.VariableName));

            #if !DEBUG
            gGPioCtrl.Write(_iPin, PINVALUE_MAP[_iVar.Value]);
            #endif
        }
        public void ReadPin(IAmVariable iVariable)
        {
            #if !DEBUG
            var _pVal = gGPioCtrl.Read(GetPin(iVariable));
            var _bKey = PINVALUE_MAP.First(iPair => (iPair.Value == _pVal)).Key;
            
            ((IAmVariable<bool>)iVariable).Value = _bKey;
            #endif
        }
        #endregion


        #region Helper
        private IAmVariable<T> GetVariable<T>(int iPin) => (IAmVariable<T>)GetVariable(iPin);
        private IAmVariable GetVariable(int iPin)
        {
            return (Datasets[DatasetGpio].Variables.First(_iVar => {
                if (_iVar.ReferredSetting is AmRaspberryPiSettings.AmPinSettings _aSetting)
                    return (_aSetting.Pin == iPin);
                else
                    return (true);
            }));
        }
        private int GetPin(IAmVariable iVariable) => ((AmRaspberryPiSettings.AmPinSettings)iVariable.ReferredSetting).Pin;
        #endregion


        GpioController gGPioCtrl;
    }
}