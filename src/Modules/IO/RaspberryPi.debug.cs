using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using Shared.Utils.Core;

namespace AutoMate.Modules.IO
{
    public partial class AmRaspberryPiModule
    {
        #if DEBUG
        // Min / Max debug interval (in [ms]).
        private static readonly Tuple<double, double> DEBUG_INTERNAL = new Tuple<double, double>(300, 15000);
        // Change (in [%]) to raze multiple COV per debug-cycle.
        private const int CHANCE_MULTIPLE_COV = 70;
        // Max. count of (multiple) COV events per debug-cycle.
        private const int MAX_MULTIPLE_COV = 5;
        // Simulated input pins.
        private static readonly string[] SIMULATED_INPUTS = new string[] { "Switch" };
        
        
        protected override void OnDebug_Init()
        {
            IsDebugging = false;

            if (IsDebugging)
            {
                // Determine inputs to simulate:
                var _aInput = Settings.IO
                    .Where(_aSetting => (_aSetting.VariableType == AmVariableType.eInput))
                    .Where(_aSetting => SIMULATED_INPUTS.Contains(_aSetting.VariableName))
                    .ToArray();

                if (_aInput.IsEmpty())
                    IsDebugging = false;
                else
                {
                    // Update state:
                    DebugState = "Simulating the following variables:";
                    _aInput.ForEach(_Var => { DebugState += string.Format("\n- {0}", _Var.VariableName); });

                    // Generate debug cycles:
                    var _rRand = new Random(DateTime.Now.Millisecond);

                    tSimulateInputs = new System.Timers.Timer();
                    tSimulateInputs.Interval = 1000;
                    tSimulateInputs.AutoReset = false;
                    tSimulateInputs.Elapsed += (oSender, eArgs) =>
                    {
                        // Determine simulated count of COV events:
                        int _iInputs = 1;
                        if ((_rRand.Next() % 100) < CHANCE_MULTIPLE_COV)
                            _iInputs += (_rRand.Next() % (MAX_MULTIPLE_COV-1));

                        // Write variable:
                        // (BETA) ...
                        // Only the first variable is written yet - implement writing all variables of "_aInput"!
                        for (int i = 0; i < _iInputs; i++)
                        {
                            var _aVar = GetVariable<bool>(_aInput[0].Pin);
                            _aVar.Value = !_aVar.Value;
                        }

                        // Next cycle:
                        tSimulateInputs.Interval = Math.Clamp(_rRand.NextDouble(), DEBUG_INTERNAL.Item1, DEBUG_INTERNAL.Item2);
                        tSimulateInputs.Start();
                    };
                    tSimulateInputs.Start();
                }
            }
        }
        protected override void OnDebug_Exit()
        {
            tSimulateInputs?.Dispose();
        }


        System.Timers.Timer tSimulateInputs;
        #endif
    }
}