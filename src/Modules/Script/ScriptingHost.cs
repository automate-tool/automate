using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Timers;
using AutoMate.Services;
using CSScriptLib;
using Microsoft.Extensions.Logging;
using Shared.Asp.GenNav;
using Shared.Utils.Core;
using Shared.Asp.GenMenu;
using Shared.Asp.Alert;
using AutoMate.Logger;
using Newtonsoft.Json;
using AutoMate.Local;
using AutoMate.Event;

namespace AutoMate.Modules.Script
{
    #region Enumerations
    public enum ScriptSource
    {
        Invalid,

        /// <summary>
        /// Script is loaded from local repository (wich is recommended).
        /// </summary>
        [Description("Local repository")]
        eLocalRepository,
        /// <summary>
        /// Script is loaded from settings.
        /// </summary>
        [Description("Settings")]
        eSettings,
        /// <summary>
        /// Script is loaded from executing assembly (for debugging purposes).
        /// </summary>
        [Description("Executing assembly")]
        eExecutingAssembly
    }
    #endregion
    #region Attributes.Script
    public class AmScriptAttribute : AmComponentAttribute
    {
        public AmScriptAttribute(long lModuleId, int iMajor, int iMinor, int iBuild = 0, int iRevision = 0) : base(iMajor, iMinor, iBuild, iRevision)
        {
            this.ModuleId = lModuleId;
        }


        #region Properties
        public long ModuleId { init; get; }
        #endregion
    }
    #endregion
    #region Interfaces.Script
    /// <summary>
    /// Internal script interface.
    /// </summary>
    internal interface IAmScriptItf : IAmScript
    {
        #region Events
        protected internal void Init(ILogger iLogger, ScriptSource sSource, AmScriptAttribute iScriptInfo, AmScriptingHostModule aOwner, bool bReInit);
        protected internal void ApplyValues();
        protected internal void Exit();
        protected internal void Cycle();
        #endregion
    }
    public interface IAmScript : IAmContext
    {
        #region Properties.Management
        Version Version { get; }
        ScriptSource Source { get; }

        /// <summary>
        /// Own module instance.
        /// </summary>
        IAmModule Self { get; }
        #endregion
    }
    public abstract class AmScript : IAmScriptItf, IAmScript
    {
        #region Properties.Management
        public Version Version { private set; get; }
        public ScriptSource Source { private set; get; }
        
        protected ILogger Logger { private set; get; }
        public IAmModule Self { private set; get; }
        #endregion


        #region Events
        /// <summary>
        /// Initialize script.
        /// </summary>
        /// <typeparam name="bReInit">Indicates weather the module is initialized the first time or re-initialized.</typeparam>
        protected abstract void OnInit(bool bReInit);
        /// <summary>
        /// Variable values may have changed and are ready to be applied.
        /// </summary>
        /// <remarks>
        /// Event occures during script initialization or triggered by user.
        /// </remarks>
        protected virtual void OnApplyValues()
        {
        }
        /// <summary>
        /// Exit script.
        /// </summary>
        protected virtual void OnExit()
        {
        }
        /// <summary>
        /// Execute cycle.
        /// </summary>
        protected abstract void OnCycle();
        #endregion


        #region Initialization
        void IAmScriptItf.Init(ILogger iLogger, ScriptSource sSource, AmScriptAttribute iDescription, AmScriptingHostModule aOwner, bool bReInit)
        {
            this.Logger = iLogger;
            this.Source = sSource;
            this.Version = iDescription.Version;
            this.Self = aOwner;
            this.iHost = aOwner;

            OnInit(bReInit);
        }
        void IAmScriptItf.ApplyValues() => OnApplyValues();
        void IAmScriptItf.Exit() => OnExit();
        void IAmScriptItf.Cycle() => OnCycle();
        #endregion


        public override string ToString() => Self.ToString();


        /// <summary>
        /// Scripting host (Provides for example configured modules).
        /// </summary>
        protected IAmScriptingHost iHost;
    }
    public interface IAmScriptingHost
    {
        #region Properties.Management
        IAmCyclicModuleSettings Settings { get; }
        IReadOnlyList<IAmModule> Modules { get; }

        IAmScript Script { get; }
        #endregion


        #region Management.Script
        IAmModule GetModule(string sModuleTitle);
        bool TryGetModule(string sModuleTitle, out IAmModule iResult);
        IAmModuleDataset GetDataset(string sModuleTitle, string sDataset);
        bool TryGetDataset(string sModuleTitle, string sDataset, out IAmModuleDataset iResult);
        IAmVariable GetVariable(string sModuleTitle, string sDataset, string sVariableName);
        IAmVariable<T> GetVariable<T>(string sModuleTitle, string sDataset, string sVariableName);
        bool TryGetVariable(string sModuleTitle, string sDataset, string sVariableName, out IAmVariable iResult);
        bool TryGetVariable<T>(string sModuleTitle, string sDataset, string sVariableName, out IAmVariable<T> iResult);
        #endregion
    }
    #endregion


    public class AmScriptingHostSettings : AmCyclicModuleSettings
    {
        #region Constants
        public static readonly AmScriptingHostSettings Default = new AmScriptingHostSettings()
        {
            Title = "Scripting Host Module",
            CycleInterval = new TimeSpan(0, 1, 0),
            Script =
@"using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Logging;
using AutoMate.Modules;
using AutoMate.Modules.Script;

[AmScript(000000000, 0,0,1)]
public class Script : AmScript
{
    #region Events
    protected override void OnInit(bool bReInit)
    {
        // Welcome!
        //
        // Use this script to start from scratch.
        // Feel free to paste your code into the 'Script' variable (module settings page).
        //
        // But the recommended workflow is to host a git repository in the local folder to put your script files!
        // > Note that the 'Script' variable needs to be empty!
        //   Otherwise it will override any hosted scripts.
        #error ""Read me""
    }
    protected override void OnCycle()
    {
    }
    #endregion
}"
        };
        #endregion


        public AmScriptingHostSettings()
        {
        }


        #region Properties.Management
        [JsonIgnore]
        [ReadOnly(true)]
        [Category(CAT_GENERAL)]
        [Description("Module ID.")]
        [DisplayName(nameof(Identifier))]
        public long ModuleId => Identifier;

        [Category("Code")]
        [Description("Interval of cyclic script execution.")]
        [DisplayName("Cycle interval")]
        public override TimeSpan CycleInterval { get; set; }
        [Category("Code")]
        [DisplayName("Variables")]
        [CollectionItemDisplayName("Variable")]
        public List<AmVariableSettings> Variables { set; get; } = new List<AmVariableSettings>();
        [Category("Code")]
        [Description("Implementation of the script code.")]
        [DisplayName("Script")]
        [Layout(LayoutType.eAreaValue)]
        public string Script { set; get; }
        #endregion
    }
    
    [ModuleDescription(AmModuleCategory.eScript, "Scripting Host", "script.host", 1,0,0)]
    public partial class AmScriptingHostModule : AmCyclicModule<AmScriptingHostSettings>, IAmScriptingHost, IMenuProvider, IAlertProvider, IAmEventProvider
    {
        #region Constants
        private const string DATASET_VARIABLES = "Variables";
        #endregion


        public AmScriptingHostModule(AmCoreService aOwner, AmScriptingHostSettings aSettings) : base(aOwner, aSettings)
        {
            iInstCount++;

            // Initialize menu:
            var _aGen = new MenuGroup("Script");
            {
                _aGen.MenuButtons.Add(mMenu_Script_Operation = new MenuButton("", MenuButtonLayout.eWarning, default, Env.ROLE_ADMIN));
                mMenu_Script_Operation.Click += OnMenu_Script_Operation;
            }
            lMenuGroups.Add(_aGen);
        }
        public AmScriptingHostModule(AmCoreService aOwner) : this(aOwner, AmScriptingHostSettings.Default)
        {
        }


        #region Properties.Management
        public IEnumerable<IMenuGroup> MenuGroups
        {
            get
            {
                if (iScript is IMenuProvider _iMenuPrv)
                {
                    foreach (var _iItem in _iMenuPrv.MenuGroups)
                        yield return (_iItem);
                }
                foreach (var _iItem in lMenuGroups)
                    yield return (_iItem);
            }
        }
        private List<IMenuGroup> lMenuGroups = new List<IMenuGroup>();
        public IEnumerable<Alert> Alerts => (iScript as IAlertProvider)?.Alerts;
        public IEnumerable<IAmEvent> Events
        {
            get
            {
                if (iScript is IAmEventProvider _iEventPrv)
                {
                    foreach (var _iEvent in _iEventPrv.Events)
                        yield return (_iEvent);
                }
                foreach (var _iEvent in (this as IAmEventProvider).GetEvents())
                    yield return (_iEvent);
            }
        }
        #endregion
        #region Properties.Script
        IAmCyclicModuleSettings IAmScriptingHost.Settings => Settings;
        IReadOnlyList<IAmModule> IAmScriptingHost.Modules => Owner.Modules;

        public IAmScript? Script => iScript;
        private IAmScriptItf? iScript { set; get; }
        #endregion


        #region Events.Initialization
        private static void InitScriptAssembly()
        {
            // Load scripts from assembly:
            if (dScriptTypeMap == null)
            {
                Assembly aLoadAsm = null;
                #if DEBUG
                    // Evaluate executing assembly:
                    aLoadAsm = Assembly.GetExecutingAssembly();
                    
                    iGenLogger.LogInformation(213, "Using executing assembly to provide (built-in) local scripts for debugging purposes.");
                #else
                    // Enumerate script files:
                    if (Env.Local.Repositories.IsEmpty())
                        iGenLogger.LogWarning(232, string.Format("No local repositories found to load any script files."));
                    else if (Env.Local.CompileScripts())
                        aLoadAsm = Env.Local.CompiledAssembly;
                #endif

                // Determine available script types:
                dScriptTypeMap = new();

                if (aLoadAsm != null)
                {
                    var _iScriptTypes = aLoadAsm.ExportedTypes
                        .Where(_tType => _tType.IsAssignableTo(typeof(IAmScript)));
                    
                    foreach (var _tType in _iScriptTypes)
                    {
                        if (_tType.TryGetCustomAttribute(out AmScriptAttribute _aAttrib))
                            dScriptTypeMap.Add(_aAttrib.ModuleId, new(_aAttrib, _tType));
                    }

                    if (dScriptTypeMap.IsEmpty())
                        iGenLogger.LogWarning(267, string.Format("No declared scripts found."));
                    else
                    {
                        iGenLogger.LogInformation(272, string.Format("Found {0} script types.", dScriptTypeMap.Count));
                        dScriptTypeMap.ForEach(_kPair => iGenLogger.LogTrace(269, string.Format("Enumerated script of type '{1}' (Module ID: {0}).", _kPair.Key, _kPair.Value.Item2.Name)));
                    }
                }
            }
        }
        public override bool OnInitModule(bool bReInit = false)
        {
            if (base.OnInitModule(bReInit))
            {
                // Add configured variables:
                AddDynamicVariables(Settings.Variables, DATASET_VARIABLES);

                InitScriptAssembly();
                
                return (true);
            }
            else
                return (false);
        }
        public override void OnPostInitModule(bool bReInit = false)
        {
            // Determine script source:
            var _sSource = ScriptSource.Invalid;

            if (iScript == null)
            {
                // Try 1) Load script from settings (if specified):
                if ((iScript == null) && (!string.IsNullOrEmpty(Settings.Script)))
                {
                    // Compile script:
                    var _sWatch = Stopwatch.StartNew();
                    var _iSettScript = CSScript.Evaluator.LoadCode<IAmScriptItf>(Settings.Script);
                    _sWatch.Stop();

                    Logger.LogInformation(384, string.Format("Compiled script from settings in {0}.", _sWatch.Elapsed));

                    var _tType = _iSettScript.GetType();
                    if (!_tType.TryGetCustomAttribute(out AmScriptAttribute _aAttrib))
                        Logger.LogWarning(390, string.Format("Dropped script of bad type '{0}!", _tType.Name));
                    else if (Identifier != _aAttrib.ModuleId)
                        Logger.LogWarning(392, string.Format("Dropped script with unexpected identifier '{0}!", _aAttrib.ModuleId));
                    else
                    {
                        iScript = _iSettScript;
                        _sSource = ScriptSource.eSettings;
                    }
                }

                // Try 2) Select script related to specified module ID:
                if ((iScript == null) && (dScriptTypeMap.TryGetValue(Identifier, out var _tScript)))
                {
                    iScript = (IAmScriptItf)Activator.CreateInstance(_tScript.Item2);
                    #if DEBUG
                        _sSource = ScriptSource.eExecutingAssembly;
                    #else
                        _sSource = ScriptSource.eLocalRepository;
                    #endif
                }
                    
                if (_sSource == ScriptSource.Invalid)
                    Logger.LogError(395, string.Format("Failed to determine script for mapping to module '{0}'!", Title));
                else
                {
                    Debug.Assert(iScript != null);

                    #if DEBUG
                    DebugState = "Debugging script";
                    IsDebugging = true;
                    #endif

                    Logger.LogInformation(406, string.Format("Mapped script of type '{0}' from {1} to module '{2}'.", iScript.GetType().Name, UtlEnum.GetEnumDescription(_sSource).ToLower(), Title));
                }
            }
            else
                _sSource = iScript.Source;

            // Initialize script:
            iScript?.Init(Logger, _sSource, dScriptTypeMap[Identifier].Item1, this, bReInit);

            base.OnPostInitModule(bReInit);

            // Notify about restored variable values:
            iScript?.ApplyValues();

            // Update controls:
            OnMenu_Script_Operation(null, null);
        }
        #endregion
        #region Events
        public override void OnApplyValues() => iScript?.ApplyValues();
        protected override void OnCycle(object oSender, ElapsedEventArgs eArgs) => ExecuteScript();
        #endregion
        #region Events.Menu
        public void OnMenu_Script_Operation(object oSender, MenuEventArgs mArgs)
        {
            if (mArgs != null)
                // Toggle state:
                RunState = !RunState;
            
            mMenu_Script_Operation.Text = (RunState ? "Pause" : "Resume");
            mMenu_Script_Operation.Outlined = RunState;
        }
        #endregion
        #region Communication
        private void ExecuteScript()
        {
            if (iScript != null)
            {
                WorkSemaphore.Wait();
                try
                {
                    // Execute script:
                    iScript.Cycle();
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {
                    WorkSemaphore.Release();
                }
            }
        }
        #endregion


        #region Management
        public override void Dispose()
        {
            // Exit script:
            iScript?.Exit();
            iScript = null;

            base.Dispose();

            iInstCount--;
            Debug.Assert(iInstCount >= 0);
            
            // Clear loaded script types if not used anymore:
            // > So reloading (may changed) types will be enabled.
            if (iInstCount == 0)
            {
                dScriptTypeMap.Clear();
                dScriptTypeMap = null;

                iGenLogger.LogInformation(407, "Cleared loaded script types.");
            }
        }
        #endregion
        #region Management.Script
        public IAmModule GetModule(string sModuleTitle)
        {
            if (TryGetModule(sModuleTitle, out var _iResult))
                return (_iResult);
            else
                throw new KeyNotFoundException(string.Format("Module '{0}' not found!", sModuleTitle));
        }
        public bool TryGetModule(string sModuleTitle, out IAmModule iResult)
        {
            var _iModule = Owner.Modules.FirstOrDefault(iModule => iModule.Title.Equals(sModuleTitle));

            iResult = _iModule;
            if (_iModule == null)
                return (false);
            else if (_iModule.State.State != AmModuleState.eOk)
                throw new InvalidOperationException(string.Format("Module '{0}' not available due to invalid state '{1}'!", sModuleTitle, _iModule.State.State));
            else
                return (true);
        }
        public IAmModuleDataset GetDataset(string sModuleTitle, string sDataset)
        {
            if (TryGetDataset(sModuleTitle, sDataset, out var _iResult))
                return (_iResult);
            else
                throw new KeyNotFoundException(string.Format("Dataset '{0}.{1}' not found!", sModuleTitle, sDataset));
        }
        public bool TryGetDataset(string sModuleTitle, string sDataset, out IAmModuleDataset iResult)
        {
            if (!TryGetModule(sModuleTitle, out var _iModule))
            {
                iResult = null;
                return (false);
            }
            else
                return (_iModule.Datasets.TryGetValue(sDataset, out iResult));
        }
        public IAmVariable GetVariable(string sModuleTitle, string sDataset, string sVariableName)
        {
            if (TryGetVariable(sModuleTitle, sDataset, sVariableName, out var _iResult))
                return (_iResult);
            else
                throw new KeyNotFoundException(string.Format("Variable '{0}.{1}.{2}' not found!", sModuleTitle, sDataset, sVariableName));
        }
        public IAmVariable<T> GetVariable<T>(string sModuleTitle, string sDataset, string sVariableName) => (IAmVariable<T>)GetVariable(sModuleTitle, sDataset, sVariableName);
        public bool TryGetVariable(string sModuleTitle, string sDataset, string sVariableName, out IAmVariable iResult)
        {
            if (!TryGetDataset(sModuleTitle, sDataset, out var _iDataset))
            {
                iResult = null;
                return (false);
            }
            else
                return (_iDataset.TryGetVariable(sVariableName, out iResult));
        }
        public bool TryGetVariable<T>(string sModuleTitle, string sDataset, string sVariableName, out IAmVariable<T> iResult)
        {
            IAmVariable _iVar = null;
            if (TryGetVariable(sModuleTitle, sDataset, sVariableName, out _iVar))
                iResult = (_iVar as IAmVariable<T>);
            else
                iResult = null;
            return (iResult != null);
        }
        #endregion


        MenuButton mMenu_Script_Operation;

        private static ILogger iGenLogger = Env.CreateLogger(typeof(AmScriptingHostModule));
        private static Dictionary<long, Tuple<AmScriptAttribute, Type>> dScriptTypeMap;

        private static int iInstCount = 0;
    }
}