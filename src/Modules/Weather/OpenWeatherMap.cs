
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.Json;
using AutoMate.Services;
using Shared.Utils.Core;

namespace AutoMate.Modules.Weather
{
    #region Module
    public class AmOpenWeatherMapSettings : AmWeatherSettings
    {
        #region Constants
        public static readonly AmOpenWeatherMapSettings Default = new AmOpenWeatherMapSettings()
        {
            Title = "Weather Module",
            CycleInterval = new TimeSpan(12, 0, 0),
        };
        #endregion
        #region Types
        public class AmApiContent
        {
            #region Properties
            [Category("")]
            [Description("")]
            [DisplayName("Current weather")]
            public bool CurrentWeather { get; set; } = true;
            [Category("")]
            [Description("")]
            [DisplayName("Minutely forecast")]
            public bool MinutelyForecast { get; set; }
            [Category("")]
            [Description("")]
            [DisplayName("Hourly forecast")]
            public bool HourlyForecast { get; set; }
            [Category("")]
            [Description("")]
            [DisplayName("Daily forecast")]
            public bool DailyForecast { get; set; } = true;
            #endregion


            #region Management
            public AmOpenWeatherMapModule.ApiContent Inactive
            {
                get => (0
                    | (CurrentWeather ? 0 : AmOpenWeatherMapModule.ApiContent.eCurrent)
                    | (MinutelyForecast ? 0 : AmOpenWeatherMapModule.ApiContent.eMinutely)
                    | (HourlyForecast ? 0 : AmOpenWeatherMapModule.ApiContent.eHourly)
                    | (DailyForecast ? 0 : AmOpenWeatherMapModule.ApiContent.eDaily));
            }
            #endregion
        }
        #endregion


        #region Properties
        [Category("Request")]
        [Description("Requested contents in API calls.")]
        [DisplayName("Contents")]
        public AmApiContent Contents { get; set; } = new AmApiContent();
        #endregion
    }
    
    [ModuleDescription(AmModuleCategory.eWeather, "Open Weather Map", "api.openweathermap.org", 1,1,0)]
    public partial class AmOpenWeatherMapModule : AmWeatherModule<AmOpenWeatherMapModule.ApiContent, AmOpenWeatherMapSettings>
    {
        #region Constants
        private const string API_UNITS = "metric";
        private static readonly IReadOnlyDictionary<string, IKnownField> KNOWN_FIELDS = new Dictionary<string, IKnownField>()
        {
            { "dt", KnownField.Timestamp },
            { "sunrise", KnownField.Sunrise },
            { "sunset", KnownField.Sunset },
            { "moonrise", KnownField.Moonrise },
            { "moonset", KnownField.Moonset },
            { "moon_phase", KnownField.MoonPhase },
            { "temp", KnownField.Temperature },
            { "temp.morn", KnownField.Temperature_Morning },
            { "temp.day", KnownField.Temperature_Day },
            { "temp.eve", KnownField.Temperature_Evening },
            { "temp.night", KnownField.Temperature_Night },
            { "temp.min", KnownField.Temperature_Min },
            { "temp.max", KnownField.Temperature_Max },
            { "feels_like", KnownField.FeelsLike },
            { "feels_like.morn", KnownField.FeelsLike_Morning },
            { "feels_like.day", KnownField.FeelsLike_Day },
            { "feels_like.eve", KnownField.FeelsLike_Evening },
            { "feels_like.night", KnownField.FeelsLike_Night },
            { "pressure", KnownField.Pressure },
            { "humidity", KnownField.Humidity },
            { "dew_point", KnownField.DewPoint },
            { "wind_speed", KnownField.WindSpeed },
            { "wind_gust", KnownField.WindGust },
            { "wind_deg", KnownField.WindDirection },
            { "clouds", KnownField.Clouds },
            { "uvi", KnownField.UvIndex },
            { "pop", KnownField.RainPropability },
            { "rain", KnownField.Rain },
            { "rain.1h", KnownField.Rain1h },
            { "snow", KnownField.Snow },
            { "snow.1h", KnownField.Snow1h },
            { "visibility", KnownField.AvgVisibility },
            { "precipitation", KnownField.Precipitation }
        };
        #endregion


        #region Enumerations
        [Flags]
        public enum ApiContent
        {
            [Format("Current weather")]
            [Description("current")]
            eCurrent = 0x01,
            [Format("Minutely forecast {0}")]
            [Description("minutely")]
            eMinutely = 0x02,
            [Format("Hourly forecast {0}")]
            [Description("hourly")]
            eHourly = 0x04,
            [Format("Daily forecast {0}")]
            [Description("daily")]
            eDaily = 0x08
        }
        #endregion


        public AmOpenWeatherMapModule(AmCoreService aOwner, AmOpenWeatherMapSettings aSettings) : base(aOwner, aSettings) { }
        public AmOpenWeatherMapModule(AmCoreService aOwner) : this(aOwner, AmOpenWeatherMapSettings.Default) { }


        #region Properties.Management
        protected override IReadOnlyDictionary<string, IKnownField> KnownFields => KNOWN_FIELDS;
        #endregion


        #region Communication
        protected override string CreateRequest()
        {
            // API-Docs:
            // https://openweathermap.org/api/one-call-api
            var _sExclude = Settings.Contents.Inactive
                .GetFlags()
                .Select(_aContent => UtlEnum.GetEnumDescription(_aContent));
            return (string.Format(
                @"https://api.openweathermap.org/data/2.5/onecall?lat={0}&lon={1}&exclude={2}&units={3}&appid={4}",
                Settings.Latitude,
                Settings.Longitude,
                string.Join(',', _sExclude),
                API_UNITS,
                Settings.ApiKey
            ));
        }
        protected override void ValidateResponse(JsonDocument document)
        {
            if (document.RootElement.TryGetPropertyValue<int>(out var _iCode, "cod"))
            {
                string _sMessage;
                if (!document.RootElement.TryGetPropertyValue(out _sMessage, "message"))
                    _sMessage = "Unknown error occured!";
                throw new HttpRequestException($"{_sMessage} (Code: {_iCode})");
            }
        }
        #endregion
    }
    #endregion
}