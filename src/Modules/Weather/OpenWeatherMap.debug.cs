using System;
using System.Linq;
using Shared.Utils.Core;

namespace AutoMate.Modules.Weather
{
    public partial class AmOpenWeatherMapModule
    {
        #if DEBUG
        protected override string CreateDebugResponse()
        {
          var aExclude = Settings.Contents.Inactive;
          var iOffset = 0;
          var sResponse = @"
{
  ""lat"": 51.8872,
  ""lon"": 8.3833,
  ""timezone"": ""Europe/Berlin"",
  ""timezone_offset"": 7200";

          if(!aExclude.HasFlag(ApiContent.eCurrent))
            sResponse += @",
""current"": {
  ""dt"": 1652121008,
  ""sunrise"": 1652067776,
  ""sunset"": 1652122975,
  ""temp"": 20.25,
  ""feels_like"": 19.9,
  ""pressure"": 1022,
  ""humidity"": 60,
  ""dew_point"": 12.24,
  ""uvi"": 0,
  ""clouds"": 47,
  ""visibility"": 10000,
  ""wind_speed"": 2.36,
  ""wind_deg"": 118,
  ""wind_gust"": 3.18,
  ""weather"": [
    {
      ""id"": 802,
      ""main"": ""Clouds"",
      ""description"": ""scattered clouds"",
      ""icon"": ""03d""
    }
  ]
}";
        
          if(!aExclude.HasFlag(ApiContent.eMinutely))
            sResponse += @",
  ""minutely"": [
    {
      ""dt"": 1652121060,
      ""precipitation"": 0
    },
    {
      ""dt"": 1652121120,
      ""precipitation"": 0
    },
    {
      ""dt"": 1652121180,
      ""precipitation"": 0
    },
    {
      ""dt"": 1652121240,
      ""precipitation"": 0
    },
    {
      ""dt"": 1652121300,
      ""precipitation"": 0
    },
    {
      ""dt"": 1652121360,
      ""precipitation"": 0
    },
    {
      ""dt"": 1652121420,
      ""precipitation"": 0
    },
    {
      ""dt"": 1652121480,
      ""precipitation"": 0
    },
    {
      ""dt"": 1652121540,
      ""precipitation"": 0
    },
    {
      ""dt"": 1652121600,
      ""precipitation"": 0
    },
    {
      ""dt"": 1652121660,
      ""precipitation"": 0
    },
    {
      ""dt"": 1652121720,
      ""precipitation"": 0
    },
    {
      ""dt"": 1652121780,
      ""precipitation"": 0
    },
    {
      ""dt"": 1652121840,
      ""precipitation"": 0
    },
    {
      ""dt"": 1652121900,
      ""precipitation"": 0
    },
    {
      ""dt"": 1652121960,
      ""precipitation"": 0
    },
    {
      ""dt"": 1652122020,
      ""precipitation"": 0
    },
    {
      ""dt"": 1652122080,
      ""precipitation"": 0
    },
    {
      ""dt"": 1652122140,
      ""precipitation"": 0
    },
    {
      ""dt"": 1652122200,
      ""precipitation"": 0
    },
    {
      ""dt"": 1652122260,
      ""precipitation"": 0
    },
    {
      ""dt"": 1652122320,
      ""precipitation"": 0
    },
    {
      ""dt"": 1652122380,
      ""precipitation"": 0
    },
    {
      ""dt"": 1652122440,
      ""precipitation"": 0
    },
    {
      ""dt"": 1652122500,
      ""precipitation"": 0
    },
    {
      ""dt"": 1652122560,
      ""precipitation"": 0
    },
    {
      ""dt"": 1652122620,
      ""precipitation"": 0
    },
    {
      ""dt"": 1652122680,
      ""precipitation"": 0
    },
    {
      ""dt"": 1652122740,
      ""precipitation"": 0
    },
    {
      ""dt"": 1652122800,
      ""precipitation"": 0
    },
    {
      ""dt"": 1652122860,
      ""precipitation"": 0
    },
    {
      ""dt"": 1652122920,
      ""precipitation"": 0
    },
    {
      ""dt"": 1652122980,
      ""precipitation"": 0
    },
    {
      ""dt"": 1652123040,
      ""precipitation"": 0
    },
    {
      ""dt"": 1652123100,
      ""precipitation"": 0
    },
    {
      ""dt"": 1652123160,
      ""precipitation"": 0
    },
    {
      ""dt"": 1652123220,
      ""precipitation"": 0
    },
    {
      ""dt"": 1652123280,
      ""precipitation"": 0
    },
    {
      ""dt"": 1652123340,
      ""precipitation"": 0
    },
    {
      ""dt"": 1652123400,
      ""precipitation"": 0
    },
    {
      ""dt"": 1652123460,
      ""precipitation"": 0
    },
    {
      ""dt"": 1652123520,
      ""precipitation"": 0
    },
    {
      ""dt"": 1652123580,
      ""precipitation"": 0
    },
    {
      ""dt"": 1652123640,
      ""precipitation"": 0
    },
    {
      ""dt"": 1652123700,
      ""precipitation"": 0
    },
    {
      ""dt"": 1652123760,
      ""precipitation"": 0
    },
    {
      ""dt"": 1652123820,
      ""precipitation"": 0
    },
    {
      ""dt"": 1652123880,
      ""precipitation"": 0
    },
    {
      ""dt"": 1652123940,
      ""precipitation"": 0
    },
    {
      ""dt"": 1652124000,
      ""precipitation"": 0
    },
    {
      ""dt"": 1652124060,
      ""precipitation"": 0
    },
    {
      ""dt"": 1652124120,
      ""precipitation"": 0
    },
    {
      ""dt"": 1652124180,
      ""precipitation"": 0
    },
    {
      ""dt"": 1652124240,
      ""precipitation"": 0
    },
    {
      ""dt"": 1652124300,
      ""precipitation"": 0
    },
    {
      ""dt"": 1652124360,
      ""precipitation"": 0
    },
    {
      ""dt"": 1652124420,
      ""precipitation"": 0
    },
    {
      ""dt"": 1652124480,
      ""precipitation"": 0
    },
    {
      ""dt"": 1652124540,
      ""precipitation"": 0
    },
    {
      ""dt"": 1652124600,
      ""precipitation"": 0
    },
    {
      ""dt"": 1652124660,
      ""precipitation"": 0
    }
  ]";

          if(!aExclude.HasFlag(ApiContent.eHourly))
            sResponse += @",
  ""hourly"": [
    {
      ""dt"": 1652119200,
      ""temp"": 19.84,
      ""feels_like"": 19.52,
      ""pressure"": 1022,
      ""humidity"": 63,
      ""dew_point"": 12.6,
      ""uvi"": 0.19,
      ""clouds"": 39,
      ""visibility"": 10000,
      ""wind_speed"": 2,
      ""wind_deg"": 113,
      ""wind_gust"": 2.86,
      ""weather"": [
        {
          ""id"": 802,
          ""main"": ""Clouds"",
          ""description"": ""scattered clouds"",
          ""icon"": ""03d""
        }
      ],
      ""pop"": 0
    },
    {
      ""dt"": 1652122800,
      ""temp"": 20.25,
      ""feels_like"": 19.9,
      ""pressure"": 1022,
      ""humidity"": 60,
      ""dew_point"": 12.24,
      ""uvi"": 0,
      ""clouds"": 47,
      ""visibility"": 10000,
      ""wind_speed"": 2.36,
      ""wind_deg"": 118,
      ""wind_gust"": 3.18,
      ""weather"": [
        {
          ""id"": 802,
          ""main"": ""Clouds"",
          ""description"": ""scattered clouds"",
          ""icon"": ""03d""
        }
      ],
      ""pop"": 0
    },
    {
      ""dt"": 1652126400,
      ""temp"": 19.24,
      ""feels_like"": 18.89,
      ""pressure"": 1022,
      ""humidity"": 64,
      ""dew_point"": 12.27,
      ""uvi"": 0,
      ""clouds"": 46,
      ""visibility"": 10000,
      ""wind_speed"": 2.48,
      ""wind_deg"": 130,
      ""wind_gust"": 3.36,
      ""weather"": [
        {
          ""id"": 802,
          ""main"": ""Clouds"",
          ""description"": ""scattered clouds"",
          ""icon"": ""03n""
        }
      ],
      ""pop"": 0
    },
    {
      ""dt"": 1652130000,
      ""temp"": 17.78,
      ""feels_like"": 17.39,
      ""pressure"": 1022,
      ""humidity"": 68,
      ""dew_point"": 11.8,
      ""uvi"": 0,
      ""clouds"": 49,
      ""visibility"": 10000,
      ""wind_speed"": 2.57,
      ""wind_deg"": 141,
      ""wind_gust"": 3.75,
      ""weather"": [
        {
          ""id"": 802,
          ""main"": ""Clouds"",
          ""description"": ""scattered clouds"",
          ""icon"": ""03n""
        }
      ],
      ""pop"": 0
    },
    {
      ""dt"": 1652133600,
      ""temp"": 16.31,
      ""feels_like"": 15.88,
      ""pressure"": 1022,
      ""humidity"": 72,
      ""dew_point"": 11.26,
      ""uvi"": 0,
      ""clouds"": 57,
      ""visibility"": 10000,
      ""wind_speed"": 2.48,
      ""wind_deg"": 156,
      ""wind_gust"": 3.3,
      ""weather"": [
        {
          ""id"": 803,
          ""main"": ""Clouds"",
          ""description"": ""broken clouds"",
          ""icon"": ""04n""
        }
      ],
      ""pop"": 0
    },
    {
      ""dt"": 1652137200,
      ""temp"": 15.05,
      ""feels_like"": 14.54,
      ""pressure"": 1022,
      ""humidity"": 74,
      ""dew_point"": 10.46,
      ""uvi"": 0,
      ""clouds"": 63,
      ""visibility"": 10000,
      ""wind_speed"": 2.39,
      ""wind_deg"": 164,
      ""wind_gust"": 2.81,
      ""weather"": [
        {
          ""id"": 803,
          ""main"": ""Clouds"",
          ""description"": ""broken clouds"",
          ""icon"": ""04n""
        }
      ],
      ""pop"": 0
    },
    {
      ""dt"": 1652140800,
      ""temp"": 14.3,
      ""feels_like"": 13.72,
      ""pressure"": 1021,
      ""humidity"": 74,
      ""dew_point"": 9.52,
      ""uvi"": 0,
      ""clouds"": 73,
      ""visibility"": 10000,
      ""wind_speed"": 2.08,
      ""wind_deg"": 173,
      ""wind_gust"": 2.47,
      ""weather"": [
        {
          ""id"": 803,
          ""main"": ""Clouds"",
          ""description"": ""broken clouds"",
          ""icon"": ""04n""
        }
      ],
      ""pop"": 0
    },
    {
      ""dt"": 1652144400,
      ""temp"": 14.51,
      ""feels_like"": 13.87,
      ""pressure"": 1021,
      ""humidity"": 71,
      ""dew_point"": 9.19,
      ""uvi"": 0,
      ""clouds"": 96,
      ""visibility"": 10000,
      ""wind_speed"": 2.27,
      ""wind_deg"": 166,
      ""wind_gust"": 2.81,
      ""weather"": [
        {
          ""id"": 804,
          ""main"": ""Clouds"",
          ""description"": ""overcast clouds"",
          ""icon"": ""04n""
        }
      ],
      ""pop"": 0
    },
    {
      ""dt"": 1652148000,
      ""temp"": 14.23,
      ""feels_like"": 13.56,
      ""pressure"": 1021,
      ""humidity"": 71,
      ""dew_point"": 8.9,
      ""uvi"": 0,
      ""clouds"": 98,
      ""visibility"": 10000,
      ""wind_speed"": 2.26,
      ""wind_deg"": 180,
      ""wind_gust"": 2.85,
      ""weather"": [
        {
          ""id"": 804,
          ""main"": ""Clouds"",
          ""description"": ""overcast clouds"",
          ""icon"": ""04n""
        }
      ],
      ""pop"": 0
    },
    {
      ""dt"": 1652151600,
      ""temp"": 14.03,
      ""feels_like"": 13.34,
      ""pressure"": 1021,
      ""humidity"": 71,
      ""dew_point"": 8.78,
      ""uvi"": 0,
      ""clouds"": 99,
      ""visibility"": 10000,
      ""wind_speed"": 2.24,
      ""wind_deg"": 197,
      ""wind_gust"": 3.89,
      ""weather"": [
        {
          ""id"": 804,
          ""main"": ""Clouds"",
          ""description"": ""overcast clouds"",
          ""icon"": ""04n""
        }
      ],
      ""pop"": 0
    },
    {
      ""dt"": 1652155200,
      ""temp"": 13.89,
      ""feels_like"": 13.21,
      ""pressure"": 1020,
      ""humidity"": 72,
      ""dew_point"": 8.86,
      ""uvi"": 0,
      ""clouds"": 99,
      ""visibility"": 10000,
      ""wind_speed"": 2.35,
      ""wind_deg"": 184,
      ""wind_gust"": 3.97,
      ""weather"": [
        {
          ""id"": 804,
          ""main"": ""Clouds"",
          ""description"": ""overcast clouds"",
          ""icon"": ""04d""
        }
      ],
      ""pop"": 0
    },
    {
      ""dt"": 1652158800,
      ""temp"": 14.21,
      ""feels_like"": 13.67,
      ""pressure"": 1020,
      ""humidity"": 76,
      ""dew_point"": 9.94,
      ""uvi"": 0.16,
      ""clouds"": 99,
      ""visibility"": 10000,
      ""wind_speed"": 2.24,
      ""wind_deg"": 182,
      ""wind_gust"": 4.24,
      ""weather"": [
        {
          ""id"": 804,
          ""main"": ""Clouds"",
          ""description"": ""overcast clouds"",
          ""icon"": ""04d""
        }
      ],
      ""pop"": 0
    },
    {
      ""dt"": 1652162400,
      ""temp"": 15.49,
      ""feels_like"": 15.08,
      ""pressure"": 1020,
      ""humidity"": 76,
      ""dew_point"": 11.07,
      ""uvi"": 0.5,
      ""clouds"": 99,
      ""visibility"": 10000,
      ""wind_speed"": 2.01,
      ""wind_deg"": 182,
      ""wind_gust"": 5.12,
      ""weather"": [
        {
          ""id"": 804,
          ""main"": ""Clouds"",
          ""description"": ""overcast clouds"",
          ""icon"": ""04d""
        }
      ],
      ""pop"": 0
    },
    {
      ""dt"": 1652166000,
      ""temp"": 17.49,
      ""feels_like"": 17.1,
      ""pressure"": 1019,
      ""humidity"": 69,
      ""dew_point"": 11.65,
      ""uvi"": 1.44,
      ""clouds"": 100,
      ""visibility"": 10000,
      ""wind_speed"": 2.89,
      ""wind_deg"": 199,
      ""wind_gust"": 6.11,
      ""weather"": [
        {
          ""id"": 804,
          ""main"": ""Clouds"",
          ""description"": ""overcast clouds"",
          ""icon"": ""04d""
        }
      ],
      ""pop"": 0
    },
    {
      ""dt"": 1652169600,
      ""temp"": 19.5,
      ""feels_like"": 19.18,
      ""pressure"": 1019,
      ""humidity"": 64,
      ""dew_point"": 12.42,
      ""uvi"": 2.57,
      ""clouds"": 99,
      ""visibility"": 10000,
      ""wind_speed"": 3.79,
      ""wind_deg"": 211,
      ""wind_gust"": 7.39,
      ""weather"": [
        {
          ""id"": 804,
          ""main"": ""Clouds"",
          ""description"": ""overcast clouds"",
          ""icon"": ""04d""
        }
      ],
      ""pop"": 0
    },
    {
      ""dt"": 1652173200,
      ""temp"": 21.12,
      ""feels_like"": 20.85,
      ""pressure"": 1019,
      ""humidity"": 60,
      ""dew_point"": 12.88,
      ""uvi"": 3.84,
      ""clouds"": 100,
      ""visibility"": 10000,
      ""wind_speed"": 4.69,
      ""wind_deg"": 218,
      ""wind_gust"": 7.61,
      ""weather"": [
        {
          ""id"": 804,
          ""main"": ""Clouds"",
          ""description"": ""overcast clouds"",
          ""icon"": ""04d""
        }
      ],
      ""pop"": 0
    },
    {
      ""dt"": 1652176800,
      ""temp"": 22.38,
      ""feels_like"": 22.08,
      ""pressure"": 1018,
      ""humidity"": 54,
      ""dew_point"": 12.4,
      ""uvi"": 5.11,
      ""clouds"": 100,
      ""visibility"": 10000,
      ""wind_speed"": 5.58,
      ""wind_deg"": 224,
      ""wind_gust"": 8.67,
      ""weather"": [
        {
          ""id"": 804,
          ""main"": ""Clouds"",
          ""description"": ""overcast clouds"",
          ""icon"": ""04d""
        }
      ],
      ""pop"": 0
    },
    {
      ""dt"": 1652180400,
      ""temp"": 23.02,
      ""feels_like"": 22.66,
      ""pressure"": 1018,
      ""humidity"": 49,
      ""dew_point"": 11.7,
      ""uvi"": 5.77,
      ""clouds"": 98,
      ""visibility"": 10000,
      ""wind_speed"": 5.89,
      ""wind_deg"": 229,
      ""wind_gust"": 9.13,
      ""weather"": [
        {
          ""id"": 804,
          ""main"": ""Clouds"",
          ""description"": ""overcast clouds"",
          ""icon"": ""04d""
        }
      ],
      ""pop"": 0
    },
    {
      ""dt"": 1652184000,
      ""temp"": 23.59,
      ""feels_like"": 23.21,
      ""pressure"": 1017,
      ""humidity"": 46,
      ""dew_point"": 11.4,
      ""uvi"": 5.75,
      ""clouds"": 97,
      ""visibility"": 10000,
      ""wind_speed"": 6.02,
      ""wind_deg"": 234,
      ""wind_gust"": 9.68,
      ""weather"": [
        {
          ""id"": 804,
          ""main"": ""Clouds"",
          ""description"": ""overcast clouds"",
          ""icon"": ""04d""
        }
      ],
      ""pop"": 0
    },
    {
      ""dt"": 1652187600,
      ""temp"": 24.01,
      ""feels_like"": 23.67,
      ""pressure"": 1016,
      ""humidity"": 46,
      ""dew_point"": 11.69,
      ""uvi"": 3.84,
      ""clouds"": 100,
      ""visibility"": 10000,
      ""wind_speed"": 5.03,
      ""wind_deg"": 231,
      ""wind_gust"": 9.53,
      ""weather"": [
        {
          ""id"": 804,
          ""main"": ""Clouds"",
          ""description"": ""overcast clouds"",
          ""icon"": ""04d""
        }
      ],
      ""pop"": 0
    },
    {
      ""dt"": 1652191200,
      ""temp"": 23.59,
      ""feels_like"": 23.39,
      ""pressure"": 1015,
      ""humidity"": 53,
      ""dew_point"": 13.35,
      ""uvi"": 2.95,
      ""clouds"": 100,
      ""visibility"": 10000,
      ""wind_speed"": 4.89,
      ""wind_deg"": 251,
      ""wind_gust"": 9.25,
      ""weather"": [
        {
          ""id"": 804,
          ""main"": ""Clouds"",
          ""description"": ""overcast clouds"",
          ""icon"": ""04d""
        }
      ],
      ""pop"": 0
    },
    {
      ""dt"": 1652194800,
      ""temp"": 21.93,
      ""feels_like"": 21.75,
      ""pressure"": 1015,
      ""humidity"": 60,
      ""dew_point"": 13.64,
      ""uvi"": 1.95,
      ""clouds"": 100,
      ""visibility"": 10000,
      ""wind_speed"": 4.86,
      ""wind_deg"": 263,
      ""wind_gust"": 9.43,
      ""weather"": [
        {
          ""id"": 804,
          ""main"": ""Clouds"",
          ""description"": ""overcast clouds"",
          ""icon"": ""04d""
        }
      ],
      ""pop"": 0
    },
    {
      ""dt"": 1652198400,
      ""temp"": 21.37,
      ""feels_like"": 21.21,
      ""pressure"": 1015,
      ""humidity"": 63,
      ""dew_point"": 13.94,
      ""uvi"": 1.23,
      ""clouds"": 100,
      ""visibility"": 10000,
      ""wind_speed"": 4.43,
      ""wind_deg"": 256,
      ""wind_gust"": 9.63,
      ""weather"": [
        {
          ""id"": 804,
          ""main"": ""Clouds"",
          ""description"": ""overcast clouds"",
          ""icon"": ""04d""
        }
      ],
      ""pop"": 0
    },
    {
      ""dt"": 1652202000,
      ""temp"": 20.4,
      ""feels_like"": 20.22,
      ""pressure"": 1015,
      ""humidity"": 66,
      ""dew_point"": 13.75,
      ""uvi"": 0.53,
      ""clouds"": 100,
      ""visibility"": 10000,
      ""wind_speed"": 4.43,
      ""wind_deg"": 248,
      ""wind_gust"": 10.11,
      ""weather"": [
        {
          ""id"": 804,
          ""main"": ""Clouds"",
          ""description"": ""overcast clouds"",
          ""icon"": ""04d""
        }
      ],
      ""pop"": 0
    },
    {
      ""dt"": 1652205600,
      ""temp"": 18.86,
      ""feels_like"": 18.71,
      ""pressure"": 1015,
      ""humidity"": 73,
      ""dew_point"": 13.82,
      ""uvi"": 0.16,
      ""clouds"": 100,
      ""visibility"": 10000,
      ""wind_speed"": 3.14,
      ""wind_deg"": 239,
      ""wind_gust"": 8.15,
      ""weather"": [
        {
          ""id"": 804,
          ""main"": ""Clouds"",
          ""description"": ""overcast clouds"",
          ""icon"": ""04d""
        }
      ],
      ""pop"": 0
    },
    {
      ""dt"": 1652209200,
      ""temp"": 16.8,
      ""feels_like"": 16.55,
      ""pressure"": 1015,
      ""humidity"": 77,
      ""dew_point"": 12.74,
      ""uvi"": 0,
      ""clouds"": 100,
      ""visibility"": 10000,
      ""wind_speed"": 2.89,
      ""wind_deg"": 224,
      ""wind_gust"": 5.38,
      ""weather"": [
        {
          ""id"": 804,
          ""main"": ""Clouds"",
          ""description"": ""overcast clouds"",
          ""icon"": ""04d""
        }
      ],
      ""pop"": 0.1
    },
    {
      ""dt"": 1652212800,
      ""temp"": 16.37,
      ""feels_like"": 16.1,
      ""pressure"": 1015,
      ""humidity"": 78,
      ""dew_point"": 12.48,
      ""uvi"": 0,
      ""clouds"": 100,
      ""visibility"": 10000,
      ""wind_speed"": 2.67,
      ""wind_deg"": 249,
      ""wind_gust"": 6.45,
      ""weather"": [
        {
          ""id"": 804,
          ""main"": ""Clouds"",
          ""description"": ""overcast clouds"",
          ""icon"": ""04n""
        }
      ],
      ""pop"": 0.1
    },
    {
      ""dt"": 1652216400,
      ""temp"": 15.05,
      ""feels_like"": 14.86,
      ""pressure"": 1016,
      ""humidity"": 86,
      ""dew_point"": 12.62,
      ""uvi"": 0,
      ""clouds"": 100,
      ""visibility"": 10000,
      ""wind_speed"": 2.47,
      ""wind_deg"": 269,
      ""wind_gust"": 4.36,
      ""weather"": [
        {
          ""id"": 804,
          ""main"": ""Clouds"",
          ""description"": ""overcast clouds"",
          ""icon"": ""04n""
        }
      ],
      ""pop"": 0.02
    },
    {
      ""dt"": 1652220000,
      ""temp"": 13.88,
      ""feels_like"": 13.7,
      ""pressure"": 1016,
      ""humidity"": 91,
      ""dew_point"": 12.36,
      ""uvi"": 0,
      ""clouds"": 100,
      ""visibility"": 10000,
      ""wind_speed"": 1.93,
      ""wind_deg"": 273,
      ""wind_gust"": 2.08,
      ""weather"": [
        {
          ""id"": 804,
          ""main"": ""Clouds"",
          ""description"": ""overcast clouds"",
          ""icon"": ""04n""
        }
      ],
      ""pop"": 0
    },
    {
      ""dt"": 1652223600,
      ""temp"": 13.28,
      ""feels_like"": 12.96,
      ""pressure"": 1016,
      ""humidity"": 88,
      ""dew_point"": 11.2,
      ""uvi"": 0,
      ""clouds"": 100,
      ""visibility"": 10000,
      ""wind_speed"": 1.78,
      ""wind_deg"": 230,
      ""wind_gust"": 1.85,
      ""weather"": [
        {
          ""id"": 804,
          ""main"": ""Clouds"",
          ""description"": ""overcast clouds"",
          ""icon"": ""04n""
        }
      ],
      ""pop"": 0
    },
    {
      ""dt"": 1652227200,
      ""temp"": 13.53,
      ""feels_like"": 13.03,
      ""pressure"": 1015,
      ""humidity"": 80,
      ""dew_point"": 10.15,
      ""uvi"": 0,
      ""clouds"": 100,
      ""visibility"": 10000,
      ""wind_speed"": 2.09,
      ""wind_deg"": 225,
      ""wind_gust"": 3.08,
      ""weather"": [
        {
          ""id"": 804,
          ""main"": ""Clouds"",
          ""description"": ""overcast clouds"",
          ""icon"": ""04n""
        }
      ],
      ""pop"": 0
    },
    {
      ""dt"": 1652230800,
      ""temp"": 14,
      ""feels_like"": 13.39,
      ""pressure"": 1015,
      ""humidity"": 74,
      ""dew_point"": 9.39,
      ""uvi"": 0,
      ""clouds"": 100,
      ""visibility"": 10000,
      ""wind_speed"": 2.11,
      ""wind_deg"": 207,
      ""wind_gust"": 4.51,
      ""weather"": [
        {
          ""id"": 804,
          ""main"": ""Clouds"",
          ""description"": ""overcast clouds"",
          ""icon"": ""04n""
        }
      ],
      ""pop"": 0
    },
    {
      ""dt"": 1652234400,
      ""temp"": 14.09,
      ""feels_like"": 13.49,
      ""pressure"": 1014,
      ""humidity"": 74,
      ""dew_point"": 9.44,
      ""uvi"": 0,
      ""clouds"": 100,
      ""visibility"": 10000,
      ""wind_speed"": 2.43,
      ""wind_deg"": 192,
      ""wind_gust"": 5.4,
      ""weather"": [
        {
          ""id"": 804,
          ""main"": ""Clouds"",
          ""description"": ""overcast clouds"",
          ""icon"": ""04n""
        }
      ],
      ""pop"": 0
    },
    {
      ""dt"": 1652238000,
      ""temp"": 12.83,
      ""feels_like"": 12.34,
      ""pressure"": 1014,
      ""humidity"": 83,
      ""dew_point"": 9.92,
      ""uvi"": 0,
      ""clouds"": 97,
      ""visibility"": 10000,
      ""wind_speed"": 2.81,
      ""wind_deg"": 189,
      ""wind_gust"": 5.71,
      ""weather"": [
        {
          ""id"": 804,
          ""main"": ""Clouds"",
          ""description"": ""overcast clouds"",
          ""icon"": ""04n""
        }
      ],
      ""pop"": 0
    },
    {
      ""dt"": 1652241600,
      ""temp"": 12.46,
      ""feels_like"": 12.11,
      ""pressure"": 1014,
      ""humidity"": 90,
      ""dew_point"": 10.71,
      ""uvi"": 0,
      ""clouds"": 95,
      ""visibility"": 10000,
      ""wind_speed"": 2.8,
      ""wind_deg"": 196,
      ""wind_gust"": 5.8,
      ""weather"": [
        {
          ""id"": 804,
          ""main"": ""Clouds"",
          ""description"": ""overcast clouds"",
          ""icon"": ""04d""
        }
      ],
      ""pop"": 0
    },
    {
      ""dt"": 1652245200,
      ""temp"": 13.72,
      ""feels_like"": 13.45,
      ""pressure"": 1014,
      ""humidity"": 88,
      ""dew_point"": 11.7,
      ""uvi"": 0.14,
      ""clouds"": 95,
      ""visibility"": 10000,
      ""wind_speed"": 2.57,
      ""wind_deg"": 203,
      ""wind_gust"": 6.68,
      ""weather"": [
        {
          ""id"": 804,
          ""main"": ""Clouds"",
          ""description"": ""overcast clouds"",
          ""icon"": ""04d""
        }
      ],
      ""pop"": 0
    },
    {
      ""dt"": 1652248800,
      ""temp"": 15.43,
      ""feels_like"": 15.14,
      ""pressure"": 1014,
      ""humidity"": 81,
      ""dew_point"": 12.02,
      ""uvi"": 0.45,
      ""clouds"": 96,
      ""visibility"": 10000,
      ""wind_speed"": 2.91,
      ""wind_deg"": 224,
      ""wind_gust"": 6.68,
      ""weather"": [
        {
          ""id"": 804,
          ""main"": ""Clouds"",
          ""description"": ""overcast clouds"",
          ""icon"": ""04d""
        }
      ],
      ""pop"": 0
    },
    {
      ""dt"": 1652252400,
      ""temp"": 16.93,
      ""feels_like"": 16.64,
      ""pressure"": 1013,
      ""humidity"": 75,
      ""dew_point"": 12.34,
      ""uvi"": 1.47,
      ""clouds"": 57,
      ""visibility"": 10000,
      ""wind_speed"": 3.54,
      ""wind_deg"": 229,
      ""wind_gust"": 6.96,
      ""weather"": [
        {
          ""id"": 803,
          ""main"": ""Clouds"",
          ""description"": ""broken clouds"",
          ""icon"": ""04d""
        }
      ],
      ""pop"": 0
    },
    {
      ""dt"": 1652256000,
      ""temp"": 18.65,
      ""feels_like"": 18.4,
      ""pressure"": 1013,
      ""humidity"": 70,
      ""dew_point"": 13.11,
      ""uvi"": 2.63,
      ""clouds"": 30,
      ""visibility"": 10000,
      ""wind_speed"": 4.57,
      ""wind_deg"": 237,
      ""wind_gust"": 8.51,
      ""weather"": [
        {
          ""id"": 802,
          ""main"": ""Clouds"",
          ""description"": ""scattered clouds"",
          ""icon"": ""03d""
        }
      ],
      ""pop"": 0
    },
    {
      ""dt"": 1652259600,
      ""temp"": 19.81,
      ""feels_like"": 19.62,
      ""pressure"": 1013,
      ""humidity"": 68,
      ""dew_point"": 13.61,
      ""uvi"": 3.92,
      ""clouds"": 27,
      ""visibility"": 10000,
      ""wind_speed"": 5.96,
      ""wind_deg"": 244,
      ""wind_gust"": 10.69,
      ""weather"": [
        {
          ""id"": 802,
          ""main"": ""Clouds"",
          ""description"": ""scattered clouds"",
          ""icon"": ""03d""
        }
      ],
      ""pop"": 0
    },
    {
      ""dt"": 1652263200,
      ""temp"": 20.24,
      ""feels_like"": 20.07,
      ""pressure"": 1012,
      ""humidity"": 67,
      ""dew_point"": 13.78,
      ""uvi"": 4.67,
      ""clouds"": 39,
      ""visibility"": 10000,
      ""wind_speed"": 6.52,
      ""wind_deg"": 247,
      ""wind_gust"": 11.23,
      ""weather"": [
        {
          ""id"": 500,
          ""main"": ""Rain"",
          ""description"": ""light rain"",
          ""icon"": ""10d""
        }
      ],
      ""pop"": 0.2,
      ""rain"": {
        ""1h"": 0.15
      }
    },
    {
      ""dt"": 1652266800,
      ""temp"": 21.29,
      ""feels_like"": 21.04,
      ""pressure"": 1012,
      ""humidity"": 60,
      ""dew_point"": 13.23,
      ""uvi"": 5.27,
      ""clouds"": 41,
      ""visibility"": 10000,
      ""wind_speed"": 7.04,
      ""wind_deg"": 254,
      ""wind_gust"": 11.89,
      ""weather"": [
        {
          ""id"": 802,
          ""main"": ""Clouds"",
          ""description"": ""scattered clouds"",
          ""icon"": ""03d""
        }
      ],
      ""pop"": 0
    },
    {
      ""dt"": 1652270400,
      ""temp"": 22.24,
      ""feels_like"": 21.93,
      ""pressure"": 1012,
      ""humidity"": 54,
      ""dew_point"": 12.29,
      ""uvi"": 5.24,
      ""clouds"": 38,
      ""visibility"": 10000,
      ""wind_speed"": 7.44,
      ""wind_deg"": 255,
      ""wind_gust"": 12.43,
      ""weather"": [
        {
          ""id"": 802,
          ""main"": ""Clouds"",
          ""description"": ""scattered clouds"",
          ""icon"": ""03d""
        }
      ],
      ""pop"": 0
    },
    {
      ""dt"": 1652274000,
      ""temp"": 22.47,
      ""feels_like"": 22.16,
      ""pressure"": 1011,
      ""humidity"": 53,
      ""dew_point"": 12.37,
      ""uvi"": 5.05,
      ""clouds"": 28,
      ""visibility"": 10000,
      ""wind_speed"": 7.08,
      ""wind_deg"": 259,
      ""wind_gust"": 12.39,
      ""weather"": [
        {
          ""id"": 802,
          ""main"": ""Clouds"",
          ""description"": ""scattered clouds"",
          ""icon"": ""03d""
        }
      ],
      ""pop"": 0.04
    },
    {
      ""dt"": 1652277600,
      ""temp"": 22.53,
      ""feels_like"": 22.25,
      ""pressure"": 1011,
      ""humidity"": 54,
      ""dew_point"": 12.55,
      ""uvi"": 3.89,
      ""clouds"": 27,
      ""visibility"": 10000,
      ""wind_speed"": 6.5,
      ""wind_deg"": 262,
      ""wind_gust"": 12.44,
      ""weather"": [
        {
          ""id"": 802,
          ""main"": ""Clouds"",
          ""description"": ""scattered clouds"",
          ""icon"": ""03d""
        }
      ],
      ""pop"": 0.08
    },
    {
      ""dt"": 1652281200,
      ""temp"": 22.25,
      ""feels_like"": 21.99,
      ""pressure"": 1010,
      ""humidity"": 56,
      ""dew_point"": 12.96,
      ""uvi"": 2.58,
      ""clouds"": 27,
      ""visibility"": 10000,
      ""wind_speed"": 5.54,
      ""wind_deg"": 267,
      ""wind_gust"": 11.48,
      ""weather"": [
        {
          ""id"": 802,
          ""main"": ""Clouds"",
          ""description"": ""scattered clouds"",
          ""icon"": ""03d""
        }
      ],
      ""pop"": 0.08
    },
    {
      ""dt"": 1652284800,
      ""temp"": 21.8,
      ""feels_like"": 21.6,
      ""pressure"": 1010,
      ""humidity"": 60,
      ""dew_point"": 13.49,
      ""uvi"": 1.46,
      ""clouds"": 33,
      ""visibility"": 10000,
      ""wind_speed"": 4.37,
      ""wind_deg"": 267,
      ""wind_gust"": 9.66,
      ""weather"": [
        {
          ""id"": 802,
          ""main"": ""Clouds"",
          ""description"": ""scattered clouds"",
          ""icon"": ""03d""
        }
      ],
      ""pop"": 0.08
    },
    {
      ""dt"": 1652288400,
      ""temp"": 20.64,
      ""feels_like"": 20.48,
      ""pressure"": 1010,
      ""humidity"": 66,
      ""dew_point"": 13.95,
      ""uvi"": 0.64,
      ""clouds"": 37,
      ""visibility"": 10000,
      ""wind_speed"": 3.29,
      ""wind_deg"": 259,
      ""wind_gust"": 7.73,
      ""weather"": [
        {
          ""id"": 802,
          ""main"": ""Clouds"",
          ""description"": ""scattered clouds"",
          ""icon"": ""03d""
        }
      ],
      ""pop"": 0.12
    }
  ]";

          if(!aExclude.HasFlag(ApiContent.eDaily))
            sResponse += @",
  ""daily"": [
    {
      ""dt"": " + GetTimestamp(ref iOffset) + @",
      ""sunrise"": 1652067776,
      ""sunset"": 1652122975,
      ""moonrise"": 1652092020,
      ""moonset"": 1652060580,
      ""moon_phase"": 0.25,
      ""temp"": {
        ""day"": 20.07,
        ""min"": 6.86,
        ""max"": 21.59,
        ""night"": 17.78,
        ""eve"": 20.29,
        ""morn"": 8.8
      },
      ""feels_like"": {
        ""day"": 19.54,
        ""night"": 17.39,
        ""eve"": 20.02,
        ""morn"": 7.68
      },
      ""pressure"": 1025,
      ""humidity"": 54,
      ""dew_point"": 10.43,
      ""wind_speed"": 2.82,
      ""wind_deg"": 53,
      ""wind_gust"": 5.46,
      ""weather"": [
        {
          ""id"": 802,
          ""main"": ""Clouds"",
          ""description"": ""scattered clouds"",
          ""icon"": ""03d""
        }
      ],
      ""clouds"": 48,
      ""pop"": 0.2,
      ""rain"": 0.0,
      ""uvi"": 5.66
    },
    {
      ""dt"": " + GetTimestamp(ref iOffset) + @",
      ""sunrise"": 1652154076,
      ""sunset"": 1652209472,
      ""moonrise"": 1652182980,
      ""moonset"": 1652148060,
      ""moon_phase"": 0.29,
      ""temp"": {
        ""day"": 23.02,
        ""min"": 13.89,
        ""max"": 24.01,
        ""night"": 15.05,
        ""eve"": 20.4,
        ""morn"": 14.21
      },
      ""feels_like"": {
        ""day"": 22.66,
        ""night"": 14.86,
        ""eve"": 20.22,
        ""morn"": 13.67
      },
      ""pressure"": 1018,
      ""humidity"": 49,
      ""dew_point"": 11.7,
      ""wind_speed"": 6.02,
      ""wind_deg"": 234,
      ""wind_gust"": 10.11,
      ""weather"": [
        {
          ""id"": 804,
          ""main"": ""Clouds"",
          ""description"": ""overcast clouds"",
          ""icon"": ""04d""
        }
      ],
      ""clouds"": 98,
      ""pop"": 0.1,
      ""rain"": 0.0,
      ""uvi"": 5.77
    },
    {
      ""dt"": " + GetTimestamp(ref iOffset) + @",
      ""sunrise"": 1652240377,
      ""sunset"": 1652295969,
      ""moonrise"": 1652274000,
      ""moonset"": 1652235360,
      ""moon_phase"": 0.33,
      ""temp"": {
        ""day"": 21.29,
        ""min"": 12.46,
        ""max"": 22.53,
        ""night"": 16.35,
        ""eve"": 20.64,
        ""morn"": 13.72
      },
      ""feels_like"": {
        ""day"": 21.04,
        ""night"": 15.97,
        ""eve"": 20.48,
        ""morn"": 13.45
      },
      ""pressure"": 1012,
      ""humidity"": 60,
      ""dew_point"": 13.23,
      ""wind_speed"": 7.44,
      ""wind_deg"": 255,
      ""wind_gust"": 12.44,
      ""weather"": [
        {
          ""id"": 500,
          ""main"": ""Rain"",
          ""description"": ""light rain"",
          ""icon"": ""10d""
        }
      ],
      ""clouds"": 41,
      ""pop"": 0.2,
      ""rain"": 0.0,
      ""uvi"": 5.27
    },
    {
      ""dt"": " + GetTimestamp(ref iOffset) + @",
      ""sunrise"": 1652326680,
      ""sunset"": 1652382464,
      ""moonrise"": 1652365080,
      ""moonset"": 1652322540,
      ""moon_phase"": 0.36,
      ""temp"": {
        ""day"": 16.86,
        ""min"": 10.08,
        ""max"": 18.32,
        ""night"": 11.76,
        ""eve"": 15.24,
        ""morn"": 10.08
      },
      ""feels_like"": {
        ""day"": 15.96,
        ""night"": 10.92,
        ""eve"": 14.6,
        ""morn"": 9.55
      },
      ""pressure"": 1018,
      ""humidity"": 52,
      ""dew_point"": 7.02,
      ""wind_speed"": 9.43,
      ""wind_deg"": 257,
      ""wind_gust"": 15.85,
      ""weather"": [
        {
          ""id"": 500,
          ""main"": ""Rain"",
          ""description"": ""light rain"",
          ""icon"": ""10d""
        }
      ],
      ""clouds"": 82,
      ""pop"": 0.9,
      ""rain"": 1.27,
      ""uvi"": 5.46
    },
    {
      ""dt"": " + GetTimestamp(ref iOffset) + @",
      ""sunrise"": 1652412985,
      ""sunset"": 1652468959,
      ""moonrise"": 1652456400,
      ""moonset"": 1652409780,
      ""moon_phase"": 0.4,
      ""temp"": {
        ""day"": 16.31,
        ""min"": 9.21,
        ""max"": 16.31,
        ""night"": 9.21,
        ""eve"": 13.24,
        ""morn"": 11.44
      },
      ""feels_like"": {
        ""day"": 15.43,
        ""night"": 7.64,
        ""eve"": 12.55,
        ""morn"": 10.57
      },
      ""pressure"": 1018,
      ""humidity"": 55,
      ""dew_point"": 7.26,
      ""wind_speed"": 7.28,
      ""wind_deg"": 266,
      ""wind_gust"": 12.02,
      ""weather"": [
        {
          ""id"": 500,
          ""main"": ""Rain"",
          ""description"": ""light rain"",
          ""icon"": ""10d""
        }
      ],
      ""clouds"": 97,
      ""pop"": 0.31,
      ""rain"": 0.37,
      ""uvi"": 3.75
    },
    {
      ""dt"": " + GetTimestamp(ref iOffset) + @",
      ""sunrise"": 1652499292,
      ""sunset"": 1652555454,
      ""moonrise"": 1652547900,
      ""moonset"": 1652497020,
      ""moon_phase"": 0.43,
      ""temp"": {
        ""day"": 17.5,
        ""min"": 8.37,
        ""max"": 17.5,
        ""night"": 10.38,
        ""eve"": 15.11,
        ""morn"": 12.03
      },
      ""feels_like"": {
        ""day"": 16.9,
        ""night"": 9.77,
        ""eve"": 14.61,
        ""morn"": 11.46
      },
      ""pressure"": 1022,
      ""humidity"": 61,
      ""dew_point"": 9.88,
      ""wind_speed"": 6.36,
      ""wind_deg"": 275,
      ""wind_gust"": 9.75,
      ""weather"": [
        {
          ""id"": 500,
          ""main"": ""Rain"",
          ""description"": ""light rain"",
          ""icon"": ""10d""
        }
      ],
      ""clouds"": 97,
      ""pop"": 0.4,
      ""rain"": 0.89,
      ""uvi"": 4.38
    },
    {
      ""dt"": " + GetTimestamp(ref iOffset) + @",
      ""sunrise"": 1652585601,
      ""sunset"": 1652641947,
      ""moonrise"": 1652639700,
      ""moonset"": 1652584440,
      ""moon_phase"": 0.47,
      ""temp"": {
        ""day"": 17.58,
        ""min"": 7.45,
        ""max"": 18.02,
        ""night"": 9.5,
        ""eve"": 14.97,
        ""morn"": 10.74
      },
      ""feels_like"": {
        ""day"": 16.88,
        ""night"": 7.82,
        ""eve"": 14.43,
        ""morn"": 10.12
      },
      ""pressure"": 1025,
      ""humidity"": 57,
      ""dew_point"": 8.83,
      ""wind_speed"": 4.11,
      ""wind_deg"": 39,
      ""wind_gust"": 7.3,
      ""weather"": [
        {
          ""id"": 801,
          ""main"": ""Clouds"",
          ""description"": ""few clouds"",
          ""icon"": ""02d""
        }
      ],
      ""clouds"": 20,
      ""pop"": 0,
      ""uvi"": 5
    },
    {
      ""dt"": " + GetTimestamp(ref iOffset) + @",
      ""sunrise"": 1652671912,
      ""sunset"": 1652728439,
      ""moonrise"": 1652731620,
      ""moonset"": 1652672100,
      ""moon_phase"": 0.5,
      ""temp"": {
        ""day"": 19.77,
        ""min"": 6.96,
        ""max"": 20.39,
        ""night"": 10.64,
        ""eve"": 15.88,
        ""morn"": 10.76
      },
      ""feels_like"": {
        ""day"": 18.87,
        ""night"": 9.59,
        ""eve"": 15.22,
        ""morn"": 9.61
      },
      ""pressure"": 1027,
      ""humidity"": 41,
      ""dew_point"": 6.07,
      ""wind_speed"": 5.02,
      ""wind_deg"": 109,
      ""wind_gust"": 9.54,
      ""weather"": [
        {
          ""id"": 800,
          ""main"": ""Clear"",
          ""description"": ""clear sky"",
          ""icon"": ""01d""
        }
      ],
      ""clouds"": 5,
      ""pop"": 0,
      ""uvi"": 5
    }
  ]";
        
          sResponse += "}";

          return (sResponse);
        }
        #endif
    }
}