using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.Json;
using AutoMate.Services;
using Shared.Asp.GenNav;
using Shared.Utils.Core;

namespace AutoMate.Modules.Weather
{
    #region Module
    public class AmWeatherApiSettings : AmWeatherSettings
    {
        #region Constants
        public static readonly AmWeatherApiSettings Default = new AmWeatherApiSettings()
        {
            Title = "Weather Module",
            CycleInterval = new TimeSpan(12, 0, 0),
        };
        #endregion


        #region Properties
        [Category("")]
        [Description("Number of days of weather forecast.")]
        [DisplayName("Days")]
        public int Days
        {
            get => days;
            set => days = Math.Clamp(value, 1, 10);
        }
        private int days = 1;
        #endregion
    }
    
    [ModuleDescription(AmModuleCategory.eWeather, "Weather API", "api.weatherapi.com", 1,0,0)]
    public partial class AmWeatherApiModule : AmWeatherModule<AmWeatherApiModule.ApiContent, AmWeatherApiSettings>
    {
        #region Constants
        private static readonly IReadOnlyDictionary<string, IKnownField> KNOWN_FIELDS = new Dictionary<string, IKnownField>()
        {
            // Current weather:
            { "last_updated_epoch", KnownField.Timestamp },
            { "temp_c", KnownField.Temperature },
            { "humidity", KnownField.Humidity },
            { "uv", KnownField.UvIndex },
            { "precip_mm", KnownField.Rain },

            // Daily forecast:
            { "date_epoch", KnownField.Timestamp },
            { "day.maxtemp_c", KnownField.Temperature },
            { "day.avghumidity", KnownField.Humidity },
            { "day.uv", KnownField.UvIndex },
            { "day.daily_chance_of_rain", KnownField.RainPropability.WithModifier(x => x * 0.01) },
            { "day.totalprecip_mm", KnownField.Rain },
        };
        #endregion


        #region Enumerations
        [Flags]
        public enum ApiContent
        {
            [Format("Location")]
            [Description("location")]
            eLocation = 0x01,
            [Format("Current weather")]
            [Description("current")]
            eCurrent = 0x02,
            [Format("Daily forecast {0}")]
            [Description("forecast.forecastday")]
            eForecast = 0x03,
        }
        #endregion


        public AmWeatherApiModule(AmCoreService aOwner, AmWeatherApiSettings aSettings) : base(aOwner, aSettings) { }
        public AmWeatherApiModule(AmCoreService aOwner) : this(aOwner, AmWeatherApiSettings.Default) { }


        #region Properties.Management
        protected override IReadOnlyDictionary<string, IKnownField> KnownFields => KNOWN_FIELDS;
        #endregion


        #region Communication
        protected override string CreateRequest()
        {
            // [API-Docs]
            // https://www.weatherapi.com/docs
            // [Sample]
            // https://api.weatherapi.com/v1/forecast.json?key=XXX&q=51.9,8.38&days=1&aqi=no&alerts=no
            return (string.Format(
                @"https://api.weatherapi.com/v1/forecast.json?key={0}&q={1},{2}&days={3}&aqi=no&alerts=no",
                Settings.ApiKey,
                Settings.Latitude.ToString("0.0", System.Globalization.CultureInfo.InvariantCulture),
                Settings.Longitude.ToString("0.0", System.Globalization.CultureInfo.InvariantCulture),
                Settings.Days
            ));
        }
        protected override void ValidateResponse(JsonDocument document)
        {
            if (document.RootElement.TryGetProperty(out var err, "error"))
            {
                if (!err.TryGetPropertyValue<int>(out var code, "code"))
                    throw new HttpRequestException("Received bad response.");
                string msg;
                if (!err.TryGetPropertyValue(out msg, "message"))
                    msg = "Unknown error occured!";
                throw new HttpRequestException($"{msg} (Code: {code})");
            }
        }
        #endregion
    }
    #endregion
}