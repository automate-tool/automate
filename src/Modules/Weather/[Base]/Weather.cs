using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Text.Json;
using System.Timers;
using AutoMate.Services;
using Shared.Utils.Core;
using Shared.Asp.GenMenu;
using Shared.Asp.Alert;
using Microsoft.Extensions.Logging;

namespace AutoMate.Modules.Weather
{
    #region Attributes
    [AttributeUsage(AttributeTargets.Property)]
    public sealed class UnitAttribute : Attribute
    {
        public UnitAttribute(string sValue)
        {
            this.Unit = sValue;
        }

        public string Unit { private set; get; }
    }
    public sealed class FormatAttribute : Attribute
    {
        public FormatAttribute(string sFormat)
        {
            this.Format = sFormat;
        }

        public string Format { private set; get; }
    }
    #endregion
    #region Module
    public abstract class AmWeatherSettings : AmCyclicModuleSettings
    {
        #region Properties
        [Category("Request")]
        [Description("")]
        [DisplayName("Read interval")]
        public override TimeSpan CycleInterval { get; set; }

        [Category("API")]
        [Description("")]
        [DisplayName("API Key")]
        public string ApiKey { get; set; }
        
        [Category("Geographic")]
        [Description("")]
        [DisplayName("Latitude")]
        public double Latitude { get; set; }
        [Category("Geographic")]
        [Description("")]
        [DisplayName("Longitude")]
        public double Longitude { get; set; }
        #endregion
    }
    public abstract class AmWeatherModule<TApiContent, TSettings> : AmCyclicModule<TSettings>, IMenuProvider, IAlertProvider
        where TApiContent : struct, Enum
        where TSettings : AmWeatherSettings, new()
    {
        #region Enumerations
        private enum AmAlertContext
        {
            eUpdated
        }
        #endregion
        #region Types
        protected interface IKnownField
        {
            #region Properties
            Type DataType { get; }
            string VariableName { get; }
            string DisplayName { get; }
            string Description { get; }
            UnitAttribute? Unit { get; }
            #endregion


            #region Management
            object Parse(JsonElement jProperty);
            #endregion
        }
        protected class KnownFieldDescriptor<TValue> : IKnownField, ICloneable
        {
            public KnownFieldDescriptor(string sVariableName, string sDisplayName, string sDescription = "", string sUnit = "")
            {
                this.DataType = typeof(TValue);
                this.VariableName = sVariableName;
                this.DisplayName = sDisplayName;
                this.Description = sDescription;

                if (!string.IsNullOrEmpty(sUnit))
                    this.Unit = new UnitAttribute(sUnit);
            }


            #region Properties
            public Type DataType { init; get; }
            public string VariableName { init; get; }
            public string DisplayName { init; get; }
            public string Description { init; get; }
            public UnitAttribute? Unit { init; get; }
            #endregion


            #region Management
            /// <summary>
            /// Applies a modifier to translate <see cref="Parse(JsonElement)">updated</see> values.
            /// </summary>
            public KnownFieldDescriptor<TValue> WithModifier(Func<TValue, TValue> modifier)
            {
                var res = (KnownFieldDescriptor<TValue>)Clone();
                {
                    res.modifier = modifier;
                }
                return (res);
            }
            object IKnownField.Parse(JsonElement jProperty) => Parse(jProperty);
            public TValue Parse(JsonElement jProperty)
            {
                // Parse value:
                TValue res;
                if (DataType == typeof(DateTime))
                    res = (TValue)(object)UtlDateTime.FromUtc1970Time(jProperty.GetValue<long>());
                else
                    res = jProperty.GetValue<TValue>();

                // Modify value (if required):
                if (modifier is not null)
                    res = modifier(res);

                return (res);
            }
            public object Clone()
            {
                var res = new KnownFieldDescriptor<TValue>(VariableName, DisplayName, Description, Unit.Unit);
                {
                    Debug.Assert(res.DataType == this.DataType);
                    res.modifier = this.modifier;
                }
                return (res);
            }
            #endregion


            private Func<TValue, TValue>? modifier = null;
        }
        protected sealed class KnownField
        {
            public static KnownFieldDescriptor<DateTime> Timestamp = new("dt", "Timestamp", "Time of forecasted data.");
            public static KnownFieldDescriptor<DateTime> Sunrise = new("sunrise", "Sunrise", "Sunrise time.");
            public static KnownFieldDescriptor<DateTime> Sunset = new("sunset", "Sunset", "Sunset time.");
            public static KnownFieldDescriptor<DateTime> Moonrise = new("moonrise", "Moonrise", "The time of when the moon rises for this day.");
            public static KnownFieldDescriptor<DateTime> Moonset = new("moonset", "Moonset", "The time of when the moon sets for this day.");
            public static KnownFieldDescriptor<double> MoonPhase = new("moon_phase", "Moon phase", "0 and 1 are 'new moon', 0.25 is 'first quarter moon', 0.5 is 'full moon' and 0.75 is 'last quarter moon'.");
            public static KnownFieldDescriptor<double> Temperature = new("temp", "Temperature", "", "°C");
            public static KnownFieldDescriptor<double> Temperature_Morning = new("temp_morn", "Morning temperature", "", "°C");
            public static KnownFieldDescriptor<double> Temperature_Day = new("temp_day", "Day temperature", "", "°C");
            public static KnownFieldDescriptor<double> Temperature_Evening = new("temp_eve", "Evening temperature", "", "°C");
            public static KnownFieldDescriptor<double> Temperature_Night = new("temp_night", "Night temperature", "", "°C");
            public static KnownFieldDescriptor<double> Temperature_Min = new("temp_min", "Min daily temperature", "", "°C");
            public static KnownFieldDescriptor<double> Temperature_Max = new("temp_max", "Max daily temperature", "", "°C");
            public static KnownFieldDescriptor<double> FeelsLike = new("feels_like", "Feels like temperature", "This accounts for the human perception of weather.", "°C");
            public static KnownFieldDescriptor<double> FeelsLike_Morning = new("feels_like_morn", "Feels like morning temperature", "This accounts for the human perception of weather.", "°C");
            public static KnownFieldDescriptor<double> FeelsLike_Day = new("feels_like_day", "Feels like day temperature", "This accounts for the human perception of weather.", "°C");
            public static KnownFieldDescriptor<double> FeelsLike_Evening = new("feels_like_eve", "Feels like evening temperature", "This accounts for the human perception of weather.", "°C");
            public static KnownFieldDescriptor<double> FeelsLike_Night = new("feels_like_night", "Feels like night temperature", "This accounts for the human perception of weather.", "°C");
            public static KnownFieldDescriptor<double> Pressure = new("pressure", "Pressure", "Atmospheric pressure on the sea level.", "hPa");
            public static KnownFieldDescriptor<double> Humidity = new("humidity", "Humidity", "", "%");
            public static KnownFieldDescriptor<double> DewPoint = new("dew_point", "Dew point", "Atmospheric temperature (varying according to pressure and humidity) below which water droplets begin to condense and dew can form.", "°C");
            public static KnownFieldDescriptor<double> WindSpeed = new("wind_speed", "Wind speed", "", "m/sec");
            public static KnownFieldDescriptor<double> WindGust = new("wind_gust", "Wind gust", "", "m/sec");
            public static KnownFieldDescriptor<double> WindDirection = new("wind_deg", "Wind direction", "", "°");
            public static KnownFieldDescriptor<double> Clouds = new("clouds", "Cloudiness", "", "%");
            public static KnownFieldDescriptor<double> UvIndex = new("uvi", "UV index", "The maximum value of UV index for the day.");
            public static KnownFieldDescriptor<double> RainPropability = new("pop", "Rain probability", "Precipitation probability. The values of the parameter vary between 0 and 1, where 0 is equal to 0%, 1 is equal to 100%.", "%");
            public static KnownFieldDescriptor<double> Rain = new("rain", "Rain", "Precipitation volume.", "mm");
            public static KnownFieldDescriptor<double> Rain1h = new("rain_1h", "Rain (last hour)", "Rain volume for last hour.", "mm");
            public static KnownFieldDescriptor<double> Snow = new("snow", "Snow", "Snow volume.", "mm");
            public static KnownFieldDescriptor<double> Snow1h = new("snow_1h", "Snow (last hour)", "Snow volume for last hour.", "mm");
            public static KnownFieldDescriptor<double> AvgVisibility = new("visibility", "Average visibility", "Maximum value of the visibility is 10km.", "m");
            public static KnownFieldDescriptor<double> Precipitation = new("precipitation", "Precipitation volume", "", "mm");
        }
        #endregion
        

        protected AmWeatherModule(AmCoreService aOwner, TSettings aSettings) : base(aOwner, aSettings)
        {
            // Initialize menu:
            MenuButton _aBut;
            var _aGen = new MenuGroup("General");
            {
                _aGen.MenuButtons.Add(_aBut = new MenuButton("Refresh", default, true, Env.ROLE_ADMIN));
                _aBut.Click += OnMenu_Gen_Refresh;
            }
            lMenuGroups.Add(_aGen);
        }


        #region Properties.Management
        public IEnumerable<IMenuGroup> MenuGroups => lMenuGroups;
        private List<IMenuGroup> lMenuGroups = new List<IMenuGroup>();
        public IEnumerable<Alert> Alerts => lAlerts.Values;
        private Dictionary<AmAlertContext, Alert> lAlerts = new Dictionary<AmAlertContext, Alert>();

        protected abstract IReadOnlyDictionary<string, IKnownField> KnownFields { get; }
        #endregion


        #region Events
        protected override void OnCycle(object oSender, ElapsedEventArgs eArgs) => ReadForecast();
        #endregion
        #region Events.Menu
        public void OnMenu_Gen_Refresh(object oSender, MenuEventArgs mArgs)
        {
            ReadForecast();
        }
        #endregion


        #if DEBUG
        protected override void OnDebug_Init()
        {
            IsDebugging = false;

            if (IsDebugging)
                DebugState = "Simulating weather forecast.";
        }
        /// <summary>
        /// Create a simulated response.
        /// </summary>
        protected abstract string CreateDebugResponse();
        #endif


        #region Communication
        protected abstract string CreateRequest();
        protected abstract void ValidateResponse(JsonDocument document);

        private void ReadForecast()
        {
            Owner.ExecuteScoped(_aContext =>
            {
                string _sResponse = "";

                Logger.LogInformation(284, "Updating weather forecast.");

                #if DEBUG
                if (IsDebugging)
                    // [Debug] Simulate response:
                    _sResponse = CreateDebugResponse();
                #endif

                if (string.IsNullOrEmpty(_sResponse))
                {
                    // Post request:
                    using (HttpClient hClient = new HttpClient(new HttpClientHandler { AutomaticDecompression = (DecompressionMethods.GZip | DecompressionMethods.Deflate) }))
                    {
                        var req = CreateRequest();
                        var rsp = hClient.PostAsync(req, new StringContent("", Encoding.UTF8, "application/json")).Result;
                        _sResponse = rsp.Content.ReadAsStringAsync().Result;
                    }
                }

                // Evaluate response:
                JsonDocument _jDoc = JsonDocument.Parse(_sResponse);
                
                ValidateResponse(_jDoc);

                foreach (var _aContent in Enum.GetValues<TApiContent>())
                {
                    var _sForecast = UtlEnum.GetEnumDescription(_aContent).Split(".");
                    if (_jDoc.RootElement.TryGetProperty(out var _jForecast, ArrayElementSelectionMode.eSingle, _sForecast))
                    {
                        // Determine weather data:
                        JsonElement[] jData;
                        switch (_jForecast.ValueKind)
                        {
                            case JsonValueKind.Object:
                                jData = new [] { _jForecast };
                                break;
                            case JsonValueKind.Array:
                                jData = _jForecast.EnumerateArray().ToArray();
                                break;
                            default:
                                throw new NotImplementedException(string.Format("Not implemented dataset of kind '{0}' received!", _jForecast.ValueKind));
                        }

                        // Evaluate weather data:
                        var _aFormat = UtlEnum.GetCustomAttribute<FormatAttribute>(_aContent);
                        int i = 0;
                        foreach (var _jEntry in jData)
                        {
                            var _aDataset = CreateOrUpdateDataset<AmDynamicModuleDataset>(string.Format(_aFormat.Format, ++i), _aDataset =>
                            {
                                foreach (var _kProp in _jEntry.ToPathDictionary())
                                {
                                    var _sVarName = _kProp.Key;
                                    if (KnownFields.TryGetValue(_sVarName, out var _iField))
                                    {
                                        var _oVal = _iField.Parse(_kProp.Value);
                                        if (_aDataset.TryGetVariable(_iField.VariableName, out var _iVar))
                                            ((AmVariable)_iVar).SetValue(_oVal, this);
                                        else
                                        {
                                            var _aVar = (AmVariable)_aDataset.AddDynamicVariable(AmVariableType.eDisplay, _iField.DataType, _iField.VariableName, _oVal);
                                            {
                                                _aVar.DisplayName = _iField.DisplayName;
                                                _aVar.Description = _iField.Description;
                                            }
                                        }
                                    }
                                    else
                                        ; // Skip (Not implemented field).
                                }
                            });
                            _aDataset.Tag = _aContent;
                        }
                    }
                }

                lAlerts.AddOrUpdate(AmAlertContext.eUpdated, new Alert(
                    string.Format("Last <b>weather update</b>: {0:dddd, d.MMMM.yyyy} at {0:HH:mm}.", DateTime.Now),
                    AlertIcon.eInfo,
                    AlertLayout.eSuccess
                ));
            });
        }
        #endregion


        #region Helper
        protected static string GetTimestamp(ref int iOffset)
        {
          var _dNow = DateTime.Now.AddDays(iOffset++);
          _dNow = _dNow.Date.AddHours(_dNow.Hour);
          return (UtlDateTime.ToUtc1970Time(_dNow).ToString());
        }
        protected static string GetTimestamp(ref int iOffset, out string formatedTimestamp)
        {
          var _dNow = DateTime.Now.AddDays(iOffset++);
          _dNow = _dNow.Date.AddHours(_dNow.Hour);
          formatedTimestamp = _dNow.ToShortDateString();
          return (UtlDateTime.ToUtc1970Time(_dNow).ToString());
        }
        #endregion
    }
    #endregion
}