
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Timers;
using System.Reflection;
using Microsoft.Extensions.Logging;
using Shared.Utils.Core;
using System.IO;
using System.Text.Json;
using System.Text;
using System.Threading;
using System.Diagnostics;
using AutoMate.Logger;
using AutoMate.Services;
using AutoMate.Modules.Script;

namespace AutoMate.Modules
{
    #region Attributes
    public abstract class AmComponentAttribute : Attribute
    {
        public AmComponentAttribute(int iMajor, int iMinor, int iBuild = 0, int iRevision = 0)
        {
            this.Version = new Version(iMajor, iMinor, iBuild, iRevision);
        }


        #region Properties
        public Version Version { init; get; }
        #endregion
    }
    public class ModuleDescriptionAttribute : AmComponentAttribute
    {
        public ModuleDescriptionAttribute(AmModuleCategory aCategory, string sDisplayName, string sDomain, int iMajor, int iMinor, int iBuild = 0, int iRevision = 0) : base(iMajor, iMinor, iBuild, iRevision)
        {
            this.Category = aCategory;
            this.DisplayName = sDisplayName;
            this.Domain = sDomain;
        }


        #region Properties.Management
        public AmModuleCategory Category { init; get; }
        public string DisplayName { init; get; }
        public string Domain { init; get; }
        #endregion
    }
    #endregion
    #region Enumerations
    public enum AmModuleCategory
    {
        [Description("I/O")]
        eIO,
        [Description("Script")]
        eScript,
        [Description("Weather")]
        eWeather
    }
    public enum AmModuleState
    {
        [Description("Error")]
        eError,
        [Description("Ok")]
        eOk
    }
    #endregion


    #region Types
    public class AmIdentityComparer : IEqualityComparer<IAmModuleID>
    {
        public AmIdentityComparer()
        {
        }


        #region Management
        public bool Equals(IAmModuleID iId1, IAmModuleID iId2) => (iId1.Identifier == iId2.Identifier);
        public int GetHashCode([DisallowNull] IAmModuleID iObject) => (int)iObject.Identifier;
        #endregion
    }
    public class AmTypeOrderComparer : IComparer<AmModuleSettings>
    {
        public int Compare(AmModuleSettings aSettings1, AmModuleSettings aSettings2)
        {
            bool _b1 = (aSettings1 is AmScriptingHostSettings);
            bool _b2 = (aSettings2 is AmScriptingHostSettings);

            if (_b1 == _b2)
                return (0);
            else
                return (_b1 ? 1 : -1);
        }
    }
    #endregion
    #region Types.Diagnosis
    public class CycleData
    {
        public CycleData(DateTime dTimestamp, TimeSpan tLength, double dLoad)
        {
            this.Timestamp = dTimestamp;
            this.Length = tLength;
            this.Load = dLoad;
        }


        #region Properties
        public DateTime Timestamp { init; get; }
        public TimeSpan Length { init; get; }
        /// Load of cycle (in [%]).
        public double Load{ init; get; }
        #endregion


        public override string ToString() => string.Format("[CYCLE] {0}: {1} ({2} %)", Timestamp, Length, Load);
    }
    public interface ICycleDiag
    {
        #region Properties.Management
        bool Ready { get; }
        bool Critical { get; }
        #endregion
        #region Properties
        TimeSpan MeasuredAveragePeriod { get; }
        TimeSpan AverageCycleLength { get; }
        double AverageLoad { get; }
        double MinLoad { get; }
        double MaxLoad { get; }
        ulong ExceededCycles { get; }
        #endregion
    }
    internal class CycleDiag : Queue<CycleData>, ICycleDiag
    {
        public CycleDiag(IAmCyclicModuleSettings iSettings, int iMaxElements)
        {
            this.Settings = (IAmCyclicModuleSettings)iSettings;
            this.iMaxElements = iMaxElements;
        }


        #region Properties.Management
        private IAmCyclicModuleSettings Settings { init; get; }
        public bool Ready => !this.IsEmpty();
        public bool Critical => (ExceededCycles > 0);
        #endregion
        #region Properties
        public TimeSpan MeasuredAveragePeriod => (this.Last().Timestamp - this.First().Timestamp);
        public TimeSpan AverageCycleLength => new TimeSpan((long)this.Average(_cDiag => _cDiag.Length.Ticks));
        public double AverageLoad => UtlMath.GetPercent(AverageCycleLength.Ticks, Settings.CycleInterval.Ticks);
        public double MinLoad { private set; get; }
        public double MaxLoad { private set; get; }
        public ulong ExceededCycles { private set; get; }
        #endregion


        #region Initialization
        public new void Clear()
        {
            base.Clear();

            MinLoad = 0;
            MaxLoad = 0;
            ExceededCycles = 0;
        }
        #endregion
        #region Management
        private new void Enqueue(CycleData cData) { }
        public CycleData Enqueue(TimeSpan tCycleLength, out bool bExceeded)
        {
            var _fLoad = UtlMath.GetPercent(tCycleLength.Ticks, Settings.CycleInterval.Ticks);
            var cResult = new CycleData(DateTime.Now, tCycleLength, _fLoad);

            if (this.IsEmpty())
                MinLoad = MaxLoad = _fLoad;
            else
            {
                MinLoad = Math.Min(MinLoad, _fLoad);
                MaxLoad = Math.Max(MaxLoad, _fLoad);
            }

            base.Enqueue(cResult);
            if (this.Count > iMaxElements)
                this.Dequeue();

            bExceeded = (tCycleLength > Settings.CycleInterval);
            if (bExceeded)
                ExceededCycles++;

            return (cResult);
        }
        #endregion


        #region Helper

        #endregion


        public override string ToString() => string.Format("[LOAD] Min: {0}%, Max: {1}%, Avg: {2}% @ last {3}", MinLoad, MaxLoad, AverageLoad, MeasuredAveragePeriod);


        private int iMaxElements;
    }
    #endregion
    #region Types.Module
    public interface IAmContext
    {
    }
    public interface IAmModuleID
    {
        #region Properties.Management
        long Identifier { get; }
        #endregion
    }
    public interface IAmModule : IAmModuleID, IAmContext
    {
        #region Properties.Management
        IReadOnlyDictionary<string, IAmModuleDataset> Datasets { get; }

        SemaphoreSlim WorkSemaphore { get; }
        #endregion
        #region Properties
        string Title { get; }
        string Comment { get; }
        object Tag { set; get; }
        #endregion
    }
    public abstract class AmModule : IAmModule, IDisposable
    {
        #region Constants
        internal static readonly Dictionary<AmDataType, Type> TYPE_MAP = new Dictionary<AmDataType, Type>()
        {
            { AmDataType.eBoolean, typeof(bool) },
            { AmDataType.eInteger, typeof(int) },
            { AmDataType.eDouble, typeof(double) },
            { AmDataType.eString, typeof(string) },
            { AmDataType.eDateTime, typeof(DateTime) },
            { AmDataType.eTimeSpan, typeof(TimeSpan) }
        };
        internal static readonly Dictionary<AmVariableType, Type> VARIABLE_MAP = new Dictionary<AmVariableType, Type>()
        {
            { AmVariableType.eDisplay, typeof(AmDisplayVariable<>) },
            { AmVariableType.eSetpoint, typeof(AmSetpointVariable<>) },
            { AmVariableType.eSetpointButton, typeof(AmSetpointButtonVariable<>) },
            { AmVariableType.eInput, typeof(AmInputVariable<>) },
            { AmVariableType.eOutput, typeof(AmOutputVariable<>) },
        };
        #endregion


        #region Types
        public class AmState
        {
            #region Constants
            public static readonly AmState Ok = new AmState(AmModuleState.eOk);
            #endregion


            public AmState(AmModuleState aState, string sMessage = "")
            {
                this.State = aState;
                this.Message = sMessage;
            }
            public AmState(Exception eException)
            {
                this.State = AmModuleState.eError;
                this.Message = eException.Message;
            }


            #region Properties
            public AmModuleState State { init; get; }
            public string Message { init; get; }
            #endregion
        }
        #endregion


        public AmModule(AmCoreService aOwner)
        {
            this.Owner = aOwner;
            this.Description = GetType().GetCustomAttribute<ModuleDescriptionAttribute>();
        }


        #region Properties.Management
        public ILogger Logger { init; get; }
        protected AmCoreService Owner { init; get; }

        [Category("Datasets")]
        [ReadOnly(true)]
        public IReadOnlyDictionary<string, IAmModuleDataset> Datasets => dDatasets.ToDictionary(iPair => iPair.Key, iPair => iPair.Value as IAmModuleDataset);
        private Dictionary<string, AmModuleDataset> dDatasets = new Dictionary<string, AmModuleDataset>();

        public ModuleDescriptionAttribute Description { get; }
        public AmState State { set; get; } = AmState.Ok;
        /// Indicates that a modules is executing work.
        public SemaphoreSlim WorkSemaphore { get; } = new SemaphoreSlim(1, 1);
        #endregion
        #region Properties
        public string Title => GetSettings().Title;
        public string Comment => GetSettings().Comment;
        public object Tag { set; get; }
        public long Identifier => GetSettings().Identifier;
        #endregion


        #region Events.Initialization
        public virtual bool OnInitModule(bool bReInit = false)
        {
            return (true);
        }
        public virtual void OnPostInitModule(bool bReInit = false)
        {
            // Read persistent data:
            Owner.ExecuteScoped(_aContext => _aContext.ReadModulePersistence(this));
            
            #if DEBUG
            OnDebug_Init();
            #endif

            Logger.LogInformation(298, string.Format("Started module '{0}'.", this));
        }
        #endregion
        #region Events.Configuration
        public abstract void OnChangingModuleSettings(AmModuleSettings aNewSettings);
        #endregion
        #region Events
        public virtual void OnApplyValues()
        {
        }
        #endregion


        #region Management
        public virtual void Dispose()
        {
            Logger.LogInformation(309, string.Format("Disposing module '{0}'.", this));

            #if DEBUG
            OnDebug_Exit();
            
            IsDebugging = false;
            DebugState = "";
            #endif

            // Update persistent data:
            Owner.ExecuteScoped(_aContext => _aContext.WriteModulePersistence(this, false));
            
            dDatasets.Clear();
        }
        public override string ToString() => Title;
        #endregion
        #region Management
        public static bool IdentifierEquals(IAmModuleID iId1, IAmModuleID iId2)
        {
            return (iId1.Identifier == iId2.Identifier);
        }
        public bool IdentifierEquals(IAmModuleID iId) => IdentifierEquals(this, iId);
        #endregion
        #region Management.Settings
        public abstract AmModuleSettings GetSettings();

        /// <summary>
        /// Creates or updates a dataset.
        /// </summary>
        /// <typeparam name="sName">Name of dataset.</typeparam>
        /// <typeparam name="aUpdate">Update command (Executed, even if created new).</typeparam>
        protected TDataset CreateOrUpdateDataset<TDataset>(string sName, Action<TDataset> aUpdate)
            where TDataset : AmModuleDataset, new()
        {
            TDataset _tDataset;
            if (dDatasets.TryGetValue(sName, out var _aDataset))
                _tDataset = (TDataset)_aDataset;
            else
            {
                _tDataset = new TDataset();
                RefreshMetaData(_tDataset);
                dDatasets.Add(sName, _tDataset);
            }
            aUpdate(_tDataset);
            
            // Notify subscriptions:
            _tDataset._OnUpdated();

            return (_tDataset);
        }
        /// Adds a (dynamic / non-hardcoded) dataset of variables.
        protected AmDynamicModuleDataset AddDynamicVariables(IEnumerable<AmVariableSettings> aVariables, string sDataset, string sDescription = "")
        {
            // Create dataset:
            var _dVariables = new AmDynamicModuleDataset(sDescription);
            foreach (var _aSetting in aVariables)
            {
                var _tDataType = TYPE_MAP[_aSetting.DataType];
                var _aVar = (AmVariable)_dVariables.AddDynamicVariable(_aSetting.VariableType, _tDataType, _aSetting.VariableName);
                {
                    _aVar.ReferredSetting = _aSetting;
                    _aVar.Category = "";
                    _aVar.DisplayName = _aSetting.VariableName;
                    _aVar.Description = _aSetting.Description;
                }
            }
            dDatasets.AddOrUpdate(sDataset, _dVariables);
            return (_dVariables);
        }
        /// Refreshes metadata of a dataset.
        private void RefreshMetaData(AmModuleDataset aDataset)
        {
            foreach (var _pProp in aDataset.GetType().GetProperties())
            {
                if (_pProp.PropertyType.IsSubclassOf(typeof(AmVariable)))
                {
                    var _aVar = (AmVariable)_pProp.GetValue(aDataset);

                    _aVar.VariableName = _pProp.Name;
                    _aVar.Category = GetAttributeValue<CategoryAttribute, string>(_pProp);
                    _aVar.DisplayName = GetAttributeValue<DisplayNameAttribute, string>(_pProp);
                    _aVar.Description = GetAttributeValue<DescriptionAttribute, string>(_pProp);

                    // Apply defaults:
                    if (string.IsNullOrEmpty(_aVar.Category))
                        _aVar.Category = "General";
                    if (string.IsNullOrEmpty(_aVar.DisplayName))
                        _aVar.DisplayName = _aVar.VariableName;
                }
            }
        }
        #endregion
        #region Management.Persistence
        internal void ReadPersistence(string sData)
        {
            using (var _jDoc = JsonDocument.Parse(sData))
            {
                foreach (var _jDataset in _jDoc.RootElement.EnumerateObject())
                {
                    if (Datasets.TryGetValue(_jDataset.Name, out var _iDataset))
                    {
                        foreach (var _jVar in _jDataset.Value.EnumerateObject())
                        {
                            if (_iDataset.TryGetVariable(_jVar.Name, out var _iVar))
                                _iVar.ParseValue(_jVar.Value.GetValue<string>());
                        }
                    }
                }
            }
        }
        internal string WritePersistence()
        {
            using (var _mStream = new MemoryStream())
            using (var _uWriter = new Utf8JsonWriter(_mStream))
            {
                _uWriter.WriteStartObject();
                foreach (var _iDataset in Datasets)
                {
                    // Filter persistent variable types:
                    var _iPersistent = _iDataset.Value.Variables.Where(_iVar => (_iVar.VariableType == AmVariableType.eSetpoint));
                    if (_iPersistent.Count() > 0)
                    {
                        _uWriter.WriteStartObject(_iDataset.Key);
                        {
                            _iPersistent.ForEach(_iVar => _uWriter.WriteString(_iVar.VariableName, _iVar.GetValue().ToString()));
                        }
                        _uWriter.WriteEndObject();
                    }
                }
                _uWriter.WriteEndObject();
                _uWriter.Flush();

                return (Encoding.UTF8.GetString(_mStream.ToArray())
                    // Reject empty json data:
                    .Replace("{}", "")
                );
            }
        }
        #endregion


        #region Helper
        private V GetAttributeValue<T, V>(PropertyInfo pProperty) where T : Attribute
        {
            V _oVal;
            if (pProperty.TryGetAttributeValue<T, V>(out _oVal))
                return (_oVal);
            else
                return (default(V));
        }
        #endregion


        #if DEBUG
        public bool IsDebugging { protected set; get; }
        public string DebugState { protected set; get; }
        protected virtual void OnDebug_Init()
        {
        }
        protected virtual void OnDebug_Exit()
        {
        }
        #endif
    }
    public abstract class AmModule<TSettings> : AmModule
        where TSettings : AmModuleSettings, new()
    {
        public AmModule(AmCoreService aOwner, TSettings aDefaults = null) : base(aOwner)
        {
            this.Settings = (aDefaults == null) ? new TSettings() : aDefaults;
            this.Logger = Env.CreateLogger(GetType(), Settings.Identifier.ToString());
        }


        #region Properties.Management
        public TSettings Settings { private set; get; }
        #endregion


        #region Events
        public override void OnChangingModuleSettings(AmModuleSettings aNewSettings)
        {
            var _aSettings = (TSettings)aNewSettings;

            OnChangingModuleSettings(_aSettings);

            // Apply new settings:
            Settings = _aSettings;
        }
        protected virtual void OnChangingModuleSettings(TSettings tNewSettings)
        {
            Logger.LogInformation(499, string.Format("Updating settings of module '{0}'.", this));
        }
        #endregion


        #region Management
        public override AmModuleSettings GetSettings() => Settings;
        #endregion
    }
    public interface IAmCyclicModule : IAmModule
    {
        #region Properties.Management
        bool RunState { get; }
        ICycleDiag Diagnosis { get; }
        #endregion
    }
    public abstract class AmCyclicModule<TSettings> : AmModule<TSettings>, IAmCyclicModule, IDisposable
        where TSettings : AmCyclicModuleSettings, new()
    {
        #region Constants.Settings
        private const bool EXECUTE_CYCLE_ON_REINIT = true;
        private const int DIAG_CYCLES = 11;
        #endregion


        public AmCyclicModule(AmCoreService aOwner, TSettings aDefaults = null) : base(aOwner, aDefaults)
        {
            tTimer.Elapsed += _OnCycle;
            tTimer.AutoReset = true;
        }


        #region Properties.Management
        public bool RunState
        {
            set { tTimer.Enabled = value; }
            get => tTimer.Enabled;
        }
        public ICycleDiag Diagnosis => cDiagnosis;
        private CycleDiag cDiagnosis = null;
        #endregion


        #region Events.Initialization
        public override void OnPostInitModule(bool bReInit = false)
        {
            base.OnPostInitModule(bReInit);

            cDiagnosis = new CycleDiag(Settings, DIAG_CYCLES);
            
            var _iInterval = (Settings.CycleInterval.TotalMilliseconds);
            if ((!bReInit) || (EXECUTE_CYCLE_ON_REINIT) || (tTimer.Interval != _iInterval))
            {
                tTimer.Stop();
                if (_iInterval > 0)
                {
                    // Execute first cycle:
                    if ((!bReInit) || (EXECUTE_CYCLE_ON_REINIT))
                        OnCycle(null, null);

                    tTimer.Interval = _iInterval;
                    tTimer.Start();
                }
            }
        }
        #endregion
        #region Events
        protected abstract void OnCycle(object oSender, ElapsedEventArgs eArgs);
        private void _OnCycle(object oSender, ElapsedEventArgs eArgs)
        {
            if (WorkSemaphore.CurrentCount == 0)
                ; // Skip new cycle to avoid parallel cycles!
            else
            {
                try
                {
                    sStopwatch.Restart();
                    {
                        OnCycle(oSender, eArgs);
                    }
                    sStopwatch.Stop();
                    var _cData = cDiagnosis.Enqueue(sStopwatch.Elapsed, out var _bExceeded);

                    /*
                    // [DEBUG] Cyclic diagnosis:
                    Debug.WriteLine(_cData);
                    Debug.WriteLine(cDiag);
                    */

                    if (_bExceeded)
                        Logger.LogError(589, new ThreadInterruptedException(), string.Format("Cycle exceeded in module '{0}'!", this));
                }
                catch (Exception _eExcpt)
                {
                    Logger.LogError(593, _eExcpt, string.Format("Detected failed cycle in module '{0}'!", this));
                }
            }
        }
        #endregion


        #region Management
        public override void Dispose()
        {
            tTimer.Stop();
            cDiagnosis?.Clear();

            base.Dispose();
        }
        #endregion


        private System.Timers.Timer tTimer = new System.Timers.Timer();

        private Stopwatch sStopwatch = new Stopwatch();
    }
    #endregion
}