
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text.Json;
using Shared.Asp.GenNav;
using Shared.Utils.Core;


namespace AutoMate.Modules
{
    #region Types
    public interface IAmModuleSettings
    {
        #region Properties.Management
        long Identifier { get; }
        bool Enabled { get; }
        #endregion
        #region Properties
        string Title { get; }
        string Comment { get; }
        #endregion
    }
    public abstract class AmModuleSettings : IAmModuleID, IAmModuleSettings
    {
        #region Constants
        public const string CAT_GENERAL = "General";
        #endregion
        #region Constants.JSON.Workaround
        private static readonly JsonSerializerOptions JSON_OPTIONS = new JsonSerializerOptions();
        static AmModuleSettings()
        {
            JSON_OPTIONS.Converters.Add(new System.Text.Json.Workaround.TimeSpanConverter());
        }
        #endregion


        public AmModuleSettings()
        {
            Identifier = DateTime.Now.Ticks;
            Enabled = true;
        }


        #region Properties.Management
        public long Identifier { set; get; }
        [Category(CAT_GENERAL)]
        [Description("")]
        [DisplayName("Enabled")]
        public bool Enabled { set; get; }
        #endregion
        #region Properties
        [Category(CAT_GENERAL)]
        [Description("")]
        [DisplayName("Title")]
        public string Title { set; get; }
        [Category(CAT_GENERAL)]
        [Description("")]
        [DisplayName("Comment")]
        public string Comment { set; get; }
        #endregion


        #region Management
        /// Reads configuration from settings.
        public static SettingsType Read<SettingsType>(string sData) where SettingsType : AmModuleSettings
        {
            return (JsonSerializer.Deserialize<SettingsType>(sData, JSON_OPTIONS));
        }
        /// Reads configuration from settings.
        public static AmModuleSettings Read(string sType, string sData)
        {
            return ((AmModuleSettings)Read(Assembly.GetExecutingAssembly().GetType(sType), sData));
        }
        /// Reads configuration from settings.
        public static AmModuleSettings Read(Type tType, string sData)
        {
            return ((AmModuleSettings)JsonSerializer.Deserialize(sData, tType, JSON_OPTIONS));
        }
        /// Writes configuration to settings.
        public string Write()
        {
            return (JsonSerializer.Serialize(this, GetType(), JSON_OPTIONS));
        }
        #endregion
        #region Management
        /// Get type of module-settings type from module type.
        public static Type GetSettingsType(string sModuleType)
        {
            return (GetSettingsType(Type.GetType(sModuleType)));
        }
        /// Get type of module-settings type from module type.
        public static Type GetSettingsType(Type tModuleType)
        {
            return (tModuleType.GetProperties().First(_pProp => (_pProp.PropertyType.IsSubclassOf(typeof(AmModuleSettings)))).PropertyType);
        }
        /// Create settings-instance of module type.
        public static AmModuleSettings CreateSettingsInstance(string sModuleType)
        {
            return (CreateSettingsInstance(GetSettingsType(sModuleType)));
        }
        /// Create settings-instance of module type.
        public static AmModuleSettings CreateSettingsInstance(Type tModuleType)
        {
            return ((AmModuleSettings)Activator.CreateInstance(tModuleType));
        }
        /// Create default settings-instance of module type.
        public static AmModuleSettings GetSettingsDefaults(string sModuleType)
        {
            return (GetSettingsDefaults(GetSettingsType(sModuleType)));
        }
        /// Create default settings-instance of module type.
        public static AmModuleSettings GetSettingsDefaults(Type tModuleType)
        {
            var _fDefault = tModuleType.GetField("Default");
            if (_fDefault == null)
                // No defaults available:
                // > Create new instance.
                return (CreateSettingsInstance(tModuleType));
            else
                return ((AmModuleSettings)_fDefault.GetValue(null));
        }
        #endregion
        #region Management
        public bool IdentifierEquals(IAmModuleID iId)
        {
            return (AmModule.IdentifierEquals(this, iId));
        }
        #endregion
    }
    public interface IAmCyclicModuleSettings
    {
        #region Properties
        TimeSpan CycleInterval { get; }
        #endregion
    }
    public abstract class AmCyclicModuleSettings : AmModuleSettings, IAmCyclicModuleSettings
    {
        #region Properties
        public abstract TimeSpan CycleInterval { get; set; }
        #endregion
    }
    [Layout(LayoutType.eInputGroup)]
    public class AmVariableSettings
    {
        #region Properties
        [DisplayName("Variable")]
        [Description("Name")]
        public string VariableName { set; get; }
        [DisplayName("")]
        [Description("Description")]
        public string Description { set; get; }
        [DisplayName("")]
        [Description("Variable Type")]
        public AmVariableType VariableType { set; get; } = AmVariableType.eDisplay;
        [DisplayName("")]
        [Description("Data Type")]
        public AmDataType DataType { set; get; } = AmDataType.eBoolean;
        #endregion
    }
    #endregion
}