using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using Shared.Asp.GenNav;
using Shared.Utils.Core;
using System.Collections;


namespace AutoMate.Modules
{
    #region Enumerations
    public enum AmVariableType
    {
        Invalid,

        [Description("Display")]
        eDisplay = 10,
        [Description("Setpoint")]
        eSetpoint = 11,
        [Description("Button")]
        eSetpointButton = 20,
        [Description("Input")]
        eInput = 30,
        [Description("Output")]
        eOutput = 40
    }
    public enum AmDataType
    {
        Invalid,
        
        [Description("Boolean")]
        eBoolean = 10,
        [Description("Integer")]
        eInteger = 20,
        [Description("Double")]
        eDouble = 21,
        [Description("String")]
        eString = 30,
        [Description("DateTime")]
        eDateTime = 40,
        [Description("TimeSpan")]
        eTimeSpan = 41
    }
    [Flags]
    public enum ValueChangedFlags
    {
        None = 0,
        Impulse = (eShortImpulse | eLongImpulse),

        /// Indicates a rising edge, rapidly followed by a falling edge.
        /// Bouncing is assumed to be an electrical issue (Used for physical input variables only).
        eBounced = 0x01,

        /// Indicates an impulse over a short priod of time.
        eShortImpulse = 0x10,
        /// Indicates an impulse over a long priod of time.
        eLongImpulse = 0x20
    }
    #endregion


    #region Types.Event
    public class ValueChangedEventArgs : EventArgs
    {
        public ValueChangedEventArgs(object oPreviousValue, ValueChangedFlags vFlags)
        {
            this.PreviousValue = oPreviousValue;
            this.Flags = vFlags;
        }


        #region Properties
        public object PreviousValue { init; get; }
        public ValueChangedFlags Flags { init; get; }
        #endregion
    }
    #endregion
    #region Types

    /// <summary>
    /// A lockable component is something that can be used in different contexts, but not simultaneously (and should be locked therefore)!
    /// </summary>
    public interface IAmLockable
    {
        #region Properties.Management
        internal AmLock Lockup { get; }
        internal IAmContext LockContext => Lockup.LockContext;
        #endregion
        #region Properties
        bool Locked => Lockup.Locked;
        #endregion


        #region Management
        /// <summary>
        /// Checks if the current instance is locked by a certain context.
        /// </summary>
        /// <typeparam name="iOwner">Context to check.</typeparam>
        bool IsLockedBy(IAmContext iOwner) => (iOwner ?.Equals(LockContext) ??false);
        /// <summary>
        /// Locks the current instance.
        /// </summary>
        /// <typeparam name="iOwner">Locking context.</typeparam>
        void Lock(IAmContext iOwner) => Lockup.Lock(iOwner);
        /// <summary>
        /// Tries to lock the current instance.
        /// </summary>
        /// <typeparam name="iOwner">Locking context.</typeparam>
        bool TryLock(IAmContext iOwner) => Lockup.TryLock(iOwner);
        /// <summary>
        /// Unlocks the current instance.
        /// </summary>
        /// <typeparam name="iOwner">Unlocking context.</typeparam>
        void Unlock(IAmContext iOwner) => Lockup.Unlock(iOwner);
        /// <summary>
        /// Tries to unlock the current instance.
        /// </summary>
        /// <typeparam name="iOwner">Unlocking context.</typeparam>
        bool TryUnlock(IAmContext iOwner) => Lockup.TryUnlock(iOwner);
        #endregion
    }
    internal abstract class AmLock
    {
        #region Types
        public class AmTemporaryLock : AmLock
        {
            #region Properties
            public override string Name { get; } = "Temporary context";
            #endregion
        }
        #endregion


        #region Properties.Management
        public IAmContext LockContext { private set; get; }

        public static AmTemporaryLock TemporaryLock { get; } = new AmTemporaryLock();
        public static object ThreadLockObject { get; } = new();

        public bool Locked => (LockContext != null);
        #endregion
        #region Properties
        public abstract string Name { get; }
        #endregion


        #region Management
        public void Lock(IAmContext iOwner)
        {
            lock (ThreadLockObject)
            {
                if (!TryLock(iOwner))
                    throw new InvalidOperationException(string.Format("Failed to lock '{0}' because it is lock within another context!", Name));
            }
        }
        public bool TryLock(IAmContext iOwner)
        {
            lock (ThreadLockObject)
            {
                if ((Locked) && (iOwner != LockContext))
                    return (false);
                else
                {
                    LockContext = iOwner;
                    return (true);
                }
            }
        }
        public void Unlock(IAmContext iOwner)
        {
            lock (ThreadLockObject)
            {
                if (!TryUnlock(iOwner))
                    throw new InvalidOperationException(string.Format("Failed to unlock '{0}' because it is lock within another context!", Name));
            }
        }
        public bool TryUnlock(IAmContext iOwner)
        {
            lock (ThreadLockObject)
            {
                if ((Locked) && (iOwner != LockContext))
                    return (false);
                else
                {
                    LockContext = null;
                    return (true);
                }
            }
        }

        public void Validate(string sOperation, IAmContext iContext)
        {
            if (LockContext == null)
                ; // No lock acquired.
            else if ((iContext != null) && (LockContext == iContext))
                ; // Context valid.
            else if ((TemporaryLock.LockContext != null) && ((iContext == null) || (iContext == TemporaryLock.LockContext)))
                ; // Context valid.
            else
            {
                string _sContext = iContext ?.ToString() ??"<Unknown>";
                throw new InvalidOperationException(string.Format("'{0}' failed operation '{1}' due to violated lock from invalid context '{2}'!", Name, sOperation, _sContext));
            }
        }
        #endregion
    }
    internal class AmVariableLock : AmLock
    {
        public AmVariableLock(IAmVariable iOwner)
        {
            this.Owner = iOwner;
        }


        #region Properties.Management
        public IAmVariable Owner { init; get; }
        #endregion
        #region Properties
        public override string Name => Owner.DisplayName;
        #endregion
    }
    public interface IAmModuleDataset
    {
        #region Properties.Management
        IEnumerable<IAmVariable> Variables { get; }
        object Tag { set; get; }
        #endregion
        #region Properties
        string Description { get; }
        #endregion


        #region Events
        event EventHandler Updated;
        #endregion


        #region Management
        IAmVariable this[string sVariable] { get; }
        #endregion
        #region Management
        IAmVariable GetVariable(string sName);
        IAmVariable<T> GetVariable<T>(string sName);
        bool TryGetVariable(string sName, out IAmVariable iVariable);
        bool TryGetVariable<T>(string sName, out IAmVariable<T> iVariable);
        T GetValue<T>(string sVariable);
        bool TryGetValue<T>(string sVariable, out T tValue);
        #endregion
    }
    /// Dataset.
    /// - Navigation: Exposed (-> browsable) members will be parsed during navigation.
    public class AmModuleDataset : IAmModuleDataset
    {
        public AmModuleDataset(string sDescription = "")
        {
            Description = sDescription;
        }


        #region Properties.Management
        [Browsable(false)]
        public virtual IEnumerable<IAmVariable> Variables
        {
            get
            {
                // Parse instance for decleared variables:
                foreach (var _pProp in GetType().GetProperties())
                {
                    if (_pProp.PropertyType.IsSubclassOf(typeof(AmVariable)))
                        yield return ((IAmVariable)_pProp.GetValue(this));
                }
            }
        }
        [Browsable(false)]
        public object Tag { set; get; }
        #endregion
        #region Properties
        [DisplayTextAttribute(TextStyle.eMuted)]
        public string Description { private set; get; }
        #endregion


        #region Events
        public event EventHandler Updated;
        #endregion


        #region Management.Events
        internal void _OnUpdated() => Updated?.Invoke(this, new EventArgs());
        #endregion
        #region Management
        public IAmVariable this[string sVariable] => GetVariable(sVariable);
        #endregion
        #region Management
        public IAmVariable GetVariable(string sName) => Variables.First(_iVar => (_iVar.VariableName == sName));
        public IAmVariable<T> GetVariable<T>(string sName) => (IAmVariable<T>)GetVariable(sName);
        public bool TryGetVariable(string sName, out IAmVariable iVariable)
        {
            iVariable = Variables.FirstOrDefault(_iVar => (_iVar.VariableName == sName));
            return (iVariable != null);
        }
        public bool TryGetVariable<T>(string sName, out IAmVariable<T> iVariable)
        {
            IAmVariable _iVar = null;
            if (TryGetVariable(sName, out _iVar))
                iVariable = (_iVar as IAmVariable<T>);
            else
                iVariable = null;
            return (iVariable != null);
        }
        public T GetValue<T>(string sVariable) => (T)GetVariable(sVariable).GetValue();
        public bool TryGetValue<T>(string sVariable, out T tValue)
        {
            IAmVariable _iVar;
            if (TryGetVariable(sVariable, out _iVar))
            {
                tValue = (T)_iVar.GetValue();
                return (true);
            }
            else
            {
                tValue = default(T);
                return (true);
            }
        }
        #endregion
    }
    /// Dynamic dataset.
    /// - Navigation: Scans members to represent during navigation.
    public class AmDynamicModuleDataset : AmModuleDataset, IContentProvider
    {
        public AmDynamicModuleDataset(string sDescription) : base(sDescription) {}
        public AmDynamicModuleDataset() : this("") {}


        #region Properties.Management
        IEnumerable<object> IContentProvider.Content => lDynVariables;
        Type IContentProvider.ContentType => lDynVariables.GetType().GenericTypeArguments[0];
        #endregion
        #region Properties.Management
        [Browsable(false)]
        public override IEnumerable<IAmVariable> Variables
        {
            get
            {
                // Yield static variables:
                foreach (var _iVar in base.Variables)
                    yield return (_iVar);

                // Yield dynamic variables:
                foreach (var _iVar in lDynVariables)
                    yield return (_iVar);
            }
        }
        private List<IAmVariable> lDynVariables = new List<IAmVariable>();
        #endregion


        #region Initialization
        public void AddDynamicVariable(IAmVariable iVariable) => lDynVariables.Add(iVariable);
        /// <summary>
        /// Adds a dynamic variable.
        /// </summary>
        /// <param name="tVariableType">Variable type.</param>
        /// <param name="tDataType">Datatype of the variable.</param>
        public IAmVariable AddDynamicVariable(AmVariableType aVariableType, Type tDataType, string sVariableName, object oDefaultValue = null)
        {
            if (!AmModule.VARIABLE_MAP.ContainsKey(aVariableType))
                throw new TypeAccessException(string.Format("Failed to add dynamic variable of unknown type '{0}'!", aVariableType));
            else if (!AmModule.TYPE_MAP.ContainsValue(tDataType))
                throw new NotSupportedException(string.Format("Failed to add dynamic variable of unsupported datatype '{0}'!", tDataType.GetType().Name));

            // Ensure valid default value:
            if (oDefaultValue == null)
            {
                if (Type.GetTypeCode(tDataType) == TypeCode.String)
                    // [Workaround] Bypass the need for a constructor argument :(
                    oDefaultValue = "";
                else
                    // Create type's default value:
                    oDefaultValue = Activator.CreateInstance(tDataType);
            }
            else if (oDefaultValue.GetType() != tDataType)
                throw new ArgumentException(string.Format("Failed to add dynamic variable '{0}' with default value of unexpected type '{1}'!", sVariableName, oDefaultValue.GetType().Name));

            // Create variable instance:
            var _tVarType = AmModule.VARIABLE_MAP[aVariableType];
            var _tGenVarType = _tVarType.MakeGenericType(tDataType);
            var _aVarInst = (AmVariable)Activator.CreateInstance(_tGenVarType, new object[] { oDefaultValue });
            {
                _aVarInst.VariableName = sVariableName;
            }
            lDynVariables.Add(_aVarInst);

            return (_aVarInst);
        }
        /// <summary>
        /// Adds a dynamic variable.
        /// </summary>
        /// <param name="tVariableType">Variable type.</param>
        /// <typeparam name="TDataType">Datatype of the variable.</typeparam>
        public IAmVariable<TDataType> AddDynamicVariable<TDataType>(AmVariableType aVariableType, string sVariableName, TDataType tDefaultValue = default(TDataType))
        {
            return ((IAmVariable<TDataType>)AddDynamicVariable(aVariableType, typeof(TDataType), sVariableName, tDefaultValue));
        }
        #endregion
    }
    public interface IAmVariable : IAmAccess, IAmLockable
    {
        #region Properties.Management
        AmVariableType VariableType { get; }
        AmVariableSettings ReferredSetting { get; }
        object Tag { set; get; }
        #endregion
        #region Properties
        string Category { get; }
        string VariableName { get; }
        string DisplayName { get; }
        string Description { get; }
        DateTime LastCov { get; }
        TimeSpan StableTime { get; }
        #endregion


        #region Events
        event EventHandler<ValueChangedEventArgs> ValueChanged;
        #endregion


        #region Management
        /// <summary>
        /// Applies a new value.
        /// </summary>
        /// <typeparam name="oValue">New value.</typeparam>
        /// <typeparam name="iContext">Identifies the context within the value is set (Required for _locked_ values).</typeparam>
        void SetValue(object oValue, IAmContext iContext = null);
        object GetValue();
        void ParseValue(string sValue);
        #endregion
    }
    public interface IAmVariable<TVal> : IAmVariable
    {
        #region Properties
        TVal Value { set; get; }
        #endregion


        #region Management
        /// <summary>
        /// Applies a new value.
        /// </summary>
        /// <typeparam name="tValue">New value.</typeparam>
        /// <typeparam name="iContext">Identifies the context within the value is set (Required for _locked_ values).</typeparam>
        void SetValue(TVal tValue, IAmContext iContext = null);
        #endregion
    }
    public interface IAmAccess
    {
        #region Properties.Access
        bool ReadOnly { get; }
        #endregion
    }
    public abstract class AmVariable : IAmVariable
    {
        public AmVariable(AmVariableType aVariableType)
        {
            this.VariableType = aVariableType;
            this.aLockup = new AmVariableLock(this);
        }


        #region Properties.Management
        [Browsable(false)]
        public AmVariableType VariableType { init; get; }
        [Browsable(false)]
        public AmVariableSettings ReferredSetting { internal set; get; }
        [Browsable(false)]
        public object Tag { set; get; }

        AmLock IAmLockable.Lockup => aLockup;
        private protected AmVariableLock aLockup;
        
        #endregion
        #region Properties.Access
        [Browsable(false)]
        public virtual bool ReadOnly => false;
        #endregion
        #region Properties
        [Browsable(false)]
        public string Category { internal set; get; }
        [Browsable(false)]
        public string VariableName { internal set; get; }
        [ReadOnly(true)]
        [DisplayName("Variable")]
        public string DisplayName { internal set; get; }
        [ReadOnly(true)]
        [DisplayName("")]
        public string Description { internal set; get; }
        [Browsable(false)]
        [DisplayName("Time of last changed value")]
        public DateTime LastCov { protected set; get; } = DateTime.Now;
        [Browsable(false)]
        [DisplayName("Duration since last changed value")]
        public TimeSpan StableTime => (DateTime.Now - LastCov);
        #endregion


        #region Events
        public event EventHandler<ValueChangedEventArgs> ValueChanged;
        #endregion


        #region Management.Events
        protected void _OnValueChanged(ValueChangedEventArgs vArgs) => ValueChanged?.Invoke(this, vArgs);
        #endregion
        #region Management
        internal virtual void Reset()
        {
            // Clear subscriptions:
            ValueChanged = delegate { };
        }

        public abstract object GetValue();
        void IAmVariable.SetValue(object oValue, IAmContext iContext) => SetValue(oValue, iContext);
        public abstract void SetValue(object oValue, IAmContext iContext = null);
        public abstract void ParseValue(string sValue);
        #endregion
    }
    [Layout(LayoutType.eInputGroup)]
    public abstract class AmVariable<TVal> : AmVariable, IAmVariable<TVal>
    {
        #region Constants
        private static readonly Dictionary<TimeSpan, ValueChangedFlags> IMPULSE_TIMING = new()
        {
            { TimeSpan.FromMilliseconds(100), ValueChangedFlags.eBounced },
            { TimeSpan.FromMilliseconds(1000), ValueChangedFlags.eShortImpulse },
            { TimeSpan.FromMilliseconds(2500), ValueChangedFlags.eLongImpulse }
        };
        #endregion


        public AmVariable(AmVariableType aVariableType, TVal tValue = default(TVal)) : base(aVariableType)
        {
            this.Value = tValue;
        }


        #region Properties
        [DisplayName("Value")]
        public virtual TVal Value
        {
            set => SetValue(value);
            get => tValue;
        }
        protected TVal tValue;
        #endregion


        #region Management
        public override string ToString() => Value.ToString();
        void IAmVariable<TVal>.SetValue(TVal tValue, IAmContext iContext) => SetValue(tValue, iContext);
        public override void SetValue(object oValue, IAmContext iContext = null)
        {
            aLockup.Validate(nameof(SetValue), iContext);

            // Apply value:
            var _tVal = (TVal)oValue;
            if (!Object.Equals(tValue, _tVal))
            {
                var _dNow = DateTime.Now;
                var _vFlags = ValueChangedFlags.None;
                var _tPrevVal = tValue;

                // Detect flags:
                if (_tVal.Equals(default(TVal)))
                {
                    if (dToActive != default)
                    {
                        var _tDuration = (_dNow - dToActive);
                        var _bSkipBounce = !(this is IAmInputVariable<TVal>);
                        var _kTiming = IMPULSE_TIMING
                            .Skip(_bSkipBounce ? 1 : 0)
                            .FirstOrDefault(_kPair => (_tDuration <= _kPair.Key));
                        if (_kTiming.Key != default)
                            _vFlags = _kTiming.Value;
                    }
                    dToActive = default;
                }
                else
                {
                    if ((tValue == null) || (tValue.Equals(default(TVal))))
                        dToActive = _dNow;
                }

                tValue = _tVal;
                LastCov = _dNow;
                _OnValueChanged(new ValueChangedEventArgs(_tPrevVal, _vFlags));
            }
        }
        public override object GetValue() => Value;
        public override void ParseValue(string sValue)
        {
            if (typeof(TVal).Equals(typeof(DateTime)))
                Value = (TVal)(object)DateTime.Parse(sValue);
            else if (typeof(TVal).Equals(typeof(TimeSpan)))
                Value = (TVal)(object)UtlTimeSpan.Parse(sValue);
            else
                Value = (TVal)Convert.ChangeType(sValue, typeof(TVal));
        }
        #endregion


        private DateTime dToActive = default;
    }
    public abstract class AmReadOnlyVariable<TVal> : AmVariable<TVal>
    {
        public AmReadOnlyVariable(AmVariableType aVariableType, TVal tValue = default(TVal)) : base(aVariableType, tValue) {}


        #region Properties.Access
        [Browsable(false)]
        public override bool ReadOnly => true;
        #endregion
        #region Properties
        [ReadOnly(true)]
        public new TVal Value
        {
            set { base.Value = value; }
            get => base.Value;
        }
        #endregion
    }
    public interface IAmDisplayVariable : IAmVariable
    {
    }
    public interface IAmDisplayVariable<TVal> : IAmVariable<TVal>, IAmDisplayVariable
    {
    }
    public class AmDisplayVariable<TVal> : AmReadOnlyVariable<TVal>, IAmDisplayVariable<TVal>
    {
        public AmDisplayVariable(TVal tValue = default(TVal)) : base(AmVariableType.eDisplay, tValue) {}
    }
    public interface IAmSetpointVariable : IAmVariable
    {
    }
    public interface IAmSetpointVariable<TVal> : IAmVariable<TVal>, IAmSetpointVariable
    {
    }
    public class AmSetpointVariable<TVal> : AmVariable<TVal>, IAmSetpointVariable<TVal>
    {
        public AmSetpointVariable(TVal tValue = default(TVal)) : base(AmVariableType.eSetpoint, tValue) {}
    }
    public class AmSetpointButtonVariable<TVal> : AmVariable<TVal>, IAmSetpointVariable<TVal>
    {
        public AmSetpointButtonVariable(TVal tValue = default(TVal)) : base(AmVariableType.eSetpointButton, tValue) {}


        #region Properties
        [ButtonLayout("far fa-hand-point-up")]
        public override TVal Value
        {
            set
            {
                // Apply value and trigger COV-event:
                base.Value = value;

                // Reset value (because a button variable always has to stay in normal state).
                base.Value = default(TVal);
            }
            get => base.Value;
        }
        #endregion
    }
    public interface IAmInputVariable : IAmVariable
    {
    }
    public interface IAmInputVariable<TVal> : IAmVariable<TVal>, IAmInputVariable
    {
    }
    public class AmInputVariable<TVal> : AmReadOnlyVariable<TVal>, IAmInputVariable<TVal>
    {
        public AmInputVariable(TVal tValue = default(TVal)) : base(AmVariableType.eInput, tValue) {}
    }
    public interface IAmOutputVariable : IAmVariable
    {
    }
    public interface IAmOutputVariable<TVal> : IAmVariable<TVal>, IAmOutputVariable
    {
    }
    public class AmOutputVariable<TVal> : AmReadOnlyVariable<TVal>, IAmOutputVariable<TVal>
    {
        public AmOutputVariable(TVal tValue = default(TVal)) : base(AmVariableType.eOutput, tValue) {}
    }
    #endregion


    #region Extensions
    public static class ExtAmLockable
    {
        #region Instance
        /// <summary>
        /// Locks the current instance as long as executing an action on it.
        /// </summary>
        /// <typeparam name="iOwner">Locking context.</typeparam>
        /// <typeparam name="aAction">Action wo be executed locked.</typeparam>
        /// <typeparam name="bUnlock">Unlocks current instance if <c>TRUE</c> otherwise leaves current instance locked when done.</typeparam>
        public static void DoLocked<T>(this T iSource, IAmContext iOwner, Action<T> aAction, bool bUnlock = true)
            where T : IAmLockable
        {
            if (!iSource.TryDoLocked(iOwner, aAction, bUnlock))
                throw new InvalidOperationException(string.Format("Failed to lock because lock is acquired within another context!"));
        }
        /// <summary>
        /// Locks the current instance as long as executing an action on it if a lock can be acquired.
        /// </summary>
        /// <typeparam name="iOwner">Locking context.</typeparam>
        /// <typeparam name="aAction">Action wo be executed locked.</typeparam>
        /// <typeparam name="bUnlock">Unlocks current instance if <c>TRUE</c> otherwise leaves current instance locked when done.</typeparam>
        public static bool TryDoLocked<T>(this T iSource, IAmContext iOwner, Action<T> aAction, bool bUnlock = true)
            where T : IAmLockable
        {
            lock (AmLock.ThreadLockObject)
            {
                if (iSource.CanLock(iOwner))
                {
                    try
                    {
                        // Lock via temporary lock:
                        AmLock.TemporaryLock.Lock(iOwner);

                        aAction(iSource);

                        if (bUnlock)
                            iSource.Unlock(iOwner);
                        else
                            // Unlocking not intent; so leave source locked:
                            iSource.Lock(iOwner);

                        return (true);
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        AmLock.TemporaryLock.Unlock(iOwner);
                    }
                }
            }
            return (false);
        }
        #endregion
        #region Enumerated
        /// <summary>
        /// Determines if one or more instances of a range of <see cref="IAmLockable"/> instances are locked.
        /// </summary>
        public static bool IsLocked<T>(this IEnumerable<T> iSource)
            where T : IAmLockable
        {
            lock (AmLock.ThreadLockObject)
            {
                return (iSource.Any(_iVar => _iVar.Locked));
            }
        }
        /// <summary>
        /// Locks a range of <see cref="IAmLockable"/> instances.
        /// </summary>
        /// <typeparam name="iOwner">Locking context.</typeparam>
        public static void Lock<T>(this IEnumerable<T> iSource, IAmContext iOwner)
            where T : IAmLockable
        {
            lock (AmLock.ThreadLockObject)
            {
                if (iSource.CanLock(iOwner))
                    iSource.ForEach(_iVar => _iVar.Lock(iOwner));
                else
                    throw new InvalidOperationException(string.Format("Failed to lock range because lock is acquired within another context!"));
            }
        }
        /// <summary>
        /// Tries to lock a range of <see cref="IAmLockable"/> instances.
        /// </summary>
        /// <typeparam name="iOwner">Locking context.</typeparam>
        public static bool TryLock<T>(this IEnumerable<T> iSource, IAmContext iOwner)
            where T : IAmLockable
        {
            lock (AmLock.ThreadLockObject)
            {
                if (iSource.CanLock(iOwner))
                {
                    iSource.Lock(iOwner);
                    return (true);
                }
                else
                    return (false);
            }
        }
        /// <summary>
        /// Unlocks a range of <see cref="IAmLockable"/> instances.
        /// </summary>
        /// <typeparam name="iOwner">Unlocking context.</typeparam>
        public static void Unlock<T>(this IEnumerable<T> iSource, IAmContext iOwner)
            where T : IAmLockable
        {
            lock (AmLock.ThreadLockObject)
            {
                if (iSource.CanLock(iOwner))
                    iSource.ForEach(_iVar => _iVar.Unlock(iOwner));
                else
                    throw new InvalidOperationException(string.Format("Failed to lock range because lock is acquired within another context!"));
            }
        }
        /// <summary>
        /// Tries to unlock a range of <see cref="IAmLockable"/> instances.
        /// </summary>
        /// <typeparam name="iOwner">Unlocking context.</typeparam>
        public static bool TryUnlock<T>(this IEnumerable<T> iSource, IAmContext iOwner)
            where T : IAmLockable
        {
            lock (AmLock.ThreadLockObject)
            {
                if (iSource.CanLock(iOwner))
                {
                    iSource.Unlock(iOwner);
                    return (true);
                }
                else
                    return (false);
            }
        }
        
        /// <summary>
        /// Locks a range of <see cref="IAmLockable"/> instances as long as executing an action for each instance.
        /// </summary>
        /// <typeparam name="iOwner">Locking context.</typeparam>
        /// <typeparam name="aAction">Action wo be executed locked.</typeparam>
        /// <typeparam name="bUnlock">Unlocks current instance if <c>TRUE</c> otherwise leaves current instance locked when done.</typeparam>
        public static void LockedForEach<T>(this IEnumerable<T> iSource, IAmContext iOwner, Action<T> aAction, bool bUnlock = true)
            where T : IAmLockable
        {
            if (!iSource.TryLockedForEach(iOwner, aAction, bUnlock))
                throw new InvalidOperationException(string.Format("Failed to lock range because lock is acquired within another context!"));
        }
        /// <summary>
        /// Locks a range of <see cref="IAmLockable"/> instances as long as executing an action for each instance if a lock can be acquired.
        /// </summary>
        /// <typeparam name="iOwner">Locking context.</typeparam>
        /// <typeparam name="aAction">Action wo be executed locked.</typeparam>
        /// <typeparam name="bUnlock">Unlocks current instance if <c>TRUE</c> otherwise leaves current instance locked when done.</typeparam>
        public static bool TryLockedForEach<T>(this IEnumerable<T> iSource, IAmContext iOwner, Action<T> aAction, bool bUnlock = true)
            where T : IAmLockable
        {
            lock (AmLock.ThreadLockObject)
            {
                if (iSource.CanLock(iOwner))
                {
                    try
                    {
                        // Lock via temporary lock:
                        AmLock.TemporaryLock.Lock(iOwner);

                        iSource.ForEach(aAction);

                        if (bUnlock)
                            iSource.Unlock(iOwner);
                        else
                            // Unlocking not intent; so leave source locked:
                            iSource.Lock(iOwner);

                        return (true);
                    }
                    catch (Exception)
                    {
                        throw;
                    }
                    finally
                    {
                        AmLock.TemporaryLock.Unlock(iOwner);
                    }
                }
            }
            return (false);
        }
        #endregion


        #region Helper
        private static bool CanLock<T>(this T iSource, IAmContext iOwner)
            where T : IAmLockable
        {
            lock (AmLock.ThreadLockObject)
            {
                if (!(AmLock.TemporaryLock.LockContext ?.Equals(iOwner) ??true))
                    return (false);
                else
                    return (iSource.LockContext ?.Equals(iOwner) ??true);
            }
        }
        private static bool CanLock<T>(this IEnumerable<T> iSource, IAmContext iOwner)
            where T : IAmLockable
        {
            lock (AmLock.ThreadLockObject)
            {
                if (!(AmLock.TemporaryLock.LockContext ?.Equals(iOwner) ??true))
                    return (false);
                else
                    return (iSource
                        .Select(_iVar => _iVar.LockContext)
                        .All(_iContext => (_iContext ?.Equals(iOwner) ??true)));
            }
        }
        #endregion
    }
    #endregion
}