using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AutoMate.Services;
using AutoMate.Logger;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Core;
using Serilog.Events;


namespace AutoMate
{
    public class Program
    {
        public static void Main(string[] args)
        {
            // Create logger configuration:
            bool _bReloadOnChange = true;
            var _cConfig = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", false, _bReloadOnChange)
                .AddJsonFile($"appsettings.{Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") ?? "Production"}.json", true, _bReloadOnChange)
                .Build();

            // Load defaults:
            // (BETA) ... [Settings]
            // > Add this as configurable option when implementing `settings`.
            Env.GeneralLogLevel.MinimumLevel = _cConfig.Providers.GetValue("Serilog:MinimumLevel:General", LogEventLevel.Warning);
            Env.AutomateLogLevel.MinimumLevel = _cConfig.Providers.GetValue("Serilog:MinimumLevel:Automate", LogEventLevel.Information);
            Env.FilteredLogLevel.MinimumLevel = _cConfig.Providers.GetValue("Serilog:MinimumLevel:Filtered", LogEventLevel.Information);
            int _iRetainedDayLimit = _cConfig.Providers.GetValue("Serilog:RetainedDayLimit", 3);
            var _sLogTemplate = _cConfig.Providers.GetValue("Serilog:LogTemplate", "{Timestamp:HH:mm:ss.fff} | {SourceContext} [{Level:u3}{EventId: 0}] {Message:lj}{NewLine}{Exception}");
            var _sViewTemplate = _cConfig.Providers.GetValue("Serilog:ViewTemplate", "{Timestamp:MM.dd HH:mm:ss} | [{Level:u3}{EventId: 0}] {Message:lj}{NewLine}{Exception}");

            // Create logger:
            Log.Logger = new LoggerConfiguration()
                // General:
                .MinimumLevel.ControlledBy(Env.GeneralLogLevel)
                .MinimumLevel.Override("AutoMate", Env.AutomateLogLevel)
                .ReadFrom.Configuration(_cConfig)
                .Enrich.FromLogContext()
                .Enrich.BeautifyEventId()
                // Sinks:
                .WriteTo.File(
                    Path.Combine(Env.LOG_PATH, string.Format("{0}.{1}", Env.LOG_FILENAME, Env.LOG_FILEEXT)),
                    rollingInterval: RollingInterval.Day,
                    fileSizeLimitBytes: (10 * 1024 * 1024),
                    retainedFileCountLimit: 30,
                    rollOnFileSizeLimit: true,
                    shared: true,
                    flushToDiskInterval: TimeSpan.FromSeconds(1),
                    outputTemplate: _sLogTemplate
                )
                .WriteTo.Console(
                    outputTemplate: _sLogTemplate
                )
                .WriteTo.ModuleLog(
                    retainedDayLimit: _iRetainedDayLimit,
                    outputTemplate: _sViewTemplate
                )
                .CreateLogger();

            try
            {
                CreateHostBuilder(args).Build().Run();
            }
            catch (Exception _eExcpt)
            {
                Log.Fatal(_eExcpt, "Host terminated unexpectedly!");
            }
            finally
            {
                Log.CloseAndFlush();
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .UseSerilog()
                .UseSystemd()
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>();
                })
                .ConfigureServices(services =>
                {
                    services.AddSingleton<AmCoreService>();
                    services.AddHostedService<AmCoreService>(provider => provider.GetService<AmCoreService>());
                });
    }


    public static class ExtConfigurationProvider
    {
        /// Determines a configuration value (Returns <seealso cref="tDefault"/> if key is not found).
        /// Priority of configuration providers has reverse order!
        public static TVal GetValue<TVal>(this IEnumerable<IConfigurationProvider> iSource, string sKey, TVal tDefault = default(TVal))
        {
            string _sVal = "";
            if (iSource.Reverse().FirstOrDefault(_iCfg => _iCfg.TryGet(sKey, out _sVal)) == null)
                return (tDefault);
            else
            {
                Type _tVal = typeof(TVal);
                if (_tVal.IsEnum)
                    return ((TVal)Enum.Parse(_tVal, _sVal));
                else
                    return ((TVal)Convert.ChangeType(_sVal, typeof(TVal)));
            }
        }
    }
}
