using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Threading;
using System.Threading.Tasks;
using AutoMate.Data;
using AutoMate.Models;
using AutoMate.Modules;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Shared.Asp.UserNotice;
using Shared.Utils.Core;

namespace AutoMate.Services
{
    public class AmCoreService : IHostedService, IDisposable
    {
        #region Delegates
        public delegate void ExecuteScopedHandler(ApplicationDbContext aContext);
        #endregion


        #region Types
        public class AmModuleTypeTuple
        {
            public AmModuleTypeTuple(Type tModuleSettingsType, ModuleDescriptionAttribute mModuleDescription)
            {
                this.ModuleSettingsType = tModuleSettingsType;
                this.ModuleDescription = mModuleDescription;
            }


            #region Properties
            public Type ModuleSettingsType { init; get; }
            public ModuleDescriptionAttribute ModuleDescription { init; get; }
            #endregion
        }
        #endregion


        public AmCoreService(IHostEnvironment iEnvironment, IServiceScopeFactory iScopeFactory, ILogger<AmCoreService> iLogger)
        {
            this.Logger = iLogger;
            this.ScopeFactory = iScopeFactory;

            LoadModuleTypes();
        }


        #region Properties.Management
        private ILogger Logger { init; get; }
        private IServiceScopeFactory ScopeFactory { init; get; }

        public UserNoticeQueue UserNotices { get; } = new UserNoticeQueue();
        #endregion
        #region Properties.Settings
        public IReadOnlyDictionary<Type, AmModuleTypeTuple> ModuleTypes => tModuleTypes;
        private Dictionary<Type, AmModuleTypeTuple> tModuleTypes = new Dictionary<Type, AmModuleTypeTuple>();
        public IReadOnlyList<AmModule> Modules => aModules;
        public IEnumerable<AmModule> FailedModules => aModules.Where(_aModule => (_aModule.State.State == AmModuleState.eError));
        private List<AmModule> aModules = new List<AmModule>();
        #endregion


        #region Events
        public void OnModuleSettingsChanged(IEnumerable<AmModuleSettings> iUpdate) => SynchronizeModuleState(iUpdate);
        public void OnImportModules(IReadOnlyDictionary<AmModuleSettings, AmModulePersistenceDataset> iUpdate) => SynchronizeModuleState(iUpdate, false);
        #endregion


        #region Initialization
        public Task StartAsync(CancellationToken cStopToken)
        {
            // Start modules:
            SynchronizeModuleState();

            return (Task.CompletedTask);
        }
        public Task StopAsync(CancellationToken cStopToken)
        {
            // Stop running modules:
            aModules.ForEach(_aModule => _aModule.Dispose());

            SaveChanges();

            return (Task.CompletedTask);
        }
        public Task RestartAsync(CancellationToken cStopToken)
        {
            Logger.LogInformation(93, "Restarting modules.");

            // Stop running modules:
            aModules.ForEach(_aModule => _aModule.Dispose());
            aModules.Clear();

            GC.Collect();

            SaveChanges();

            // Start modules:
            return (StartAsync(cStopToken));
        }
        private void LoadModuleTypes()
        {
            var aAssembly = Assembly.GetCallingAssembly();
            if (aAssembly != null)
            {
                foreach (var _tType in aAssembly.ExportedTypes)
                {
                    var _mDescr = _tType.GetCustomAttribute<ModuleDescriptionAttribute>();
                    if (_mDescr != null)
                    {
                        var _pProperty = _tType.GetProperties().FirstOrDefault(_pProp => _pProp.PropertyType.IsSubclassOf(typeof(AmModuleSettings)));
                        if (_pProperty == null)
                            throw new TypeLoadException("Failed to load module type due to not implemented settings property!");
                        else
                            tModuleTypes.Add(_tType, new AmModuleTypeTuple(_pProperty.PropertyType, _mDescr));
                    }
                }
            }
        }
        #endregion
        #region Management
        /// Save database changes.
        private void SaveChanges() => ExecuteScoped(_aContext => _aContext.SaveChanges());
        #endregion
        #region Management
        /// Executes an action by using scoped services.
        public void ExecuteScoped(ExecuteScopedHandler eCommand)
        {
            if (aCurScope == null)
            {
                // Create scope for executing command:
                using (var scope = ScopeFactory.CreateScope())
                {
                    eCommand(scope.ServiceProvider.GetService<ApplicationDbContext>());
                }
            }
            else
                // Execute command with previously served scope:
                eCommand(aCurScope);
        }
        /// Executes an action by serving a scope.
        public void ExecuteScopedServed(ApplicationDbContext aContext, Action aCommand)
        {
            aCurScope = aContext;
            {
                aCommand();
            }
            aCurScope = null;
        }
        private ApplicationDbContext aCurScope = null;
        #endregion
        #region Management.Modules
        /// <summary>
        /// Synchronizes a loaded module with its settings.
        /// </summary>
        private void SynchronizeModuleState()
        {
            ExecuteScoped((_aContext) => SynchronizeModuleState(_aContext.ModuleSettings));
        }
        /// <summary>
        /// Synchronizes a loaded module with its settings.
        /// </summary>
        /// <param name="iUpdate">Updated settings.</param>
        private void SynchronizeModuleState(IEnumerable<AmModuleSettings> iUpdate)
        {
            SynchronizeModuleState(iUpdate.ToDictionary<AmModuleSettings, AmModuleSettings, AmModulePersistenceDataset>(
                _aSettings => _aSettings,
                _aSettings => null
            ), true);
        }
        /// <summary>
        /// Synchronizes a loaded module with its settings.
        /// </summary>
        /// <param name="iUpdate">Updated settings.</param>
        /// <param name="bRemoveMissing">Removes missing modules if no settings are specified.</param>
        private void SynchronizeModuleState(IReadOnlyDictionary<AmModuleSettings, AmModulePersistenceDataset> iUpdate, bool bRemoveMissing)
        {
            Logger.LogInformation(169, "Synchronizing module state.");

            var _aIdCmpr = new AmIdentityComparer();

            // Determine module initialization order:
            // > (BETA) ... [Order by module type]
            //   Modules with dependencies (to other modules) have to be initialized AFER non-dependant modules!
            //   Actual workaround:
            //   > Initialize scripting-host module at last (knowing that it is the only dependent module-type)
            //   TODO:
            //   > Dependencies have to be determined better! So an Module-initialization-order can be build more exactly!
            var _iSortedUpdate = iUpdate
                .OrderBy(_aPair => _aPair.Key, new AmTypeOrderComparer())
                .ToDictionary(_iPair => _iPair.Key, _iPair => _iPair.Value);
            
            // Step 1) Reset all variables:
            aModules.ForEach(_iModule =>
            {
                _iModule.Datasets.Values.ForEach(_iDataset =>
                {
                    _iDataset.Variables.ForEach(_aVar => ((AmVariable)_aVar).Reset());
                });
            });

            // Step 2.1) Remove deleted modules:
            if (bRemoveMissing)
            {
                // Deleting modules according to module settings:
                var _aToRemove = aModules.Except<IAmModuleID>(_iSortedUpdate.Keys, _aIdCmpr).Cast<AmModule>();
                if (!_aToRemove.IsEmpty())
                {
                    _aToRemove.ForEach(_aModule =>
                    {
                        Logger.LogTrace(202, string.Format("Deleting module '{0}'.", _aModule));

                        _aModule.Dispose();
                        ExecuteScoped(_aContext => _aContext.RemoveModulePersistence(_aModule, false));
                    });
                    aModules.RemoveRange(_aToRemove);
                }
            }

            // Step 2.2) Remove disabled modules:
            // Deleting modules according to module settings:
            var _aDisabled = _iSortedUpdate.Keys.Where(_iSettings => !_iSettings.Enabled).ToArray();
            var _aToDisable = aModules.Intersect<IAmModuleID>(_aDisabled, _aIdCmpr).Cast<AmModule>();
            if (!_aToDisable.IsEmpty())
            {
                _aToDisable.ForEach(_aModule => {
                    Logger.LogTrace(218, string.Format("Disable module '{0}'.", _aModule));
                    _aModule.Dispose();
                });
                aModules.RemoveRange(_aToDisable);
            }

            // Step 3) Stop modules to update:
            var _aToUpdate = aModules.Intersect<IAmModuleID>(_iSortedUpdate.Keys, _aIdCmpr).Cast<AmModule>();

            // Dispose modules to start reinitialization:
            _aToUpdate.ForEach(_aModule => {
                Logger.LogTrace(229, string.Format("Updating module '{0}'.", _aModule));
                _aModule.Dispose();
                });

            // Step 4) Import persistent data:
            _iSortedUpdate.ForEach(_iUpdate =>
            {
                if (_iUpdate.Value != null)
                    ExecuteScoped(_aContext => _aContext.WriteModulePersistence(_iUpdate.Value, false));
            });
            SaveChanges();

            // Update changed module's according to module settings:
            var _lUpdated = new List<AmModule>();
            _aToUpdate.ForEach(_aModule =>
            {
                try
                {
                    var _iUpdate = _iSortedUpdate.First(_iPair => _aIdCmpr.Equals(_aModule, _iPair.Key));

                    // Change settings:
                    _aModule.State = AmModule.AmState.Ok;
                    _aModule.OnChangingModuleSettings(_iUpdate.Key);

                    // Reinitialize:
                    if (_aModule.OnInitModule(true))
                        _lUpdated.Add(_aModule);
                }
                catch (Exception eExcpt)
                {
                    _aModule.State = new AmModule.AmState(eExcpt);
                    Logger.LogError(250, eExcpt, string.Format("Update of module '{0}' failed!", _aModule));
                }
            });

            // Step 5) Create and initialize new modules:
            var _lCreated = new List<AmModule>();
            var _aToCreate = _iSortedUpdate.Keys
                .Except<IAmModuleID>(aModules, _aIdCmpr).Cast<AmModuleSettings>()
                .Where(_aSettings => _aSettings.Enabled);
            _aToCreate.ForEach(_aSettings =>
            {
                var _aModuleType = ModuleTypes.First(_mType => (_mType.Value.ModuleSettingsType == _aSettings.GetType()));
                var _aModule = (AmModule)Activator.CreateInstance(_aModuleType.Key, new object[] { this, _aSettings });

                _aModule.State = AmModule.AmState.Ok;
                try
                {
                    // Initialize:
                    Logger.LogTrace(278, string.Format("Initializing module '{0}'.", _aModule));
                    if (_aModule.OnInitModule())
                        _lCreated.Add(_aModule);
                    else
                        throw new AggregateException();
                }
                catch (Exception eExcpt)
                {
                    _aModule.State = new AmModule.AmState(eExcpt);
                    Logger.LogError(276, eExcpt, string.Format("Initialization of module '{0}' failed!", _aModule));
                }
                finally
                {
                    aModules.Add(_aModule);
                }
            });

            // Step 6) Finalize:
            // Post-Initialize new and updated module instances:
            Action<AmModule, bool> PostInit = (_aModule, bReInit) =>
            {
                try
                {
                    Logger.LogTrace(301, string.Format("Finalizing module '{0}'.", _aModule));
                    _aModule.OnPostInitModule(bReInit);
                }
                catch (Exception eExcpt)
                {
                    _aModule.State = new AmModule.AmState(eExcpt);
                    Logger.LogError(295, eExcpt, string.Format("Post-initialization of module '{0}' failed!", _aModule));
                }
            };
            _lUpdated.ForEach(_aModule => PostInit(_aModule, true));
            _lCreated.ForEach(_aModule => PostInit(_aModule, false));

            // Save database changes that may occured during initialization:
            SaveChanges();

            // Notify user:
            var _sFailed = FailedModules.Select(_aModule => ("- " + _aModule.Title)).ToArray();
            if (_sFailed.Length > 0)
            {
                var _sText = ("Failed to load the following modules:\n" + string.Join('\n', _sFailed));
                UserNotices.Enqueue(new UserNotice(_sText, "Modules", "Initialization", NoticeType.eError));
            }

            Logger.LogTrace(169, "Synchronized module state.");
        }
        #endregion


        public void Dispose()
        {
        }
    }
}