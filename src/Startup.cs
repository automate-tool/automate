using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMate.Data;
using AutoMate.Modules;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace AutoMate
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }


        #region Properties.Management
        public IConfiguration Configuration { get; }
        #endregion


        #region Initialization
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>();

            services.AddDatabaseDeveloperPageExceptionFilter();

            services.AddDefaultIdentity<IdentityUser>(options => options.SignIn.RequireConfirmedAccount = true)
                .AddRoles<IdentityRole>()
                .AddEntityFrameworkStores<ApplicationDbContext>();
            
            services.AddSession();
        }
        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder iApplication, IWebHostEnvironment iEnvironment, ApplicationDbContext aContext, IServiceProvider iServices, ILoggerFactory iLoggerFactory)
        {
            Env.Init(iLoggerFactory);

            if (iEnvironment.IsDevelopment())
            {
                iApplication.UseDeveloperExceptionPage();
                iApplication.UseMigrationsEndPoint();
            }
            else
            {
                iApplication.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                iApplication.UseHsts();
            }
            iApplication.UseHttpsRedirection();
            iApplication.UseSession();
            iApplication.UseStaticFiles();

            iApplication.UseRouting();

            iApplication.UseAuthentication();
            iApplication.UseAuthorization();

            iApplication.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapRazorPages();
            });

            // Seed defaults:
            CreateRole(iServices, Env.ROLE_ADMIN).Wait();
            CreateRole(iServices, Env.ROLE_RESIDENT).Wait();
        }
        #endregion
        #region Management
        private async Task CreateRole(IServiceProvider iServices, string sRole)
        {
            var _iRoleManager = iServices.GetService<RoleManager<IdentityRole>>();
            if (!await _iRoleManager.RoleExistsAsync(sRole))
                await _iRoleManager.CreateAsync(new IdentityRole(sRole));
        }
        private async Task CreateUser(IServiceProvider iServices, string sUser, string? sPassword = null)
        {
            var _iUserManager = iServices.GetService<UserManager<IdentityUser>>();
            var _iAdmin = await _iUserManager.FindByNameAsync(sUser);
            if (_iAdmin == null)
            {
                _iAdmin = new IdentityUser()
                {
                    UserName = sUser
                };

                if (sPassword == null)
                    await _iUserManager.CreateAsync(_iAdmin);
                else
                    await _iUserManager.CreateAsync(_iAdmin, sPassword);
            }
        }
        #endregion
    }
}
